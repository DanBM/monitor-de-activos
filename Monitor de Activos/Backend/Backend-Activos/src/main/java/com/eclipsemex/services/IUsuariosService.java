package com.eclipsemex.services;

import org.springframework.security.core.Authentication;

import com.eclipsemex.security.dto.NuevoUsuario;

/**
 * Interfaz donde se definen los metodos de un Usuario
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
public interface IUsuariosService {
	/**
	 * Obtiene todos los usuarios
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	String buscarTodos(Authentication auth);

	/**
	 * Obtiene el detalle de un usuario
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	String usuarioDetalle(String emailEmailUsuario, Authentication auth);

	/**
	 * Agrega a un usuario.
	 * 
	 * @body nuevoUsuario representa un modelo para asignarle la informacion
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	String registrarUsuario(NuevoUsuario nuevoUsuario, Authentication auth);

	/**
	 * Modifica a un usuario.
	 * 
	 * @body nuevoUsuario representa un modelo para asignarle la informacion
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	String actualizarUsuario(NuevoUsuario nuevoUsuario, Authentication auth);

	/**
	 * Elimina a un usuario.
	 * 
	 * @param emailUsuario representa un correo de un usuario
	 * 
	 * @param auth         Representa el token para una solicitud de autenticación
	 *                     no se manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	String eliminarUsuario(String email, Authentication auth);

	/**
	 * Modifica la contraseña de un usuario.
	 * 
	 * @param emailUsuario representa un correo de un usuario
	 * 
	 * @param password     representa una contraseña de un usuario
	 * 
	 * @param auth         Representa el token para una solicitud de autenticación
	 *                     no se manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	String actualizarPasswordUsuario(String email, String password, Authentication auth);
}
