<?php

session_start();
/*Se verefica que el rol ingresado tenga el permiso para estar en esta pagania o pestaña.
@author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
@version 23/02/2021
 */
$views = [
    'admin'   => 'vistaAdminnistrador.html',
    'gerente' => 'vistaGerente.html',
    'index'   => '../index.php',
];
if ($_SESSION['tokenGuardado'] == '') {
    header('Location: ' . $views['index']);
} else {
    if ($_SESSION['rol'] == 'ROLE_ADMIN') {
        header('Location: ' . $views['admin']);

    }if ($_SESSION['rol'] == 'ROLE_GERENTE') {
        header('Location: ' . $views['gerente']);

    }
}
$curl = curl_init();
/* cambiar por el token que se necesite y como se haya guardado, este sempre tiene que ser dinamico
@author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
@version 23/02/2021
 */
curl_setopt_array($curl, array(
    CURLOPT_URL            => 'http://localhost:8080/rest/mis-rentas?email=' . $_SESSION['emailUsu'],
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING       => '',
    CURLOPT_MAXREDIRS      => 10,
    CURLOPT_TIMEOUT        => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST  => 'GET',
    CURLOPT_HTTPHEADER     => array(
        'Authorization: Bearer ' . $_SESSION['tokenGuardado'],
    ),
));

$response = curl_exec($curl);
curl_close($curl);
$dataToJson = json_decode($response, true);
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Tabla Rentas</title>
        <meta name="description" content="A sidebar menu as seen on the Google Nexus 7 website" />
        <meta name="keywords" content="google nexus 7 menu, css transitions, sidebar, side menu, slide out menu" />
        <meta name="author" content="Codrops" />
        <link rel="shortcut icon" href="../favicon.ico">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.2/css/all.css" integrity="sha384-vSIIfh2YWi9wW0r9iZe7RJPrKwp6bG+s9QZMoITbCckVJqGCCRhc+ccxNcdpHuYu" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/validacion.css" />
        <link rel="stylesheet" type="text/css" href="css/normalize.css" />
        <link rel="stylesheet" type="text/css" href="css/demo.css" />
        <link rel="stylesheet" type="text/css" href="css/component.css" />
         <!-- Librerias para la tabla -->
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js"></script>

    <!-- Datatables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/v/bs4-4.1.1/dt-1.10.18/datatables.min.css">
    <script src="https://cdn.datatables.net/v/bs4-4.1.1/dt-1.10.18/datatables.min.js"></script>

    <!-- Buttons -->
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.53/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.53/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>




        <script src="js/modernizr.custom.js"></script>
    </head>
    <body>
        <div class="container">
            <ul id="gn-menu" class="gn-menu-main">
                <li class="gn-trigger">
                    <a class="gn-icon gn-icon-menu"><span>Menú</span></a>
                    <nav class="gn-menu-wrapper">
                        <div class="gn-scroller">
                            <ul class="gn-menu">

                                <li>
                                    <a href="salir.php" class="gn-icon gn-icon-downloa">Salir</a>
                                    <ul class="gn-submenu">
                                        <li><a href="tablaActivoGeneral.php" class="gn-icon gn-icon-photosh">Activos</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="tablaRentasGeneral.php" class="gn-icon gn-icon-archive">Rentas</a>

                                </li>
                            </ul>

<!-- /Script para los modales
@author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
@version 23/02/2021-->

<script type="text/javascript">
        $(document).ready(function(){
        // Activate tooltip
        $('[data-toggle="tooltip"]').tooltip();

        // Select/Deselect checkboxes
        var checkbox = $('table tbody input[type="checkbox"]');
        $("#selectAll").click(function(){
            if(this.checked){
                checkbox.each(function(){
                    this.checked = true;
                });
            } else{
                checkbox.each(function(){
                    this.checked = false;
                });
            }
        });
        checkbox.click(function(){
            if(!this.checked){
                $("#selectAll").prop("checked", false);
            }
        });
    });
</script>
                        </div><!-- /gn-scroller -->
                    </nav>
                </li>
                <li><a href="">Sistema de Monitoreo de Activos</a></li>
                <li><a class="codrops-icon codrops-icon-" href="#addRentaModal" data-toggle="modal"><span>Nueva Renta</span></a></li>
                <li><a class="codrops-icon codrops-icon-" href="vistaGeneral.html"><span>Home</span></a></li>
            </ul>

<br> <br> <br>
<!-- /Contiene el mensaje de registro de una Renta.
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
 -->
<?php
if (isset($_SESSION['msg'])) {
    echo '<script type="text/javascript">
    alert("' . $_SESSION['msg'] . '");
    </script>';
}

unset($_SESSION['msg']);

?>

        </div><!-- /container -->
<section class="contenidoTabla">
    <div class="table-responsive" id="mydatatable-container">
        <table class="records_list table table-striped table-bordered table-hover" id="mydatatable">
            <thead>
              <tr>
                <th scope="col">Fecha Inicio</th>
                <th scope="col">Fecha Vencimiento</th>
                <th scope="col">Direccion</th>
                <th scope="col">Renta Vensida</th>
                <th scope="col">Estado</th>
                <th scope="col">Municipio</th>
                <th scope="col">Usuario Rentando</th>
                <th scope="col">Activo Rentado</th>

              </tr>
            </thead>
             <tfoot>
                <tr>

                    <th>Filter..</th>
                    <th>Filter..</th>
                    <th>Filter..</th>
                    <th>Filter..</th>
                    <th>Filter..</th>
                    <th>Filter..</th>
                    <th>Filter..</th>
                    <th>Filter..</th>


                </tr>
            </tfoot>
    <!-- /Se trae la informacion de las Rentas de la DB.
        @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
        @version 23/02/2021
    -->
    <tbody>
    <?php foreach ($dataToJson['Contenido'] as $renta) {?>
            <tr>

              <td><?php echo $renta['fechaInicio'] ?></td>
              <td><?php echo $renta['fechaVencimiento'] ?></td>
              <td><?php echo $renta['direccion'] ?></td>
              <td><?php echo ($renta['operandoRenta'] == 1) ? 'Si' : 'No' ?></td>
              <td><?php echo $renta['estado'] ?></td>
              <td><?php echo $renta['municipio'] ?></td>
              <td><?php echo $renta['nombreUsuario'] ?></td>
              <td><?php echo $renta['placActivo'] ?></td>

            </tr>

            <?php }?>
    </tbody>
    </table>
    </div>

</section>
<!-- /Scrip para la tabla y filtros de la misma.
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
-->

    <script type="text/javascript">
        $(document).ready(function () {
            $('#mydatatable tfoot th').each(function () {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Filtrar.." />');
            });

            var table = $('#mydatatable').DataTable({
                "dom": 'B<"float-left"i><"float-right"f>t<"float-left"l><"float-right"p><"clearfix">',
                "responsive": false,
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                },
                "order": [
                    [0, "desc"]
                ],
                "initComplete": function () {
                    this.api().columns().every(function () {
                        var that = this;

                        $('input', this.footer()).on('keyup change', function () {
                            if (that.search() !== this.value) {
                                that
                                    .search(this.value)
                                    .draw();
                            }
                        });
                    })
                },
                "buttons": [ 'excel', 'pdf']
            });
        });
    </script>

        <script src="js/classie.js"></script>
        <script src="js/gnmenu.js"></script>
        <script>
            new gnMenu( document.getElementById( 'gn-menu' ) );
        </script>
         <!-- /Modal Agregar Renta
            @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
            @version 23/02/2021
          -->
        <div id="addRentaModal" class="modal fade">
<?php
/*Se trae los municipios de la DB.
@author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
@version 23/02/2021
 */
$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_URL            => 'localhost:8080/rest/municipios',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING       => '',
    CURLOPT_MAXREDIRS      => 10,
    CURLOPT_TIMEOUT        => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST  => 'GET',
    CURLOPT_HTTPHEADER     => array(
        'Authorization: Bearer ' . $_SESSION['tokenGuardado'],
    ),
));
$responseMunicipios = curl_exec($curl);
curl_close($curl);
$municipios = json_decode($responseMunicipios, true);

$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_URL            => 'localhost:8080/rest/estados',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING       => '',
    CURLOPT_MAXREDIRS      => 10,
    CURLOPT_TIMEOUT        => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST  => 'GET',
    CURLOPT_HTTPHEADER     => array(
        'Authorization: Bearer ' . $_SESSION['tokenGuardado'],
    ),
));
$responseEstados = curl_exec($curl);
curl_close($curl);
$estados = json_decode($responseEstados, true);

$curl = curl_init();
/*Se trae la informacion de los Activos de la DB.
@author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
@version 23/02/2021
 */

curl_setopt_array($curl, array(
    CURLOPT_URL            => 'http://localhost:8080/rest/activos',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING       => '',
    CURLOPT_MAXREDIRS      => 10,
    CURLOPT_TIMEOUT        => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST  => 'GET',
    CURLOPT_HTTPHEADER     => array(
        'Authorization: Bearer ' . $_SESSION['tokenGuardado'],
    ),
));

$response = curl_exec($curl);
curl_close($curl);
$activos = json_decode($response, true);
?>
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="../controllerRenta/registroRentaUser.php" method="POST" id="formularioRenta">
                    <div class="modal-header">
                        <h4 class="modal-title">Nueva Renta</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label>Fecha Vencimiento</label>
                            <input name="txtFechaFin" type="date" class="form-control" id="fechaFin"  require value="">
                            <small class="error-text">Solo se permiten fechas.</small>
                            <input name="FechaFin" type="time" class="form-control" id="fechaFin"  require value="">
                            <small class="error-text">Solo se permiten horas.</small>
                        </div>
                        <div class="form-group">
                            <label>Dirección</label>
                            <input type="text" name="txtDireccion"  maxlength="200"  max="200" title="Solo permite 200 caracteres, letras y numeros" pattern="[A-Za-z0-9]+" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Estado</label>
                            <select name="estados" class="form-control">
                                <option>Seleccionar</option>
                            <?php foreach ($estados['Contenido'] as $estado) {?>
                                <option value="<?php echo $estado['idEstado'] ?>"><?php echo $estado['nomEstado'] ?></option>
                            <?php }?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Municipio</label>
                            <select name="municipios" class="form-control">
                                <option>Seleccionar</option>
                            <?php foreach ($municipios['Contenido'] as $municipio) {?>
                                <option value="<?php echo $municipio['idMunicipio'] ?>"><?php echo $municipio['nomMunicipio'] ?></option>
                            <?php }?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Activo</label>
                            <select name="activos" id="" class="form-control">
                            <option>Seleccionar</option>
                            <?php foreach ($activos['Contenido'] as $activo) {?>
                            <option class="form-control" value="<?php echo $activo['idActivo'] ?>"><?php echo $activo['placa'] ?></option>
                                <?php }?>
                        </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
                        <input type="submit" class="btn btn-success" id="submit-btn" value="Agregar">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /Modal Editar Renta
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
    -->
    <div id="editRentaModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="#">
                    <div class="modal-header">
                        <h4 class="modal-title">Editar Sensor</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>ID</label>
                            <input type="text" id="idRenta" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label>Fecha Vencimineto</label>
                            <input type="date" id="fechaVencimiento" class="form-control" required>
                            <input type="time" id="horaVencimiento" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Dirección</label>
                            <input type="text" id="direccion" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Estado</label>
                            <select name="txtrol" id="estado" class="form-control">
                                <option>Seleccionar</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Municipio</label>
                            <select name="txtrol" id="municipio" class="form-control">
                                <option>Seleccionar</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Usuario</label>
                            <select name="txtrol" id="usuario" class="form-control">
                                <option>Seleccionar</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Activo</label>
                            <select name="txtrol" id="activo" class="form-control">
                                <option>Seleccionar</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Renta Activa</label>
                            <input type="radio" name="habilitado" id="si" value="1"> si
                            </label>
                            <label>
                                <input type="radio" name="habilitado" id="no" value="0"> no
                            </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
                        <button class="btn btn-success" onclick="actualizarRenta();">Editar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /Modal Elimianr Renta
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
    -->
    <div id="deleteRentaModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>
                    <div class="modal-header">
                        <h4 class="modal-title">Eliminar Renta</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <input type="hidden" id="idRentae" name="">
                    <div class="modal-body">
                        <p>¿Está seguro de que desea eliminar este registro?</p>
                        <p class="text-warning"><small>Esta acción no se puede deshacer.</small></p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
                        <button class="btn btn-danger" onclick="eliminarRenta();">Borrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<!-- /Llenar los datos para actualizar la  renta Script
@author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
@version 23/02/2021
-->
<script type="text/javascript">
     var token='<?php echo $_SESSION['tokenGuardado']; ?>';
    function llenarForm(idRenta){
        var settings = {
      "url": "http://localhost:8080/rest/renta-usuario-detalle?idRenta=" + idRenta,
      "method": "GET",
      "dataType": 'json',
      "headers": {
        "Authorization": "Bearer " + token
  },
};
$.ajax(settings).done(function (response) {
  console.log(response);
  if (response['Respuesta'] == "Éxito") {
    $('#idRenta').val(idRenta);
    var fechaCompleta = response['Contenido'][0]['fechaVencimiento'].split(" ");
    $('#fechaVencimiento').val(fechaCompleta[0]);
    $('#horaVencimiento').val(fechaCompleta[1]);
    $('#direccion').val(response['Contenido'][0]['direccion']);


        var settings2 = {
                "url": "http://localhost:8080/rest/estados",
                "method": "GET",
                "dataType": 'json',
                "headers": {
                    "Authorization": "Bearer " + token
                },
            };

            $.ajax(settings2).done(function(response2) {
                $(response2['Contenido']).each(function(i, v) { // indice, valor
            if (response['Contenido'][0]['estado'] == v['nomEstado']) {
                $("#estado").append('<option value="' + v['idEstado'] + '" selected>' + v['nomEstado'] + '</option>');
            } else {
                $("#estado").append('<option value="' + v['idEstado'] + '">' + v['nomEstado'] + '</option>');
            }
            })

            });

            var settings3 = {
              "url": "http://localhost:8080/rest/municipios",
              "method": "GET",
              "dataType": 'json',
              "headers": {
                "Authorization": "Bearer " + token
              },
            };

            $.ajax(settings3).done(function (response3) {
              console.log(response3);
               $(response3['Contenido']).each(function(i, v) { // indice, valor
            if (response['Contenido'][0]['municipio'] == v['nomMunicipio']) {
                $("#municipio").append('<option value="' + v['idMunicipio'] + '" selected>' + v['nomMunicipio'] + '</option>');
            } else {
                $("#municipio").append('<option value="' + v['idMunicipio'] + '">' + v['nomMunicipio'] + '</option>');
            }
            })

            });
            var settings4 = {
              "url": "http://localhost:8080/rest/usuarios",
              "method": "GET",
              "dataType": 'json',
              "headers": {
                "Authorization": "Bearer " + token
              },
            };

            $.ajax(settings4).done(function (response4) {
              console.log(response);
            $(response4['Contenido']).each(function(i, v) { // indice, valor
            if (response['Contenido'][0]['email'] == v['email']) {
                $("#usuario").append('<option value="' + v['email'] + '" selected>' + v['nombreUsuario'] + '</option>');
            } else {
                $("#usuario").append('<option value="' + v['email'] + '">' + v['nombreUsuario'] + '</option>');
            }
            })

            });

            var settings5 = {
              "url": "http://localhost:8080/rest/activos",
              "method": "GET",
              "dataType": 'json',
              "headers": {
                "Authorization": "Bearer " + token
              },
            };

            $.ajax(settings5).done(function (response5) {
              console.log(response);
            $(response5['Contenido']).each(function(i, v) { // indice, valor
            if (response['Contenido'][0]['idActivo'] == v['idActivo']) {
                $("#activo").append('<option value="' + v['idActivo'] + '" selected>' + v['placa'] + '</option>');
            } else {
                $("#activo").append('<option value="' + v['idActivo'] + '">' + v['placa'] + '</option>');
            }
            })
            });

             if (response['Contenido'][0]['operandoRenta'] == 1) {
                    document.querySelector('#si').checked = true;
                } else {
                    document.querySelector('#no').checked = true;
                }

    }
});

    }
</script>
<!-- /Actualizar Renta Script
@author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
@version 23/02/2021
-->
<script type="text/javascript">
    var token='<?php echo $_SESSION['tokenGuardado']; ?>';
    function actualizarRenta(){
            var idRenta=$('#idRenta').val();
            var fechaVencimiento=$('#fechaVencimiento').val();
            var horaVencimiento=$('#horaVencimiento').val();
            var direccion=$('#direccion').val();
            var estado=$('#estado').val();
            var municipio=$('#municipio').val();
            var usuario=$('#usuario').val();
            var activo=$('#activo').val();
            var  select;
            if (document.querySelector('#si').checked == true) {
                  select=$('#si').val();
                }else{
                    select=$('#no').val();
                }
            var data=JSON.stringify({"idRenta":idRenta,"fechaVencimiento":fechaVencimiento +' '+horaVencimiento,"direccion": direccion,"idEstado":parseInt(estado),"idMunicipio":parseInt(municipio),"idActivo":parseInt(activo),"email":usuario,"operandoRenta":parseInt(select)});

        $.ajax({
        url: 'http://localhost:8080/rest/renta',
        type: 'PUT',
        data: data ,
        contentType:"application/json",
        async: false,
        headers: {
                    "Authorization": "Bearer " + token
                },
        success: function (msg) {
            var jsonData = JSON.parse(msg);
            alert(jsonData.Contenido);

        },
        error: function (error) {
            var jsonData = JSON.parse(error);
            alert(jsonData.Contenido);
        }
    });

    }
</script>

<!-- /Eliminar Renta Script
@author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
@version 23/02/2021
-->
 <script type="text/javascript">
        var token='<?php echo $_SESSION['tokenGuardado']; ?>';
        function copiarValor(idRenta){

            $('#idRentae').val(idRenta);
        }


        function eliminarRenta(){
            var idRenta = $('#idRentae').val();
            $.ajax({
                url: "http://localhost:8080/rest/renta?idRenta=" + idRenta,
                type: 'DELETE',
                contentType:"application/json",
                async: false,
                headers: {
                            "Authorization": "Bearer " + token
                        },
                success: function (msg) {
                    var jsonData = JSON.parse(msg);
                    alert(jsonData.Contenido);

                },
                error: function (error) {
                    var jsonData = JSON.parse(error);
                    alert(jsonData.Contenido);
                }
            });
}
</script>
<script defer="" src="js/validacionSensor.js"></script>
<!-- /Cerrar Pagina en 15 min Script
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
-->
 <div id="warning" style="display: none;" class="modal fade">
            <p>
                Tiempo para finalizar la sesion.
                <span>
                </span>
            </p>
            <p>
                <button id="close" type="button">
                    Close
                </button>
            </p>
        </div>

<script type="text/javascript">
var timeOfLastActivity;
var timeForCloseSession;
var timeBeforeWarning = 5000;   // 5 seconds
var timeBeforeClose = 900000;    // 15 min
var timeForUpdateWarning = 500; // 0.5 seconds

function resetAll() {
    timeOfLastActivity = new Date().getTime();
    timeForCloseSession = timeOfLastActivity + timeBeforeClose;
    setTimeout(function() {
        $("#warning").show();
        showTime();
    }, timeBeforeWarning);
    $("#warning").hide();
}

function showTime() {
    var currentTime = new Date().getTime();
    if (currentTime < timeForCloseSession) {
        $("#warning span").html(timeForCloseSession - currentTime)
    } else {
        $("#close").trigger("click");
    }
    if ($("#warning").is(":visible")) {
        setTimeout(showTime, timeForUpdateWarning);
    }
}

$("#close").click(function() {
    $("#warning").hide();
    location.href ="../index.php";
    $.post("/echo/json/"); // Send AJAX close
});


resetAll();
        </script>
</body>
</html>