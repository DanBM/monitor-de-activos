package com.eclipsemex.services.db;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.security.core.Authentication;

import com.eclipsemex.model.Municipio;
import com.eclipsemex.repository.MunicipioRepository;
import com.eclipsemex.security.entity.UsuarioPrincipal;
import com.eclipsemex.security.enums.Errores;
import com.eclipsemex.services.IMunicipioService;

/**
 * Clase de servicio para los Municipio
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@Service
@Primary
public class MunicipioServiceJpa implements IMunicipioService {

	@Autowired
	private MunicipioRepository repoMunicipio;
	private UsuarioPrincipal usuarioPrincipal;

	/**
	 * Obtiene todos los municipios
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@Override
	public String buscarTodos(Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		List<Object[]> municipiosDetalle = repoMunicipio.listaEstadosProcedure(emailUsuarioModificando);
		Object excepcion = (Object) municipiosDetalle.get(0)[0];
		if (excepcion.equals("1")) {
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.BAD_REQUEST);
		} else if (excepcion.equals("2")) {
			return respuestaJson("Error", Errores.No_Hay_Resultados, HttpStatus.NO_CONTENT);
		} else {
			List<Municipio> municipios = new LinkedList();
			for (Object[] objects : municipiosDetalle) {
				BigInteger idMunicipio = (BigInteger) objects[0];
				String nomMunicipio = (String) objects[1];
				Municipio mun = new Municipio(idMunicipio, nomMunicipio);
				municipios.add(mun);
			}
			return respuestaJson("Éxito", municipios, HttpStatus.OK);
		}
	}

	/**
	 * Genera un JSON de respuesta en String
	 * 
	 * @param respuesta que respuesta se requiere mostrar
	 * 
	 * @param contenido objeto que puede tener cualquier estructura para mostrar
	 * 
	 * @param codigo    codigo de respuesta que se desea enviar
	 * 
	 * @return JSON con valores enviados en formato string
	 */
	public String respuestaJson(String respuesta, Object contenido, HttpStatus codigo) {
		JSONObject objson = new JSONObject();
		objson.put("Respuesta", respuesta);
		objson.put("Contenido", contenido);
		objson.put("Codigo http", codigo);
		return objson.toString();
	}

}
