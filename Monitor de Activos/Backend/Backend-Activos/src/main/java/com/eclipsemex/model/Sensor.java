package com.eclipsemex.model;

import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Modelo que pertenece a la tabla de la base de datos rol
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@Entity
@Table(name = "cat_sensor")
public class Sensor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger idSensor;
	private String nomSensor;
	private int habilitado;

	/**
	 * Constructor.
	 * 
	 * @param idSensor   idSensor: id de un sensor.
	 * @param nomSensor  nomSensor: nombre de un sensor.
	 * @param habilitado habilitado: estado de un sensor.
	 */
	public Sensor(BigInteger idSensor, String nomSensor, int habilitado) {
		super();
		this.idSensor = idSensor;
		this.nomSensor = nomSensor;
		this.habilitado = habilitado;
	}

	public void setHabilitado(int habilitado) {
		this.habilitado = habilitado;
	}

	public BigInteger getIdSensor() {
		return idSensor;
	}

	public void setIdSensor(BigInteger idSensor) {
		this.idSensor = idSensor;
	}

	public String getNomSensor() {
		return nomSensor;
	}

	public void setNomSensor(String nomSensor) {
		this.nomSensor = nomSensor;
	}

	public Integer getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(Integer habilitado) {
		this.habilitado = habilitado;
	}

}
