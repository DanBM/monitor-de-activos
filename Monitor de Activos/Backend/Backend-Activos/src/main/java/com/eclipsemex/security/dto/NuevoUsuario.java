package com.eclipsemex.security.dto;

import java.util.HashSet;
import java.util.Set;

public class NuevoUsuario {

	private String nombreUsuario;
	private String appUsuario;
	private String apmUsuario;
	private String email;
	private String password;
	private String tel;
	private int operandoUsuario;
	private Set<String> roles = new HashSet<>();

	public NuevoUsuario(String nombreUsuario, String email, String tel, int operandoUsuario, Set<String> roles) {
		super();
		this.nombreUsuario = nombreUsuario;
		this.email = email;
		this.tel = tel;
		this.operandoUsuario = operandoUsuario;
		this.roles = roles;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getAppUsuario() {
		return appUsuario;
	}

	public void setAppUsuario(String appUsuario) {
		this.appUsuario = appUsuario;
	}

	public String getApmUsuario() {
		return apmUsuario;
	}

	public void setApmUsuario(String apmUsuario) {
		this.apmUsuario = apmUsuario;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public int getOperandoUsuario() {
		return operandoUsuario;
	}

	public void setOperandoUsuario(int operandoUsuario) {
		this.operandoUsuario = operandoUsuario;
	}

	public Set<String> getRoles() {
		return roles;
	}

	public void setRoles(Set<String> roles) {
		this.roles = roles;
	}

}
