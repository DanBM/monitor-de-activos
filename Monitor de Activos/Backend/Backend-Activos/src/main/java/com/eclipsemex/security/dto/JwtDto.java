package com.eclipsemex.security.dto;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Modelo para generar un token
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
public class JwtDto {
	private String token;
	private String bearer = "Bearer";
	private String email;
	private Collection<? extends GrantedAuthority> authorities;

	/**
	 * Constructor.
	 * 
	 * @param token         token: token de un usuario.
	 * @param nombreUsuario nombreUsuario: nombre de un usuario.
	 * @param authorities   authorities: Rol de un usuario.
	 */
	public JwtDto(String token, String nombreUsuario, Collection<? extends GrantedAuthority> authorities) {
		this.token = token;
		this.email = nombreUsuario;
		this.authorities = authorities;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getBearer() {
		return bearer;
	}

	public void setBearer(String bearer) {
		this.bearer = bearer;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
		this.authorities = authorities;
	}
}
