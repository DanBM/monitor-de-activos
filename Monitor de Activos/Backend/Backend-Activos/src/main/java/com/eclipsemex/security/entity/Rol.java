package com.eclipsemex.security.entity;

import com.eclipsemex.security.enums.RolNombre;
import com.sun.istack.NotNull;

import javax.persistence.*;

/**
 * Modelo que pertenece a la tabla de la base de datos rol
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@Entity
public class Rol {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idRol;
	@NotNull
	@Enumerated(EnumType.STRING)
	private RolNombre rolNombre;

	/**
	 * Constructor.
	 * 
	 * @param idRol     idRol: id de un rol.
	 * @param rolNombre rolNombre: nombre de un rol.
	 */
	public Rol(int idRol, RolNombre rolNombre) {
		super();
		this.idRol = idRol;
		this.rolNombre = rolNombre;
	}

	public Rol() {
	}

	public Rol(@NotNull RolNombre rolNombre) {
		this.rolNombre = rolNombre;
	}

	public int getIdRol() {
		return idRol;
	}

	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}

	public RolNombre getRolNombre() {
		return rolNombre;
	}

	public void setRolNombre(RolNombre rolNombre) {
		this.rolNombre = rolNombre;
	}
}
