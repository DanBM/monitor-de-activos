package com.eclipsemex.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.eclipsemex.model.Municipio;

/**
 * Repositorio de un Municipio que extiende de JPARepository y tiene como modelo
 * Municipio
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
public interface MunicipioRepository extends JpaRepository<Municipio, Integer> {
	/**
	 * Obtiene todos los Municipios por medio de un procedimiento almacenado
	 * 
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo
	 * @return lista de objetos de los Municipios encontrados
	 */
	@Query(value = "{call sp_obtener_municipio(:emailUsuarioModificandoIn)}", nativeQuery = true)
	List<Object[]> listaEstadosProcedure(@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);
}
