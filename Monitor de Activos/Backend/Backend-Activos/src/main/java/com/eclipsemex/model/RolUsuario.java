package com.eclipsemex.model;

import java.math.BigInteger;

/**
 * Modelo que pertenece a la tabla de la base de datos rol
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
public class RolUsuario {
	private BigInteger idRol;
	private String rolNombre;

	public BigInteger getIdRol() {
		return idRol;
	}

	public void setIdRol(BigInteger idRol) {
		this.idRol = idRol;
	}

	public String getRolNombre() {
		return rolNombre;
	}

	public void setRolNombre(String rolNombre) {
		this.rolNombre = rolNombre;
	}

	/**
	 * Constructor.
	 * 
	 * @param idRol     idRol: id de un rol.
	 * @param rolNombre rolNombre: nombre de un rol.
	 */
	public RolUsuario(BigInteger idRol, String rolNombre) {
		super();
		this.idRol = idRol;
		this.rolNombre = rolNombre;
	}

}
