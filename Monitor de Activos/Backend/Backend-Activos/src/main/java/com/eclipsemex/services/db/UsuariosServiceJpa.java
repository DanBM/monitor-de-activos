package com.eclipsemex.services.db;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;

import org.springframework.http.HttpStatus;

import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.eclipsemex.security.dto.NuevoUsuario;

import com.eclipsemex.security.entity.UsuarioPrincipal;
import com.eclipsemex.security.enums.Errores;
import com.eclipsemex.security.repository.UsuarioRepository;
import com.eclipsemex.security.service.UsuarioService;
import com.eclipsemex.services.IUsuariosService;

/**
 * Clase de servicio para los Usuarios
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@Service
@Primary
public class UsuariosServiceJpa implements IUsuariosService {

	@Autowired
	private UsuarioRepository repoUsuarios;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private UsuarioService usuarioService;
	private UsuarioPrincipal usuarioPrincipal;

	/**
	 * Obtiene todos los usuarios
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@Override
	public String buscarTodos(Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		List<Object[]> todosuUsuarios = repoUsuarios.listaUsuariosProcedure(emailUsuarioModificando);
		Object excepcion = (Object) todosuUsuarios.get(0)[0];
		if (excepcion.equals("1")) {
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.BAD_REQUEST);
		} else if (excepcion.equals("2")) {
			return respuestaJson("Error", Errores.No_Hay_Resultados, HttpStatus.NO_CONTENT);
		} else {
			List<NuevoUsuario> nUsuario = new LinkedList();
			for (Object[] objects : todosuUsuarios) {
				String nomUsuario = (String) objects[0] + " " + objects[1] + " " + objects[2];
				String email = (String) objects[3];
				String tel = (String) objects[4];
				int operandoUsuario = (int) objects[5];
				Set<String> rol = new HashSet<>();
				rol.add((String) objects[6]);
				NuevoUsuario nuevoUsuario = new NuevoUsuario(nomUsuario, email, tel, operandoUsuario, rol);
				nUsuario.add(nuevoUsuario);
			}
			return respuestaJson("Éxito", nUsuario, HttpStatus.OK);
		}
	}

	/**
	 * Obtiene el detalle de un usuario
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@Override
	public String usuarioDetalle(String emailidEmailUsuario, Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		if (emailidEmailUsuario.trim().isEmpty() || emailidEmailUsuario.trim() == null
				|| emailidEmailUsuario.trim().length() <= 0 || emailidEmailUsuario.trim().length() >= 50
				|| correoValido(emailidEmailUsuario.trim().replace(" +", "")) == false)
			return respuestaJson("Error", "El 'Email' debe estar lleno, formato valido y tener entre 1 y 50 caracteres",
					HttpStatus.BAD_REQUEST);
		List<Object[]> usuarioDetalle = repoUsuarios.usuarioDetalleProcedure(emailidEmailUsuario,
				emailUsuarioModificando);
		Object excepcion = (Object) usuarioDetalle.get(0)[0];
		if (excepcion.equals("1")) {
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.BAD_REQUEST);
		} else if (excepcion.equals("2")) {
			return respuestaJson("Error", Errores.No_Hay_Resultados, HttpStatus.NO_CONTENT);
		} else {
			List<NuevoUsuario> nUsuario = new LinkedList();
			for (Object[] objects : usuarioDetalle) {
				String nomUsuario = (String) objects[0] + " " + objects[1] + " " + objects[2];
				String email = (String) objects[3];
				String tel = (String) objects[4];
				int operandoUsuario = (int) objects[5];
				Set<String> rol = new HashSet<>();
				rol.add((String) objects[6]);
				NuevoUsuario nuevoUsuario = new NuevoUsuario(nomUsuario, email, tel, operandoUsuario, rol);
				nUsuario.add(nuevoUsuario);
			}
			return respuestaJson("Éxito", nUsuario, HttpStatus.OK);
		}
	}

	/**
	 * Agrega a un usuario.
	 * 
	 * @body nuevoUsuario representa un modelo para asignarle la informacion
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@Override
	public String registrarUsuario(NuevoUsuario nuevoUsuario, Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		if (nuevoUsuario.getNombreUsuario().trim().isEmpty() || nuevoUsuario.getNombreUsuario().trim() == null
				|| nuevoUsuario.getNombreUsuario().trim().length() <= 0
				|| nuevoUsuario.getNombreUsuario().trim().length() >= 50
				|| soloLetras(nuevoUsuario.getNombreUsuario().trim().replace(" +", "")) == false)
			return respuestaJson("Error",
					"El 'NombreUsuario' debe estar lleno, contener solo letras y tener entre 1 y 50 caracteres",
					HttpStatus.BAD_REQUEST);
		if (nuevoUsuario.getApmUsuario().trim().isEmpty() || nuevoUsuario.getApmUsuario().trim() == null
				|| nuevoUsuario.getApmUsuario().trim().length() <= 0
				|| nuevoUsuario.getApmUsuario().trim().length() >= 50
				|| soloLetras(nuevoUsuario.getApmUsuario().trim().replace(" +", "")) == false)
			return respuestaJson("Error",
					"El 'appusuario' debe estar lleno, contener solo letras y tener entre 1 y 50 caracteres",
					HttpStatus.BAD_REQUEST);
		if (nuevoUsuario.getApmUsuario().trim().isEmpty() || nuevoUsuario.getApmUsuario().trim() == null
				|| nuevoUsuario.getApmUsuario().trim().length() <= 0
				|| nuevoUsuario.getApmUsuario().trim().length() >= 50
				|| soloLetras(nuevoUsuario.getApmUsuario().trim().replace(" +", "")) == false)
			return respuestaJson("Error",
					"El 'appusuario' debe estar lleno, contener solo letras y tener entre 1 y 50 caracteres",
					HttpStatus.BAD_REQUEST);
		if (nuevoUsuario.getEmail().trim().isEmpty() || nuevoUsuario.getEmail().trim() == null
				|| nuevoUsuario.getEmail().trim().length() <= 0 || nuevoUsuario.getEmail().trim().length() >= 50
				|| correoValido(nuevoUsuario.getEmail().trim().replace(" +", "")) == false)
			return respuestaJson("Error",
					"El 'Email' debe estar lleno, formato valido 'ejemplo@ejemplo.com' y tener entre 1 y 50 caracteres",
					HttpStatus.BAD_REQUEST);
		if (nuevoUsuario.getPassword().trim().isEmpty() || nuevoUsuario.getPassword().trim() == null
				|| nuevoUsuario.getPassword().trim().length() <= 0 || nuevoUsuario.getPassword().trim().length() >= 50)
			return respuestaJson("Error",
					"El 'password' debe estar lleno, contener solo letras y tener entre 1 y 50 caracteres",
					HttpStatus.BAD_REQUEST);
		if (nuevoUsuario.getTel().trim().length() <= 0 || nuevoUsuario.getTel().trim().length() > 10
				|| soloNumeros(nuevoUsuario.getTel().replace(" +", "")) == false)
			return respuestaJson("Error",
					"El 'telefono' debe estar lleno, contener solo numeros y tener entre 1 y 10 caracteres",
					HttpStatus.BAD_REQUEST);
		if (nuevoUsuario.getOperandoUsuario() < 0 || nuevoUsuario.getOperandoUsuario() > 1
				|| soloNumeros(String.valueOf(nuevoUsuario.getOperandoUsuario())) == false)
			return respuestaJson("Error",
					"El campo 'operandoUsuario' debe estar lleno, contener solo numeros y debe ser 0 o 1",
					HttpStatus.BAD_REQUEST);
		if (!usuarioService.existsByEmail(nuevoUsuario.getEmail())) {
			int status = repoUsuarios.registrarUsuarioProcedure(nuevoUsuario.getNombreUsuario().replace(" +", ""),
					nuevoUsuario.getAppUsuario().replace(" +", ""), nuevoUsuario.getApmUsuario().replace(" +", ""),
					nuevoUsuario.getEmail(), passwordEncoder.encode(nuevoUsuario.getPassword()), nuevoUsuario.getTel(),
					nuevoUsuario.getOperandoUsuario(), nuevoUsuario.getRoles().toArray()[0].toString(),
					emailUsuarioModificando);
			if (status == 1)
				return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.BAD_REQUEST);
			if (status == 2)
				return respuestaJson("Error",
						Errores.No_Se_Encontro_EL_Rol
								+ " debe coincidir con algún formato como: ROLE_ADMIN, ROLE_ADMIN, ROLE_USER",
						HttpStatus.BAD_REQUEST);
			if (status == 3)
				return respuestaJson("Error", Errores.Usuario_Agregado_No_Se_Encontro, HttpStatus.NO_CONTENT);
			return respuestaJson("Éxito", "Usuario agregado con éxito.", HttpStatus.OK);
		}
		return respuestaJson("Error", Errores.Usuario_Repetido, HttpStatus.NOT_ACCEPTABLE);
	}

	/**
	 * Modifica a un usuario.
	 * 
	 * @body nuevoUsuario representa un modelo para asignarle la informacion
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@Override
	public String actualizarUsuario(NuevoUsuario nuevoUsuario, Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		if (nuevoUsuario.getNombreUsuario().trim().isEmpty() || nuevoUsuario.getNombreUsuario().trim() == null
				|| nuevoUsuario.getNombreUsuario().trim().length() <= 0
				|| nuevoUsuario.getNombreUsuario().trim().length() >= 50
				|| soloLetras(nuevoUsuario.getNombreUsuario().trim().replace(" +", "")) == false)
			return respuestaJson("Error",
					"El 'NombreUsuario' debe estar lleno, contener solo letras y tener entre 1 y 50 caracteres",
					HttpStatus.BAD_REQUEST);
		if (nuevoUsuario.getApmUsuario().trim().isEmpty() || nuevoUsuario.getApmUsuario().trim() == null
				|| nuevoUsuario.getApmUsuario().trim().length() <= 0
				|| nuevoUsuario.getApmUsuario().trim().length() >= 50
				|| soloLetras(nuevoUsuario.getApmUsuario().trim().replace(" +", "")) == false)
			return respuestaJson("Error",
					"El 'appusuario' debe estar lleno, contener solo letras y tener entre 1 y 50 caracteres",
					HttpStatus.BAD_REQUEST);
		if (nuevoUsuario.getApmUsuario().trim().isEmpty() || nuevoUsuario.getApmUsuario().trim() == null
				|| nuevoUsuario.getApmUsuario().trim().length() <= 0
				|| nuevoUsuario.getApmUsuario().trim().length() >= 50
				|| soloLetras(nuevoUsuario.getApmUsuario().trim().replace(" +", "")) == false)
			return respuestaJson("Error",
					"El 'appusuario' debe estar lleno, contener solo letras y tener entre 1 y 50 caracteres",
					HttpStatus.BAD_REQUEST);
		if (nuevoUsuario.getEmail().trim().isEmpty() || nuevoUsuario.getEmail().trim() == null
				|| nuevoUsuario.getEmail().trim().length() <= 0 || nuevoUsuario.getEmail().trim().length() >= 50
				|| correoValido(nuevoUsuario.getEmail().trim().replace(" +", "")) == false)
			return respuestaJson("Error", "El 'Email' debe estar lleno, formato valido y tener entre 1 y 50 caracteres",
					HttpStatus.BAD_REQUEST);
		if (nuevoUsuario.getTel().trim().length() <= 0 || nuevoUsuario.getTel().trim().length() >= 11
				|| soloNumeros(nuevoUsuario.getTel().replace(" +", "")) == false)
			return respuestaJson("Error",
					"El 'telefono' debe estar lleno, contener solo numeros y tener entre 1 y 10 caracteres",
					HttpStatus.BAD_REQUEST);
		if (nuevoUsuario.getOperandoUsuario() < 0 || nuevoUsuario.getOperandoUsuario() > 1
				|| soloNumeros(String.valueOf(nuevoUsuario.getOperandoUsuario())) == false)
			return respuestaJson("Error",
					"El campo 'operandoUsuario' debe estar lleno, contener solo letras y tener entre 1 y 50 caracteres",
					HttpStatus.BAD_REQUEST);
		int status = repoUsuarios.actualizarUsuarioProcedure(nuevoUsuario.getNombreUsuario().replace(" +", ""),
				nuevoUsuario.getAppUsuario().replace(" +", ""), nuevoUsuario.getApmUsuario().replace(" +", ""),
				nuevoUsuario.getEmail(), nuevoUsuario.getTel(), nuevoUsuario.getOperandoUsuario(),
				nuevoUsuario.getRoles().toArray()[0].toString(), emailUsuarioModificando);
		if (status == 1)
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.BAD_REQUEST);
		if (status == 2)
			return respuestaJson("Error", Errores.Usuario_No_Encontrado, HttpStatus.NO_CONTENT);
		if (status == 3)
			return respuestaJson("Error",
					Errores.No_Se_Encontro_EL_Rol
							+ " debe coincidir con algún formato como: ROLE_ADMIN, ROLE_ADMIN, ROLE_USER",
					HttpStatus.NO_CONTENT);
		return respuestaJson("Éxito", "Usuario Modificado con éxito.", HttpStatus.OK);
	}

	/**
	 * Elimina a un usuario.
	 * 
	 * @param emailUsuario representa un correo de un usuario
	 * 
	 * @param auth         Representa el token para una solicitud de autenticación
	 *                     no se manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@Override
	public String eliminarUsuario(String emailidEmailUsuario, Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		if (emailidEmailUsuario.trim().isEmpty() || emailidEmailUsuario.trim() == null
				|| emailidEmailUsuario.trim().length() <= 0 || emailidEmailUsuario.trim().length() >= 50
				|| correoValido(emailidEmailUsuario.trim().replace(" +", "")) == false)
			return respuestaJson("Error", "El 'Email' debe estar lleno, formato valido y tener entre 1 y 50 caracteres",
					HttpStatus.BAD_REQUEST);
		int status = repoUsuarios.usuarioEliminarProcedure(emailidEmailUsuario, emailUsuarioModificando);
		if (status == 1)
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.BAD_REQUEST);
		if (status == 2)
			return respuestaJson("Error", Errores.Usuario_No_Encontrado, HttpStatus.NO_CONTENT);
		return respuestaJson("Éxito", "Usuario Eliminado", HttpStatus.OK);
	}

	/**
	 * Modifica la contraseña de un usuario.
	 * 
	 * @param emailUsuario representa un correo de un usuario
	 * 
	 * @param password     representa una contraseña de un usuario
	 * 
	 * @param auth         Representa el token para una solicitud de autenticación
	 *                     no se manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@Override
	public String actualizarPasswordUsuario(String email, String password, Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();

		if (email.trim().isEmpty() || email.trim() == null || email.trim().length() <= 0 || email.trim().length() >= 50
				|| correoValido(email.trim().replace(" +", "")) == false)
			return respuestaJson("Error", "El 'Email' debe estar lleno, formato valido y tener entre 1 y 50 caracteres",
					HttpStatus.BAD_REQUEST);
		if (password.trim().isEmpty() || password.trim() == null || password.trim().length() <= 0
				|| password.trim().length() >= 50)
			return respuestaJson("Error", "El campo 'password' debe estar lleno, formato valido",
					HttpStatus.BAD_REQUEST);
		int status = repoUsuarios.cambiarPasswordUsuarioProcedure(email, passwordEncoder.encode(password),
				emailUsuarioModificando);
		if (status == 1)
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.BAD_REQUEST);
		if (status == 2)
			return respuestaJson("Error", Errores.Usuario_No_Encontrado, HttpStatus.NO_CONTENT);
		return respuestaJson("Éxito", "Password Modificada con exito", HttpStatus.OK);

	}

	/**
	 * Genera un JSON de respuesta en String
	 * 
	 * @param respuesta que respuesta se requiere mostrar
	 * 
	 * @param contenido objeto que puede tener cualquier estructura para mostrar
	 * 
	 * @param codigo    codigo de respuesta que se desea enviar
	 * 
	 * @return JSON con valores enviados en formato string
	 */
	public String respuestaJson(String respuesta, Object contenido, HttpStatus codigo) {
		JSONObject objson = new JSONObject();
		objson.put("Respuesta", respuesta);
		objson.put("Contenido", contenido);
		objson.put("Codigo http", codigo);
		return objson.toString();
	}

	/**
	 * Valida que una cadena tenga solo letras
	 * 
	 * @param cadena que respuesta se requiere mostrar
	 * 
	 * @return true o false si el correo es valido o incorrecto segun su caso
	 */
	public boolean soloLetras(String cadena) {
		Pattern pat = Pattern.compile("[ A-Za-zäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓ	ÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙñÑ.-]+");
		Matcher mat = pat.matcher(cadena);
		return mat.matches();
	}

	/**
	 * Valida que una cadena tenga solo numeros
	 * 
	 * @param cadena que respuesta se requiere mostrar
	 * 
	 * @return true o false si el correo es valido o incorrecto segun su caso
	 */
	private boolean soloNumeros(String cadena) {
		if (cadena.matches("[0-9]*")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Valida que un correo tenga un formato correcto
	 * 
	 * @param cadena que respuesta se requiere mostrar
	 * 
	 * @return true o false si el correo es valido o incorrecto segun su caso
	 */
	public boolean correoValido(String cadena) {
		Pattern pat = Pattern.compile(
				"^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");
		Matcher mat = pat.matcher(cadena);
		return mat.matches();
	}
}
