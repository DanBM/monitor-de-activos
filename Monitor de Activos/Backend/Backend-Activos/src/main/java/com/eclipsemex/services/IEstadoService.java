package com.eclipsemex.services;

import org.springframework.security.core.Authentication;

/**
 * Interfaz donde se definen los metodos de un Estado
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
public interface IEstadoService {
	/**
	 * Obtiene todos los estados
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	String buscarTodos(Authentication auth);

}
