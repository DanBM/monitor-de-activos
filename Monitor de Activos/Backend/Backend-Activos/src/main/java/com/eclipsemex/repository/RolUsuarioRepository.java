package com.eclipsemex.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.eclipsemex.security.entity.Rol;

/**
 * Repositorio de un rol que extiende de JPARepository y tiene como modelo rol
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
public interface RolUsuarioRepository extends JpaRepository<Rol, Integer> {
	/**
	 * Obtiene todos los roles por medio de un procedimiento almacenado.
	 * 
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo.
	 * 
	 * @return lista de objetos de los roles.
	 */
	@Query(value = "{call sp_obtener_roles(:emailUsuarioModificandoIn)}", nativeQuery = true)
	List<Object[]> listaRolesProcedure(@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);

}
