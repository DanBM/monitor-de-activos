<?php
session_start();

/*Se verefica que el rol ingresado tenga el permiso para estar en esta pagania o pestaña.
@author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
@version 23/02/2021
 */
$views = [
    'gerente' => 'vistaGerente.html',
    'usuario' => 'vistaGeneral.html',
    'index'   => '../index.php',
];
if ($_SESSION['tokenGuardado'] == '') {
    header('Location: ' . $views['index']);
} else {
    if ($_SESSION['rol'] == 'ROLE_GERENTE') {
        header('Location: ' . $views['gerente']);

    }if ($_SESSION['rol'] == 'ROLE_USER') {
        header('Location: ' . $views['usuario']);

    }
}

$curl = curl_init();
/* cambiar por el token que se necesite y como se haya guardado, este sempre tiene que ser dinamico
@author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
@version 23/02/2021
 */
curl_setopt_array($curl, array(
    CURLOPT_URL            => 'http://localhost:8080/rest/usuarios',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING       => '',
    CURLOPT_MAXREDIRS      => 10,
    CURLOPT_TIMEOUT        => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST  => 'GET',
    CURLOPT_HTTPHEADER     => array(
        'Authorization: Bearer ' . $_SESSION['tokenGuardado'],
    ),
));

$response = curl_exec($curl);
curl_close($curl);
$dataToJson = json_decode($response, true);

?>


<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Tabla Usuario</title>
        <meta name="description" content="A sidebar menu as seen on the Google Nexus 7 website" />
        <meta name="keywords" content="google nexus 7 menu, css transitions, sidebar, side menu, slide out menu" />
        <meta name="author" content="Codrops" />
        <link rel="shortcut icon" href="../favicon.ico">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.2/css/all.css" integrity="sha384-vSIIfh2YWi9wW0r9iZe7RJPrKwp6bG+s9QZMoITbCckVJqGCCRhc+ccxNcdpHuYu" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/validacion.css" />
        <link rel="stylesheet" type="text/css" href="css/normalize.css" />
        <link rel="stylesheet" type="text/css" href="css/demo.css" />
        <link rel="stylesheet" type="text/css" href="css/component.css" />
         <!-- Librerias para la tabla -->
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js"></script>

    <!-- Datatables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/v/bs4-4.1.1/dt-1.10.18/datatables.min.css">
    <script src="https://cdn.datatables.net/v/bs4-4.1.1/dt-1.10.18/datatables.min.js"></script>

    <!-- Buttons -->
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.53/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.53/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="js/modernizr.custom.js"></script>

    </head>
    <body>
        <div class="container">
            <ul id="gn-menu" class="gn-menu-main">
                <li class="gn-trigger">
                    <a class="gn-icon gn-icon-menu"><span>Menú</span></a>
                    <nav class="gn-menu-wrapper">
                        <div class="gn-scroller">
                            <ul class="gn-menu">

                                <li>
                                    <a href="salir.php" class="gn-icon gn-icon-downloa">Salir</a>
                                    <ul class="gn-submenu">
                                        <li><a href="tablaUsuario.php" class="gn-icon gn-icon-usuer">Usuarios</a></li>
                                        <li><a href="tablaActivoAdmin.php" class="gn-icon gn-icon-photosh">Activos</a></li>
                                    </ul>
                                </li>
                                <li><a href="tablaMonitoreoAdmin.php" class="gn-icon gn-icon-cog">Monitoreo</a></li>
                                <li><a href="tablaSensoresAdmin.php" class="gn-icon gn-icon-help">Sensores</a></li>
                                <li>
                                    <a href="tablaRentasAdmin.php" class="gn-icon gn-icon-archive">Rentas</a>

                                </li>
                            </ul>
                        </div><!-- /gn-scroller -->
                    </nav>
                </li>
                <li><a href="">Sistema de Monitoreo de Activos</a></li>
                <li><a class="codrops-icon codrops-icon-" href="#addEmployeeModal" data-toggle="modal"><span>Nuevo Usuario</span></a></li>
                <li><a class="codrops-icon codrops-icon-prev" href="vistaAdministrador.html"><span> Home</span></a></li>
            </ul>
             <br> <br> <br>

        <!-- /Scrip para los modales
            @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
            @version 23/02/2021
         -->

        <script type="text/javascript">
    $(document).ready(function(){
    // Activate tooltip
    $('[data-toggle="tooltip"]').tooltip();

    // Select/Deselect checkboxes
    var checkbox = $('table tbody input[type="checkbox"]');
    $("#selectAll").click(function(){
        if(this.checked){
            checkbox.each(function(){
                this.checked = true;
            });
        } else{
            checkbox.each(function(){
                this.checked = false;
            });
        }
    });
    checkbox.click(function(){
        if(!this.checked){
            $("#selectAll").prop("checked", false);
        }
    });
});
</script>


</div><!-- /container -->
<!-- /Contiene el mensaje de registro de un usuario
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
 -->
<?php
if (isset($_SESSION['msg'])) {
    echo '<script type="text/javascript">
    alert("' . $_SESSION['msg'] . '");
    </script>';
}

unset($_SESSION['msg']);

?>

<section class="contenidoTabla">
    <div class="table-responsive" id="mydatatable-container">
        <p id="respuesta"></p>
        <table class="records_list table table-striped table-bordered table-hover" id="mydatatable">
            <thead>
              <tr>
                <th scope="col">Nombre</th>
                <th scope="col">Email</th>
                <th scope="col">Teléfono</th>
                <th scope="col">Rol</th>
                <th scope="col">Activo</th>
                <th scope="col">Acciones</th>
              </tr>
            </thead>
             <tfoot>
                <tr>
                    <th>Filter..</th>
                    <th>Filter..</th>
                    <th>Filter..</th>
                    <th>Filter..</th>
                    <th>Filter..</th>
                    <th>Filter..</th>

                </tr>
            </tfoot>
            <tbody>
                <!-- /Se trae la informacion de los usuario de la DB.
                 @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
                 @version 23/02/2021
                -->

                <?php foreach ($dataToJson['Contenido'] as $usuario) {
    ?>
              <tr>
                <td><?php echo $usuario['nombreUsuario'] ?></td>
                <td><?php echo $usuario['email'] ?></td>
                <td><?php echo $usuario['tel'] ?></td>
                <?php
if ($usuario['roles'][0] == 'ROLE_ADMIN') {
        ?>
                        <td>Administrador</td>
                        <?php

    } elseif ($usuario['roles'][0] == 'ROLE_GERENTE') {
        ?>
                        <td>Gerente</td>
                        <?php
} elseif ($usuario['roles'][0] == 'ROLE_USER') {
        ?>
                        <td>General</td>
                        <?php
}
    ?>
                <td><?php echo ($usuario['operandoUsuario'] == 1) ? 'Si' : 'No' ?></td>


                 <td>


                    <a data-toggle="modal" href="#deleteEmployeeModal" onclick="copiarValor('<?php echo $usuario['email']; ?>');"><i class="fas fa-trash-alt"></i></a>
                    <a  data-toggle="modal" href="#editEmployeeModal" onclick="editarForm('<?php echo $usuario['email']; ?>');"><i class="fas fa-edit"></i></a>
                    <a data-toggle="modal" href="#passwordModal" onclick="copiarValor('<?php echo $usuario['email']; ?>');"><i class="fas fa-key"></i></a>

                </td>
              </tr>
                <?php }?>
            </tbody>
        </table>
    </div>

</section>
 <!-- /Scrip para la tabla y filtros de la misma.
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
-->

    <script type="text/javascript">
        $(document).ready(function () {
            $('#mydatatable tfoot th').each(function () {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Filtrar.." />');
            });

            var table = $('#mydatatable').DataTable({
                "dom": 'B<"float-left"i><"float-right"f>t<"float-left"l><"float-right"p><"clearfix">',
                "responsive": false,
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                },
                "order": [
                    [0, "desc"]
                ],
                "initComplete": function () {
                    this.api().columns().every(function () {
                        var that = this;

                        $('input', this.footer()).on('keyup change', function () {
                            if (that.search() !== this.value) {
                                that
                                    .search(this.value)
                                    .draw();
                            }
                        });
                    })
                },
                "buttons": [ 'excel', 'pdf']
            });
        });
    </script>

        <script src="js/classie.js"></script>
        <script src="js/gnmenu.js"></script>
        <script>
            new gnMenu( document.getElementById( 'gn-menu' ) );
        </script>
        <!-- /Modal Agregar USUARIO
            @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
            @version 23/02/2021
        -->
        <?php
$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL            => 'localhost:8080/rest/roles',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING       => '',
    CURLOPT_MAXREDIRS      => 10,
    CURLOPT_TIMEOUT        => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST  => 'GET',
    CURLOPT_HTTPHEADER     => array(
        'Authorization: Bearer ' . $_SESSION['tokenGuardado'],
    ),
));

$responseRoles = curl_exec($curl);

curl_close($curl);
$roles = json_decode($responseRoles, true);
?>


        <div id="addEmployeeModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="../controllerUser/registroUsuario.php" method="POST" id="formularioUsu">
                    <div class="modal-header">
                        <h4 class="modal-title">Nuevo Usuario</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" name="txtnombre" id="txtnombre" minlength="5" maxlength="40" max="40" title="Solo permite letras." pattern="[A-Za-z]+"   class="form-control" required>
                            <small class="error-text">Solo permite letras.</small>
                        </div>
                        <div class="form-group">
                            <label>Apellido Paterno</label>
                            <input type="text" name="txtapp" minlength="5" maxlength="40"  max="40" title="Solo permite letras." pattern="[A-Za-z]+"  class="form-control" required>
                            <small class="error-text">Solo permite letras.</small>
                        </div>
                        <div class="form-group">
                            <label>Apellido Materno</label>
                            <input type="text" name="txtapm" minlength="5" maxlength="40" max="40" title="Solo permite letras." pattern="[A-Za-z]+" class="form-control" required>
                            <small class="error-text">Solo permite letras.</small>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="txtemail" minlength="12" maxlength="60" max="60" title="Solo permite correos de 12 a 60 caracteres" class="form-control" required>
                            <small class="error-text">Solo permite correos de 12 a 60 caracteres, debe ser un email valido ejemplo@ejemplo.com</small>
                        </div>
                         <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="txtpassword" minlength="8" maxlength="16" max="16" title="Solo permite contraseñas de 8 a 16 caracteres" class="form-control"  required>
                            <small class="error-text">Solo permite contraseñas de 8 a 16 caracteres, minimo una mayuscula, un caracter especial "#, $, %, =, ?" y un numero</small>
                        </div>
                        <div class="form-group">
                            <label>Teléfono</label>
                            <input type="text" name="txttel" minlength="8" maxlength="10" max="10" pattern="[0-9]+" title="Solo permite numeros, máximo 10 caracteres " class="form-control"  required>
                            <small class="error-text">Solo permite numeros, máximo 10 caracteres.</small>
                        </div>
                        <div class="form-group">
                            <label>Rol</label>
                            <select name="txtrol" id="rol" class="form-control">
                                            <option>Seleccionar</option>
                                            <?php foreach ($roles['Contenido'] as $rol) {
    ?>
                                            <?php
if ($rol['rolNombre'] == 'ROLE_ADMIN') {
        ?>
                                                    <option class="form-control" value="<?php echo $rol['rolNombre'] ?>">Administrador</option>
                                                    <?php

    } elseif ($rol['rolNombre'] == 'ROLE_GERENTE') {
        ?>
                                                    <option class="form-control" value="<?php echo $rol['rolNombre'] ?>">Gerente</option>
                                                    <?php
} elseif ($rol['rolNombre'] == 'ROLE_USER') {
        ?>
                                                    <option class="form-control" value="<?php echo $rol['rolNombre'] ?>">General</option>
                                                    <?php
}
    ?>
                                            <?php }?>

                                        </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
                        <input type="submit" class="btn btn-success" id="submit-btn" value="Agregar">
                    </div>
                </form>
            </div>
        </div>
    </div>

<!-- /Modal Editar USUARIO
@author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
@version 23/02/2021
-->
    <div id="editEmployeeModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="formularioUsu" action="#" method="POST" name="formEditar">
                    <div class="modal-header">
                        <h4 class="modal-title">Editar Usuario</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text"  name="txtnombre" minlength="5" maxlength="40" max="40" title="Solo permite letras." pattern="[A-Za-z]+" class="form-control" id="nomUsuario" required>
                            <small class="error-text">Solo permite letras.</small>
                        </div>
                        <div class="form-group">
                            <label>Apellido Paterno</label>
                            <input type="text" name="txtapp" class="form-control"  minlength="5" maxlength="40"  max="40" title="Solo permite letras." pattern="[A-Za-z]+" id="appUsuario" required>
                            <small class="error-text">Solo permite letras.</small>
                        </div>
                        <div class="form-group">
                            <label>Apellido Materno</label>
                            <input type="text" name="txtapm" class="form-control"  minlength="5" maxlength="40" max="40" title="Solo permite letras." pattern="[A-Za-z]+" id="apmUsuario" required>
                            <small class="error-text">Solo permite letras.</small>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="txtemail" class="form-control"  id="emailUsuario" readonly>
                        </div>
                        <div class="form-group">
                            <label>Teléfono</label>
                            <input type="text"  name="txttel"  minlength="8" maxlength="10" max="10" pattern="[0-9]+" title="Solo permite numeros, máximo 10 caracteres " class="form-control" id="telUsuario" required>
                             <small class="error-text">Solo permite numeros, máximo 10 caracteres.</small>

                        </div>
                         <div class="form-group">
                            <label>Activo</label>
                            <label>
                            <input type="radio" name="activo" id="si" value="1"> si
                            </label>
                            <label>
                                <input type="radio" name="activo" id="no" value="0"> no
                            </label>
                        </div>
                        <div class="form-group">
                            <label>Rol</label>
                            <select name="txtrol" id="rolUsuario" class="form-control">
                                <option>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
                        <button class="btn btn-success"  id="submit-btn" onclick="actualizarUsuario();">Editar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- /Modal Editar Password USUARIO
        @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
        @version 23/02/2021
    -->

    <div id="passwordModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="formularioUsu"  method="POST">
                    <div class="modal-header">
                        <h4 class="modal-title">Editar Password</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="txtemail" id="correoUsuario" minlength="12" maxlength="60" max="60" title="Solo permite correos de 12 a 60 caracteres" class="form-control" readonly="" required>
                            <small class="error-text">Solo permite correos de 12 a 60 caracteres, debe ser un email valido ejemplo@ejemplo.com</small>
                        </div>

                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="txtpassword" id="passwordUsu" minlength="8" maxlength="16" max="16" title="Solo permite contraseñas de 8 a 16 caracteres" class="form-control"  required>
                            <small class="error-text">Solo permite contraseñas de 8 a 16 caracteres</small>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
                        <button class="btn btn-success"  id="submit-btn" onclick="actualizarPassword();">Editar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- /Modal Eliminar USUARIO
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
    -->
    <div id="deleteEmployeeModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>
                    <div class="modal-header">
                        <h4 class="modal-title">Eliminar Usuario</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <input type="hidden" id="correoUsuario">
                    <div class="modal-body">
                        <p>¿Está seguro de que desea eliminar este registro?</p>
                        <p class="text-warning"><small>Esta acción no se puede deshacer.</small></p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
                       <button class="btn btn-danger" onclick="eliminarUsuario();">Borrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<!-- /Editar Usuario, Llenar la informacion del usuario. Script
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
-->
    <script type="text/javascript">
        var token='<?php echo $_SESSION['tokenGuardado']; ?>';
        function editarForm(email){
            var settingsUsDet = {
            "url": "http://localhost:8080/rest/usuario-detalle?emailUsuario=" + email,
            "method": "GET",
            "dataType": 'json',
            "headers": {
                "Authorization": "Bearer " + token
            },
        };
        $.ajax(settingsUsDet).done(function(response) {
            if (response['Respuesta'] == "Éxito") {
                var nombreCompleto = response['Contenido'][0]['nombreUsuario'].split(" ");;
                $('#nomUsuario').val(nombreCompleto[0]);
                $('#appUsuario').val(nombreCompleto[1]);
                $('#apmUsuario').val(nombreCompleto[2]);
                $('#emailUsuario').val(response['Contenido'][0]['email']);
                $('#telUsuario').val(response['Contenido'][0]['tel']);
                var settingsToRo = {
                    "url": "http://localhost:8080/rest/roles",
                    "method": "GET",
                    "dataType": 'json',
                    "headers": {
                        "Authorization": "Bearer " + token
                    },
                };
                $.ajax(settingsToRo).done(function(response2) {
                     $("#rolUsuario").empty();
                    if (response2['Respuesta'] == "Éxito") {
                        $(response2['Contenido']).each(function(i, v) { // indice, valor
                            if (response['Contenido'][0]['roles'][0] == v['rolNombre']) {
                                if (v['rolNombre'] == "ROLE_ADMIN") {
                                    $("#rolUsuario").append('<option value="' + v['rolNombre'] + '" selected>Administrador</option>');
                                } else if (v['rolNombre'] == "ROLE_GERENTE") {
                                    $("#rolUsuario").append('<option value="' + v['rolNombre'] + '" selected>Gerente</option>');
                                } else if (v['rolNombre'] == "ROLE_USER") {
                                    $("#rolUsuario").append('<option value="' + v['rolNombre'] + '" selected>General</option>');
                                }
                            } else {
                                if (v['rolNombre'] == "ROLE_ADMIN") {
                                    $("#rolUsuario").append('<option value="' + v['rolNombre'] + '">Administrador</option>');
                                } else if (v['rolNombre'] == "ROLE_GERENTE") {
                                    $("#rolUsuario").append('<option value="' + v['rolNombre'] + '">Gerente</option>');
                                } else if (v['rolNombre'] == "ROLE_USER") {
                                    $("#rolUsuario").append('<option value="' + v['rolNombre'] + '">General</option>');
                                }
                            }
                        })
                    }
                });
                if (response['Contenido'][0]['operandoUsuario'] == 1) {
                    document.querySelector('#si').checked = true;
                } else {
                    document.querySelector('#no').checked = true;
                }
            }
        });
        }
    </script>

    </script>
    <!-- /Editar Usuario Script
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
    -->

    <script type="text/javascript">
        var token='<?php echo $_SESSION['tokenGuardado']; ?>';
        function actualizarUsuario(){
            var nombreUsuario=$('#nomUsuario').val();
            var appUsuario=$('#appUsuario').val();
            var apmUsuario=$('#apmUsuario').val();
            var emailUsuario=$('#emailUsuario').val();
            var telUsuario=$('#telUsuario').val();
            var  select;
            if (document.querySelector('#si').checked == true) {
                  select=$('#si').val();
                }else{
                    select=$('#no').val();
                }
            var rol=[$('#rolUsuario').val()];
            var data=JSON.stringify({"nombreUsuario":nombreUsuario,"appUsuario":appUsuario,"apmUsuario":apmUsuario,"email":emailUsuario,"tel":telUsuario,"operandoUsuario":select,"roles":rol});

        $.ajax({
        url: 'http://localhost:8080/rest/usuario',
        type: 'PUT',
        data: data ,
        contentType:"application/json",
        async: false,
        headers: {
                    "Authorization": "Bearer " + token
                },
        success: function (msg) {
            var jsonData = JSON.parse(msg);
            alert(jsonData.Contenido);

        },
        error: function (error) {
            var jsonData = JSON.parse(error);
            alert(jsonData.Contenido);
        }
    });


        }

    </script>
     <!-- /Eliminar Usuario Script
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
    -->
    <script type="text/javascript">
        var token='<?php echo $_SESSION['tokenGuardado']; ?>';
        function copiarValor(email){

            $('#correoUsuario').val(email);
        }

        function eliminarUsuario(){
            var email = $('#correoUsuario').val();
        $.ajax({
        url: "http://localhost:8080/rest/usuario?emailUsuario="+email,
        type: 'DELETE',
        contentType:"application/json",
        async: false,
        headers: {
                    "Authorization": "Bearer " + token
                },
        success: function (msg) {
            var jsonData = JSON.parse(msg);
            alert(jsonData.Contenido);

        },
        error: function (error) {
            var jsonData = JSON.parse(error);
            alert(jsonData.Contenido);
        }
    });
        }
    </script>
    <!-- /Editar Password Script
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
    -->
    <script type="text/javascript">
        var token='<?php echo $_SESSION['tokenGuardado']; ?>';
        function actualizarPassword(){
        var correo=$('#correoUsuario').val();
        var clave=$('#passwordUsu').val();
        /*var settings = {
          "url": "http://localhost:8080/rest/cambiar-pass-usuario?emailUsuario="+correo +"&password="+clave,
          "method": "POST",
          "dataType": 'json',
          "headers": {
            "Authorization": "Bearer " + token
          },
        };

        $.ajax(settings).done(function (response) {
         alert(response['Contenido']);
        });*/

         $.ajax({
        url: "http://localhost:8080/rest/cambiar-pass-usuario?emailUsuario="+correo +"&password="+clave,
        type: 'POST',
        contentType:"application/json",
        async: false,
        headers: {
                    "Authorization": "Bearer " + token
                },
        success: function (msg) {
            var jsonData = JSON.parse(msg);
            alert(jsonData.Contenido);

        },
        error: function (error) {
            var jsonData = JSON.parse(error);
            alert(jsonData.Contenido);
        }
    });


    }
    </script>
    <script defer="" src="js/validacion.js"></script>
    <!-- /Cerrar Pagina en 15 min Script
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
    -->

    <div id="warning" style="display: none;" class="modal fade">
            <p>
                Tiempo para finalizar la sesion.
                <span>
                </span>
            </p>
            <p>
                <button id="close" type="button">
                    Close
                </button>
            </p>
        </div>

<script type="text/javascript">
var timeOfLastActivity;
var timeForCloseSession;
var timeBeforeWarning = 5000;   // 5 seconds
var timeBeforeClose = 900000;    // 15 min
var timeForUpdateWarning = 500; // 0.5 seconds

function resetAll() {
    timeOfLastActivity = new Date().getTime();
    timeForCloseSession = timeOfLastActivity + timeBeforeClose;
    setTimeout(function() {
        $("#warning").show();
        showTime();
    }, timeBeforeWarning);
    $("#warning").hide();
}

function showTime() {
    var currentTime = new Date().getTime();
    if (currentTime < timeForCloseSession) {
        $("#warning span").html(timeForCloseSession - currentTime)
    } else {
        $("#close").trigger("click");
    }
    if ($("#warning").is(":visible")) {
        setTimeout(showTime, timeForUpdateWarning);
    }
}

$("#close").click(function() {
    $("#warning").hide();
    location.href ="../index.php";
    $.post("/echo/json/"); // Send AJAX close
});


resetAll();
        </script>
</body>
</html>