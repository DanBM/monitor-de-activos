<!DOCTYPE HTML>


<html>
	<head>
		<title>login</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="is-preload">

	<?php
		session_start();
		if (isset($_SESSION['msgError'])) {
		    echo '<script type="text/javascript">
		    alert("'.$_SESSION['msgError'].'");
		    </script>';    
		}

		unset($_SESSION['msgError']);

		?>

		<?php
		 
		$_SESSION['tiempo']=time();  
		?>
		<!-- Header -->
			<header id="header">
				<h1>Sistema de Monitoreo de Activos</h1>
				<p>Una forma más facil de visualizar tus activos.<br />
				tu próxima gran cosa. Traído a usted por <a href="https://www.eclipsemex.com/">Eclipse</a>.</p>
			</header>

		<!-- Signup Form -->
			<form id="signup-form" method="post" action="views/controller.php">
				<input type="email" name="email" id="email" placeholder="Correo Electronico" />
				<br><br>
				<input type="password" name="password" id="password" placeholder="Contraseña" />
				<br><br><br>
				<input type="submit" value="Iniciar" />
			</form>

		<!-- Footer -->
			<footer id="footer">
				<ul class="icons">
					<li><a href="#" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
					<li><a href="#" class="icon brands fa-instagram"><span class="label">Instagram</span></a></li>
					<li><a href="#" class="icon fa-envelope"><span class="label">Email</span></a></li>
				</ul>
				<ul class="copyright">
					<li>Copyright 2021 &copy; Eclipse Telecomunicaciones S.A de C.V - Versión 1.2.2</li><li>Creditos: <a href="https://www.eclipsemex.com/">Eclipse Telecomunicaciones S.A de C.V</a></li>
				</ul>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/main.js"></script>

	</body>
</html>