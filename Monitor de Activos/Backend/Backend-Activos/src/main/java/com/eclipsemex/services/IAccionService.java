package com.eclipsemex.services;

import java.util.List;

import com.eclipsemex.model.Accion;

/**
 * Interfaz que genera metodos para las Acciones
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
public interface IAccionService {
	/**
	 * Busca todas las acciones en la base de datos
	 * 
	 * @return Regresa una lista de todas las acciones
	 */
	List<Accion> buscarTodos();
}
