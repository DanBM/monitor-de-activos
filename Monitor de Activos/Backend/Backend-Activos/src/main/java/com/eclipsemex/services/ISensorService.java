package com.eclipsemex.services;

import java.math.BigInteger;

import org.springframework.security.core.Authentication;

import com.eclipsemex.model.Sensor;

/**
 * Interfaz donde se definen los metodos de un Sensor
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
public interface ISensorService {
	/**
	 * Obtiene todos los sensores
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	String buscarTodos(Authentication auth);

	/**
	 * Obtiene el detalle de un sensor
	 * 
	 * @param idSensor: representa un id de un sensor
	 * 
	 * @param auth      Representa el token para una solicitud de autenticación no
	 *                  se manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	String sensorDetalle(BigInteger idSensor, Authentication auth);

	/**
	 * Agrega un nuevo sensor
	 * 
	 * @param nombreSensor: representa el nombre de un sensor
	 * 
	 * @param auth:         Representa el token para una solicitud de autenticación
	 *                      no se manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	String agregarSensor(String nombreSensor, Authentication auth);

	/**
	 * Modifica un sensor
	 * 
	 * @body sensor: representa un modelo de sensor
	 * 
	 * @param auth: Representa el token para una solicitud de autenticación no se
	 *              manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	String modificarSensor(Sensor sensor, Authentication auth);

	/**
	 * Elimina un sensor
	 * 
	 * @param idSensor: representa un id de un sensor
	 * 
	 * @param auth:     Representa el token para una solicitud de autenticación no
	 *                  se manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	String eliminarSensor(BigInteger idSensor, Authentication auth);
}
