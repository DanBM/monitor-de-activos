package com.eclipsemex.model;

import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Modelo que pertenece a la tabla de la base de datos cat_municipio
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@Entity
@Table(name = "cat_municipio")
public class Municipio {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger idMunicipio;
	private String nomMunicipio;

	/**
	 * Constructor.
	 * 
	 * @param idMuicipio   idMunicipio: id de un municipio.
	 * @param nomMunicipio nomMunicipio: nombre de un municipio.
	 */
	public Municipio(BigInteger idMunicipio, String nomMunicipio) {
		super();
		this.idMunicipio = idMunicipio;
		this.nomMunicipio = nomMunicipio;
	}

	public BigInteger getIdMunicipio() {
		return idMunicipio;
	}

	public void setIdMunicipio(BigInteger idMunicipio) {
		this.idMunicipio = idMunicipio;
	}

	public String getNomMunicipio() {
		return nomMunicipio;
	}

	public void setNomMunicipio(String nomMunicipio) {
		this.nomMunicipio = nomMunicipio;
	}

}
