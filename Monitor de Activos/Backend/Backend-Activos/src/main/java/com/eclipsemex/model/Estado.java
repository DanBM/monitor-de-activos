package com.eclipsemex.model;

import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Modelo que pertenece a la tabla de la base de datos cat_estado
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@Entity
@Table(name = "cat_estado")
public class Estado {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger idEstado;
	private String nomEstado;

	/**
	 * Constructor.
	 * 
	 * @param idEstado  idEstado: id de estado
	 * @param nomEstado nomEstado: nombre del estado
	 */
	public Estado(BigInteger idEstado, String nomEstado) {
		super();
		this.idEstado = idEstado;
		this.nomEstado = nomEstado;
	}

	/**
	 * Getter.
	 * 
	 * @return idEstado: obtiene el id del estado
	 */
	public BigInteger getIdEstado() {
		return idEstado;
	}

	/**
	 * Setter.
	 * 
	 * @param idEstado: modifica un id de un estado
	 */
	public void setIdEstado(BigInteger idEstado) {
		this.idEstado = idEstado;
	}

	/**
	 * Getter.
	 * 
	 * @return nomEstado: obtiene el nombre del estado
	 */
	public String getNomEstado() {
		return nomEstado;
	}

	/**
	 * Setter.
	 * 
	 * @param nomEstado: modifica un nombre de un estado
	 */
	public void setNomEstado(String nomEstado) {
		this.nomEstado = nomEstado;
	}

}
