package com.eclipsemex.security.entity;

import javax.persistence.*;

import java.util.HashSet;
import java.util.Set;

/**
 * Modelo que pertenece a la tabla de la base de datos Usuario
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@Entity
public class Usuario {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idUsuario;
	private String nombreUsuario;
	private String appUsuario;
	private String apmUsuario;
	private String email;
	private String password;
	private String tel;
	private int operandoUsuario;
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "usuario_rol", joinColumns = @JoinColumn(name = "usuario_id"), inverseJoinColumns = @JoinColumn(name = "rol_id"))
	private Set<Rol> roles = new HashSet<>();

	public Usuario() {
	}

	/**
	 * Constructor.
	 * 
	 * @param nombreUsuario   nombreUsuario: nombre de un usuario.
	 * @param appUsuario      appUsuario: apellido paterno de un usuario.
	 * @param apmUsuario      apmUsuario: apellido materno de un usuario.
	 * @param email           email: correo de un usuario.
	 * @param password        password: contraseña de un usuario.
	 * @param tel             tel: telefono de un usuario.
	 * @param operandoUsuario operandoUsuario: estado de un usuario.
	 * 
	 */
	public Usuario(String nombreUsuario, String appUsuario, String apmUsuario, String email, String password,
			String tel, int operandoUsuario) {
		this.nombreUsuario = nombreUsuario;
		this.appUsuario = appUsuario;
		this.apmUsuario = apmUsuario;
		this.email = email;
		this.password = password;
		this.tel = tel;
		this.operandoUsuario = operandoUsuario;
	}

	public int getId() {
		return idUsuario;
	}

	public void setId(int id) {
		this.idUsuario = id;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Rol> getRoles() {
		return roles;
	}

	public void setRoles(Set<Rol> roles) {
		this.roles = roles;
	}

	public String getAppUsuario() {
		return appUsuario;
	}

	public void setAppUsuario(String appUsuario) {
		this.appUsuario = appUsuario;
	}

	public String getApmUsuario() {
		return apmUsuario;
	}

	public void setApmUsuario(String apmUsuario) {
		this.apmUsuario = apmUsuario;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public int getOperandoUsuario() {
		return operandoUsuario;
	}

	public void setOperandoUsuario(int operandoUsuario) {
		this.operandoUsuario = operandoUsuario;
	}

}
