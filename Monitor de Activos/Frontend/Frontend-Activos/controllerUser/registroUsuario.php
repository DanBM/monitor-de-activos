<?php
session_start();


if (isset($_POST)) {
    $views = '../views/tablaUsuario.php';

	$datos = array(
        "nombreUsuario"=> $_POST['txtnombre'],
        "appUsuario"=> $_POST['txtapp'],
        "apmUsuario"=> $_POST['txtapm'],
		"email" => $_POST['txtemail'],
		"password" => $_POST['txtpassword'],
        "tel"=> $_POST['txttel'],
        "roles"=> [$_POST['txtrol']]
	);

	$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'localhost:8080/rest/usuario',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS => json_encode($datos),
  CURLOPT_HTTPHEADER => array(
    'Authorization: Bearer '.$_SESSION['tokenGuardado'],
    'Content-Type: application/json'
  ),
));


	$response = curl_exec($curl);
	curl_close($curl);
	$dataToJson = json_decode($response,true);
	if ($dataToJson['Respuesta']=="Error") {
        $_SESSION['msg']=$dataToJson['Contenido'];
        header('Location: ' . $views);
   }else{
        $_SESSION['msg']=$dataToJson['Contenido'];
        header('Location: ' . $views);
   }

}



?>