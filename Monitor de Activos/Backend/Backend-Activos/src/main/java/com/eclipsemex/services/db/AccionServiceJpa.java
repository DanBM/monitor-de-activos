package com.eclipsemex.services.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.eclipsemex.model.Accion;
import com.eclipsemex.repository.AccionRepository;
import com.eclipsemex.services.IAccionService;

@Service
@Primary
public class AccionServiceJpa implements IAccionService {

	@Autowired
	AccionRepository repoAccion;

	/**
	 * Busca todas las acciones en la base de datos
	 * 
	 * @return Regresa una lista de todas las acciones
	 */
	@Override
	public List<Accion> buscarTodos() {
		return repoAccion.findAll();
	}

}
