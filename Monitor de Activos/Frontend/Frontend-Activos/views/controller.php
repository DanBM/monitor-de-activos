<?php

session_start();

//echo "<h1>Hola hermano estas en el Controller.php</h1>";

if (isset($_POST)) {

    $views = [
        'admin'   => 'vistaAdministrador.html',
        'gerente' => 'vistaGerente.html',
        'usuario' => 'vistaGeneral.html',
        'index'   => '../index.php',
    ];

    $datos = array(
        "email"    => $_POST['email'],
        "password" => $_POST['password'],
    );

    $curl = curl_init();
    curl_setopt_array($curl, array(

        /* le digo hacia que url va a llamar */
        CURLOPT_URL            => 'localhost:8080/auth/login',
        /* los demas parametros son necesarios para la llamada*/
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING       => '',
        CURLOPT_MAXREDIRS      => 10,
        CURLOPT_TIMEOUT        => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
        /* que metodo se va a ocupar o pide el api */
        CURLOPT_CUSTOMREQUEST  => 'POST',
        /* mando los datos en formato JSON */
        CURLOPT_POSTFIELDS     => json_encode($datos),
        /* agrego un header, es necesario tenerlo en cuenta ya que se ocupara en otras ocasiones para poner el token */
        CURLOPT_HTTPHEADER     => array(
            'Content-Type: application/json',
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    $dataToJson = json_decode($response, true);
    if ($dataToJson['Respuesta'] == 'Error') {
        $_SESSION['msgError'] = $dataToJson['Contenido'];
        header('Location: ' . $views['index']);
    } else {
        if (isset($dataToJson['Contenido']['token'])) {
            $nuevoToken                = ($dataToJson['Contenido']['token']);
            $_SESSION['tokenGuardado'] = $nuevoToken;

            echo 'existe el token: ' . $dataToJson['Contenido']['token'];
            if (isset($dataToJson['Contenido']['email'])) {
                /* valido si existe el correo para guardarlo en algun lado o en una sesion */
                echo 'existe el token: ' . $dataToJson['Contenido']['email'];
                if ($dataToJson['Contenido']['authorities'][0]['authority'] == 'ROLE_ADMIN') {
                    /* validamos que tipo de usuario es, si es admin, gerente o usuario y dependiendo de eso lo mandamos a su vista correspondiente */

                    $_SESSION['rol'] = $dataToJson['Contenido']['authorities'][0]['authority'];
                    header('Location: ' . $views['admin']);

                }
                if ($dataToJson['Contenido']['authorities'][0]['authority'] == 'ROLE_GERENTE') {
                    $_SESSION['rol'] = $dataToJson['Contenido']['authorities'][0]['authority'];
                    header('Location: ' . $views['gerente']);
                }
                if ($dataToJson['Contenido']['authorities'][0]['authority'] == 'ROLE_USER') {
                    $_SESSION['rol']      = $dataToJson['Contenido']['authorities'][0]['authority'];
                    $_SESSION['emailUsu'] = $dataToJson['Contenido']['email'];
                    header('Location: ' . $views['usuario']);
                }
            }

        }

    }

}
