package com.eclipsemex.security.controller;

import com.eclipsemex.security.dto.JwtDto;
import com.eclipsemex.security.dto.LoginUsuario;

import com.eclipsemex.security.jwt.JwtProvider;

import com.eclipsemex.security.service.UsuarioService;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

import org.springframework.web.bind.annotation.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Api principal para generar token de un usuario
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@RestController
@RequestMapping("/auth")
@CrossOrigin
public class AuthController {

	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private JwtProvider jwtProvider;

	/**
	 * Valida las credenciales de un usuario (Genera Token)
	 * 
	 * @Body valores: de un usuario en un modelo
	 * 
	 * @return mensajes: de error o credenciales de acceso par aun usuario
	 */
	@PostMapping("/login")
	public String login(@RequestBody LoginUsuario loginUsuario) {
		if (loginUsuario.getEmail().trim().isEmpty() || loginUsuario.getEmail().trim() == null
				|| loginUsuario.getEmail().trim().length() <= 0 || loginUsuario.getEmail().trim().length() >= 50
				|| correoValido(loginUsuario.getEmail().trim().replace(" +", "")) == false)
			return respuestaJson("Error", "El 'Email' debe estar lleno, formato valido y tener entre 1 y 50 caracteres",
					HttpStatus.BAD_REQUEST);
		if (loginUsuario.getPassword().trim().isEmpty() || loginUsuario.getPassword().trim() == null
				|| loginUsuario.getPassword().trim().length() <= 0 || loginUsuario.getPassword().trim().length() >= 50)
			return respuestaJson("Error", "El campo 'Password' debe estar lleno o formato valido",
					HttpStatus.BAD_REQUEST);
		if (!usuarioService.existsByEmail(loginUsuario.getEmail()))
			return respuestaJson("Error", "El email no existe", HttpStatus.BAD_REQUEST);
		if (!passwordEncoder.matches(loginUsuario.getPassword(), usuarioService.obtenerPass(loginUsuario.getEmail())))
			return respuestaJson("Error", "El password es incorrecto", HttpStatus.BAD_REQUEST);
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginUsuario.getEmail(), loginUsuario.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtProvider.generateToken(authentication);
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		JwtDto jwtDto = new JwtDto(jwt, loginUsuario.getEmail(), userDetails.getAuthorities());
		JSONObject objson = new JSONObject();
		objson.put("token", jwtDto.getToken());
		objson.put("bearer", jwtDto.getBearer());
		objson.put("email", jwtDto.getEmail());
		objson.put("authorities", jwtDto.getAuthorities());
		return respuestaJson("Éxito", objson, HttpStatus.OK);
	}

	/**
	 * Genera un JSON de respuesta en String
	 * 
	 * @param respuesta que respuesta se requiere mostrar
	 * 
	 * @param contenido objeto que puede tener cualquier estructura para mostrar
	 * 
	 * @param codigo    codigo de respuesta que se desea enviar
	 * 
	 * @return JSON con valores enviados en formato string
	 */
	private String respuestaJson(String respuesta, Object contenido, HttpStatus codigo) {
		JSONObject objson = new JSONObject();
		objson.put("Respuesta", respuesta);
		objson.put("Contenido", contenido);
		objson.put("Codigo http", codigo);
		return objson.toString();
	}

	/**
	 * Valida que un correo tenga un formato correcto
	 * 
	 * @param cadena que respuesta se requiere mostrar
	 * 
	 * @return true o false si el correo es valido o incorrecto segun su caso
	 */
	private boolean correoValido(String cadena) {
		Pattern pat = Pattern.compile(
				"^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");
		Matcher mat = pat.matcher(cadena);
		return mat.matches();
	}

}
