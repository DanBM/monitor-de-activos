package com.eclipsemex.repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.eclipsemex.model.Sensor;

/**
 * Repositorio de un Sensor que extiende de JPARepository y tiene como modelo
 * Sensor
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
public interface SensorRepository extends JpaRepository<Sensor, Integer> {
	/**
	 * Obtiene todos los sensores por medio de un procedimiento almacenado
	 * 
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo.
	 * 
	 * @return lista de objetos de los sensores encontrados
	 */
	@Query(value = "{call sp_obtener_sensores(:emailUsuarioModificandoIn)}", nativeQuery = true)
	List<Object[]> listaSensoresProcedure(@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);

	/**
	 * Obtiene el detalle de un Sensor a partir de su idSensor por medio de un
	 * procedimiento almacenado
	 * 
	 * @param idSensor                idSensor: id de un Sensor.
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo.
	 * 
	 * @return lista de objetos de los Sensores encontrados
	 */
	@Query(value = "{call sp_obtener_sensor_idsensor(:idsensorIn,:emailUsuarioModificandoIn)}", nativeQuery = true)
	List<Object[]> sensorDetalleProcedure(@Param("idsensorIn") BigInteger idSensor,
			@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);

	/**
	 * Agrega un Sensor
	 * 
	 * @param nombreSensor            nombreSensor: Nombre de un sensor.
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo.
	 * 
	 * @return obtiene un entero para saber si se realizao la accion o no
	 */
	@Query(value = "{call sp_agregar_sensor(:nombreSensorIn,:emailUsuarioModificandoIn)}", nativeQuery = true)
	int agregarSensorProcedure(@Param("nombreSensorIn") String nombreSensor,
			@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);

	/**
	 * Eliminar un Sensor
	 * 
	 * @param idSensor                idSensor: id de un sensor.
	 * @param nombreSensor            nombreSensor: Nombre de un sensor.
	 * @param habilitado              habilitado: estado de un sensor.
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo.
	 * 
	 * @return obtiene un entero para saber si se realizao la accion o no
	 */
	@Query(value = "{call sp_actualizar_sensor(:idSensorIn,:nombreSensorId,:habilitadoIn,:emailUsuarioModificandoIn)}", nativeQuery = true)
	int modificarSensorProcedure(@Param("idSensorIn") BigInteger idSensor, @Param("nombreSensorId") String nombreSensor,
			@Param("habilitadoIn") int habilitado, @Param("emailUsuarioModificandoIn") String emailUsuarioModificando);

	/**
	 * Eliminar un Sensor
	 * 
	 * @param idSensor                idSensor: id de un sensor.
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo.
	 * 
	 * @return obtiene un entero para saber si se realizao la accion o no
	 */
	@Query(value = "{call sp_eliminar_sensor(:idSensorIn,:emailUsuarioModificandoIn)}", nativeQuery = true)
	int eliminarSensorProcedure(@Param("idSensorIn") BigInteger idSensor,
			@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);
}
