package com.eclipsemex.security.jwt;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Clase componente que se encarga de validar si un usuario tiene credenciales
 * validas
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@Component
public class JwtEntryPoint implements AuthenticationEntryPoint {

	/**
	 * Validar si un usuario es apto para acceder al sistema
	 * 
	 * @param req req: Variable que obtienen una peticion http.
	 * @param res res: variable que genera una respuesta http
	 * @param e   e: variable para verificar las credenciales del usuario.
	 */
	@Override
	public void commence(HttpServletRequest req, HttpServletResponse res, AuthenticationException e)
			throws IOException, ServletException {
		res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "No Autorizado");
	}
}
