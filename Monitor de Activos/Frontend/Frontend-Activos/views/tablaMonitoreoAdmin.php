<?php
session_start();
/*Se verefica que el rol ingresado tenga el permiso para estar en esta pagania o pestaña.
@author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
@version 23/02/2021*/
$views = [
    'gerente' => 'vistaGerente.html',
    'usuario' => 'vistaGeneral.html',
    'index'   => '../index.php',
];
if ($_SESSION['tokenGuardado'] == '') {
    header('Location: ' . $views['index']);
} else {
    if ($_SESSION['rol'] == 'ROLE_GERENTE') {
        header('Location: ' . $views['gerente']);

    }if ($_SESSION['rol'] == 'ROLE_USER') {
        header('Location: ' . $views['usuario']);

    }
}
$curl = curl_init();
/* Se verifica que el token exista para poder utilizar la pagina.
@author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
@version 23/02/2021
 */
curl_setopt_array($curl, array(
    CURLOPT_URL            => 'http://localhost:8080/rest/monitoreos',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING       => '',
    CURLOPT_MAXREDIRS      => 10,
    CURLOPT_TIMEOUT        => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST  => 'GET',
    CURLOPT_HTTPHEADER     => array(
        'Authorization: Bearer ' . $_SESSION['tokenGuardado'],
    ),
));

$response = curl_exec($curl);
curl_close($curl);
$dataToJson = json_decode($response, true);

?>

<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Tabla Monitoreo</title>
        <meta name="description" content="A sidebar menu as seen on the Google Nexus 7 website" />
        <meta name="keywords" content="google nexus 7 menu, css transitions, sidebar, side menu, slide out menu" />
        <meta name="author" content="Codrops" />
        <link rel="shortcut icon" href="../favicon.ico">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.2/css/all.css" integrity="sha384-vSIIfh2YWi9wW0r9iZe7RJPrKwp6bG+s9QZMoITbCckVJqGCCRhc+ccxNcdpHuYu" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/normalize.css" />
        <link rel="stylesheet" type="text/css" href="css/demo.css" />
        <link rel="stylesheet" type="text/css" href="css/component.css" />
         <!-- Librerias para la tabla -->
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js"></script>

    <!-- Datatables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/v/bs4-4.1.1/dt-1.10.18/datatables.min.css">
    <script src="https://cdn.datatables.net/v/bs4-4.1.1/dt-1.10.18/datatables.min.js"></script>

    <!-- Buttons -->
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.53/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.53/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>




        <script src="js/modernizr.custom.js"></script>
    </head>
    <body>
        <div class="container">
            <ul id="gn-menu" class="gn-menu-main">
                <li class="gn-trigger">
                    <a class="gn-icon gn-icon-menu"><span>Menú</span></a>
                    <nav class="gn-menu-wrapper">
                        <div class="gn-scroller">
                            <ul class="gn-menu">

                                <li>
                                    <a href="salir.php" class="gn-icon gn-icon-downloa">Salir</a>
                                    <ul class="gn-submenu">
                                        <li><a href="tablaUsuario.php" class="gn-icon gn-icon-usuer">Usuarios</a></li>
                                        <li><a href="tablaActivoAdmin.php" class="gn-icon gn-icon-photosh">Activos</a></li>
                                    </ul>
                                </li>
                                <li><a href="tablaMonitoreoAdmin.php" class="gn-icon gn-icon-cog">Monitoreo</a></li>
                                <li><a href="tablaSensoresAdmin.php" class="gn-icon gn-icon-help">Sensores</a></li>
                                <li>
                                    <a href="tablaRentasAdmin.php" class="gn-icon gn-icon-archive">Rentas</a>

                                </li>
                            </ul>
                        </div><!-- /gn-scroller -->
                    </nav>
                </li>
                <li><a href="">Sistema de Monitoreo de Activos</a></li>

                <li><a class="codrops-icon codrops-icon-" href="vistaAdministrador.html"><span>Home</span></a></li>
            </ul>

<!-- /Script para los modales
@author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
@version 23/02/2021-->

        <script type="text/javascript">
    $(document).ready(function(){
    // Activate tooltip
    $('[data-toggle="tooltip"]').tooltip();

    // Select/Deselect checkboxes
    var checkbox = $('table tbody input[type="checkbox"]');
    $("#selectAll").click(function(){
        if(this.checked){
            checkbox.each(function(){
                this.checked = true;
            });
        } else{
            checkbox.each(function(){
                this.checked = false;
            });
        }
    });
    checkbox.click(function(){
        if(!this.checked){
            $("#selectAll").prop("checked", false);
        }
    });
});
</script>

             <br> <br> <br>
             <?php
if (isset($_SESSION['msg'])) {
    echo '<script type="text/javascript">
    alert("' . $_SESSION['msg'] . '");
    </script>';
}

unset($_SESSION['msg']);

?>

        </div><!-- /container -->
<section class="contenidoTabla">
    <div class="table-responsive" id="mydatatable-container">
        <table class="records_list table table-striped table-bordered table-hover" id="mydatatable">
            <thead>
              <tr>
                <th scope="col">Fecha de Revision</th>
                <th scope="col">Comentario</th>
                <th scope="col">Sensor</th>
                <th scope="col">Activo</th>
              </tr>
            </thead>
             <tfoot>
                <tr>
                    <th>Filter..</th>
                    <th>Filter..</th>
                    <th>Filter..</th>
                    <th>Filter..</th>

                </tr>
            </tfoot>
    <!-- /Se trae la informacion del monitoreo de activos de la DB.
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
    -->
            <tbody>
           <?php foreach ($dataToJson['Contenido'] as $monitor) {?>
    <tr>

      <td><?php echo $monitor['fechaRevision'] ?></td>
      <td><?php echo $monitor['comentario'] ?></td>
      <td><?php echo $monitor['nomSensor'] ?></td>
      <td><?php echo $monitor['placa'] ?></td>



    </tr>
    <?php }?>
            </tbody>
        </table>
    </div>

</section>

<!-- /Scrip para la tabla y filtros de la misma.
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
-->

    <script type="text/javascript">
        $(document).ready(function () {
            $('#mydatatable tfoot th').each(function () {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Filtrar.." />');
            });

            var table = $('#mydatatable').DataTable({
                "dom": 'B<"float-left"i><"float-right"f>t<"float-left"l><"float-right"p><"clearfix">',
                "responsive": false,
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                },
                "order": [
                    [0, "desc"]
                ],
                "initComplete": function () {
                    this.api().columns().every(function () {
                        var that = this;

                        $('input', this.footer()).on('keyup change', function () {
                            if (that.search() !== this.value) {
                                that
                                    .search(this.value)
                                    .draw();
                            }
                        });
                    })
                },
                "buttons": [ 'excel', 'pdf']
            });
        });
    </script>

<script src="js/classie.js"></script>
<script src="js/gnmenu.js"></script>
<script>new gnMenu( document.getElementById( 'gn-menu' ) );</script>
<!-- /Cerrar Pagina en 15 min Script
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
-->
 <div id="warning" style="display: none;" class="modal fade">
            <p>
                Tiempo para finalizar la sesion.
                <span>
                </span>
            </p>
            <p>
                <button id="close" type="button">
                    Close
                </button>
            </p>
        </div>

<script type="text/javascript">
var timeOfLastActivity;
var timeForCloseSession;
var timeBeforeWarning = 5000;   // 5 seconds
var timeBeforeClose = 900000;    // 15 min
var timeForUpdateWarning = 500; // 0.5 seconds

function resetAll() {
    timeOfLastActivity = new Date().getTime();
    timeForCloseSession = timeOfLastActivity + timeBeforeClose;
    setTimeout(function() {
        $("#warning").show();
        showTime();
    }, timeBeforeWarning);
    $("#warning").hide();
}

function showTime() {
    var currentTime = new Date().getTime();
    if (currentTime < timeForCloseSession) {
        $("#warning span").html(timeForCloseSession - currentTime)
    } else {
        $("#close").trigger("click");
    }
    if ($("#warning").is(":visible")) {
        setTimeout(showTime, timeForUpdateWarning);
    }
}

$("#close").click(function() {
    $("#warning").hide();
    location.href ="../index.php";
    $.post("/echo/json/"); // Send AJAX close
});


resetAll();
        </script>

    </body>
</html>