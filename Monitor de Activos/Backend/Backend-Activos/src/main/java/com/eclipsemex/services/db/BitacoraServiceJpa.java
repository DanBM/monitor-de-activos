package com.eclipsemex.services.db;

import java.util.LinkedList;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.eclipsemex.model.Bitacora;
import com.eclipsemex.repository.BitacoraRepository;
import com.eclipsemex.security.entity.UsuarioPrincipal;
import com.eclipsemex.security.enums.Errores;
import com.eclipsemex.services.IBitacoraService;

/**
 * Clase de servicio para las acciones en la Bitacora
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@Service
@Primary
public class BitacoraServiceJpa implements IBitacoraService {

	@Autowired
	private BitacoraRepository repoBitacora;
	private UsuarioPrincipal usuarioPrincipal;

	/**
	 * Obtiene las acciones registradas en la bitacora
	 * 
	 * @param idMonitoreo: representa el id de un monitoreo
	 * 
	 * @param auth         Representa el token para una solicitud de autenticación
	 *                     no se manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@Override
	public String buscarTodos(Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		List<Object[]> todasAcciones = repoBitacora.listaAccionesBitacoraProcedure(emailUsuarioModificando);
		Object excepcion = (Object) todasAcciones.get(0)[0];
		if (excepcion.equals("1")) {
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.BAD_REQUEST);
		} else if (excepcion.equals("2")) {
			return respuestaJson("Error", Errores.No_Hay_Resultados, HttpStatus.NO_CONTENT);
		} else {
			List<Bitacora> acciones = new LinkedList();
			for (Object[] objects : todasAcciones) {
				String fecha = (String) objects[0];
				String nomAccion = (String) objects[1];
				String nomUsuario = (String) objects[2] + " " + (String) objects[3] + " " + (String) objects[4];
				String tabla = (String) objects[5];
				Bitacora bitacora = new Bitacora(fecha, nomAccion, nomUsuario, tabla);
				acciones.add(bitacora);
			}
			return respuestaJson("Éxito", acciones, HttpStatus.OK);
		}
	}

	/**
	 * Genera un JSON de respuesta en String
	 * 
	 * @param respuesta que respuesta se requiere mostrar
	 * 
	 * @param contenido objeto que puede tener cualquier estructura para mostrar
	 * 
	 * @param codigo    codigo de respuesta que se desea enviar
	 * 
	 * @return JSON con valores enviados en formato string
	 */
	public String respuestaJson(String respuesta, Object contenido, HttpStatus codigo) {
		JSONObject objson = new JSONObject();
		objson.put("Respuesta", respuesta);
		objson.put("Contenido", contenido);
		objson.put("Codigo http", codigo);
		return objson.toString();
	}

}
