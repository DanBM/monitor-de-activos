package com.eclipsemex.controller;

import java.math.BigInteger;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eclipsemex.model.Activo;

import com.eclipsemex.model.Monitoreo;
import com.eclipsemex.model.Renta;
import com.eclipsemex.model.Sensor;
import com.eclipsemex.security.dto.NuevoUsuario;

import com.eclipsemex.services.IActivoService;
import com.eclipsemex.services.IBitacoraService;
import com.eclipsemex.services.IEstadoService;
import com.eclipsemex.services.IMonitoreoService;
import com.eclipsemex.services.IMunicipioService;
import com.eclipsemex.services.IRentaService;
import com.eclipsemex.services.IRolService;
import com.eclipsemex.services.ISensorService;
import com.eclipsemex.services.IUsuariosService;

/**
 * Api principal para hacer todas las funcionalidades una vez que un usuario
 * tenga token
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@RestController
@RequestMapping("/rest")
@CrossOrigin
public class ApiController {

	@Autowired
	private IActivoService serviceActivo;
	@Autowired
	private IEstadoService serviceEstado;
	@Autowired
	private IMonitoreoService serviceMonitoreo;
	@Autowired
	private IMunicipioService serviceMunicipio;
	@Autowired
	private IRentaService serviceRenta;
	@Autowired
	private ISensorService serviceSensor;
	@Autowired
	private IUsuariosService serviceUsuarios;
	@Autowired
	private IBitacoraService serviceBitacora;
	@Autowired
	private IRolService serviceRol;

	/**
	 * Obtiene todos los roles de un usuario
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@PreAuthorize("hasAnyRole('ADMIN','GERENTE')")
	@GetMapping("/roles")
	public String mostrarRoles(Authentication auth) {
		return serviceRol.buscarTodos(auth);
	}

	/**
	 * Agrega a un usuario.
	 * 
	 * @body nuevoUsuario representa un modelo para asignarle la informacion
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/usuario")
	public String nuevoUsuario(@RequestBody NuevoUsuario nuevoUsuario, Authentication auth) {
		return serviceUsuarios.registrarUsuario(nuevoUsuario, auth);
	}

	/**
	 * Modifica a un usuario.
	 * 
	 * @body nuevoUsuario representa un modelo para asignarle la informacion
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/usuario")
	public String modificarUsuario(@RequestBody NuevoUsuario nuevoUsuario, Authentication auth) {
		return serviceUsuarios.actualizarUsuario(nuevoUsuario, auth);
	}

	/**
	 * Modifica la contraseña de un usuario.
	 * 
	 * @param emailUsuario representa un correo de un usuario
	 * 
	 * @param password     representa una contraseña de un usuario
	 * 
	 * @param auth         Representa el token para una solicitud de autenticación
	 *                     no se manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/cambiar-pass-usuario")
	public String cambiarPassUsuario(@RequestParam("emailUsuario") String emailUsuario,
			@RequestParam("password") String password, Authentication auth) {
		return serviceUsuarios.actualizarPasswordUsuario(emailUsuario, password, auth);
	}

	/**
	 * Elimina a un usuario.
	 * 
	 * @param emailUsuario representa un correo de un usuario
	 * 
	 * @param auth         Representa el token para una solicitud de autenticación
	 *                     no se manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/usuario")
	public String eliminarUsuario(@RequestParam("emailUsuario") String emailUsuario, Authentication auth) {
		return serviceUsuarios.eliminarUsuario(emailUsuario, auth);
	}

	/**
	 * Obtiene todos los usuarios
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/usuarios")
	public String mostrarUsuarios(Authentication auth) {
		return serviceUsuarios.buscarTodos(auth);
	}

	/**
	 * Obtiene el detalle de un usuario
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/usuario-detalle")
	public String mostrarUsuarioDetalle(@RequestParam("emailUsuario") String emailUsuario, Authentication auth) {
		return serviceUsuarios.usuarioDetalle(emailUsuario, auth);
	}

	/**
	 * Agrega un nuevo sensor
	 * 
	 * @param nombreSensor: representa el nombre de un sensor
	 * 
	 * @param auth:         Representa el token para una solicitud de autenticación
	 *                      no se manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@PreAuthorize("hasAnyRole('ADMIN','GERENTE')")
	@PostMapping("/sensor")
	public String registrarSensor(@RequestParam("nombreSensor") String nombreSensor, Authentication auth) {
		return serviceSensor.agregarSensor(nombreSensor, auth);
	}

	/**
	 * Modifica un sensor
	 * 
	 * @body sensor: representa un modelo de sensor
	 * 
	 * @param auth: Representa el token para una solicitud de autenticación no se
	 *              manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@PreAuthorize("hasAnyRole('ADMIN','GERENTE')")
	@PutMapping("/sensor")
	public String modificarSensor(@RequestBody Sensor sensor, Authentication auth) {
		return serviceSensor.modificarSensor(sensor, auth);
	}

	/**
	 * Elimina un sensor
	 * 
	 * @param idSensor: representa un id de un sensor
	 * 
	 * @param auth:     Representa el token para una solicitud de autenticación no
	 *                  se manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@PreAuthorize("hasAnyRole('ADMIN','GERENTE')")
	@DeleteMapping("/sensor")
	public String eliminarSensor(@RequestParam("idSensor") BigInteger idSensor, Authentication auth) {
		return serviceSensor.eliminarSensor(idSensor, auth);
	}

	/**
	 * Obtiene todos los sensores
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@PreAuthorize("hasAnyRole('ADMIN','GERENTE')")
	@GetMapping("/sensores")
	public String mostrarSensores(Authentication auth) {
		return serviceSensor.buscarTodos(auth);
	}

	/**
	 * Obtiene el detalle de un sensor
	 * 
	 * @param idSensor: representa un id de un sensor
	 * 
	 * @param auth      Representa el token para una solicitud de autenticación no
	 *                  se manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@PreAuthorize("hasAnyRole('ADMIN','GERENTE')")
	@GetMapping("/sensor-detalle")
	public String mostrarSensoDetalle(@RequestParam("idSensor") BigInteger idSensor, Authentication auth) {
		return serviceSensor.sensorDetalle(idSensor, auth);
	}

	/**
	 * Agrega un nuevo Activo
	 * 
	 * @param activo: representa el modelo de un activo
	 * 
	 * @param auth:   Representa el token para una solicitud de autenticación no se
	 *                manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@PreAuthorize("hasAnyRole('ADMIN','GERENTE')")
	@PostMapping("/activo")
	public String registrarActivo(@RequestBody Activo activo, Authentication auth) {
		return serviceActivo.agregarActivo(activo, auth);
	}

	/**
	 * Modificar un nuevo Activo
	 * 
	 * @param activo: representa el modelo de un activo
	 * 
	 * @param auth:   Representa el token para una solicitud de autenticación no se
	 *                manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@PreAuthorize("hasAnyRole('ADMIN','GERENTE')")
	@PutMapping("/activo")
	public String modificarActivo(@RequestBody Activo activo, Authentication auth) {
		return serviceActivo.modificarActivo(activo, auth);
	}

	/**
	 * Eliminar un nuevo Activo
	 * 
	 * @param activo: representa el modelo de un activo
	 * 
	 * @param auth:   Representa el token para una solicitud de autenticación no se
	 *                manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@PreAuthorize("hasAnyRole('ADMIN','GERENTE')")
	@DeleteMapping("/activo")
	public String eliminarActivo(@RequestParam("idActivo") BigInteger idActivo, Authentication auth) {
		return serviceActivo.eliminarActivo(idActivo, auth);
	}

	/**
	 * Obtiene todos los Activos
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@PreAuthorize("hasAnyRole('ADMIN','GERENTE','USER')")
	@GetMapping("/activos")
	public String mostrarActivos(Authentication auth) {
		return serviceActivo.buscarTodos(auth);
	}

	/**
	 * Obtiene un activo en especifico
	 * 
	 * @param idActivo: representa un id de un activo
	 * 
	 * @param auth      Representa el token para una solicitud de autenticación no
	 *                  se manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@PreAuthorize("hasAnyRole('ADMIN','GERENTE','USER')")
	@GetMapping("/activo-detalle")
	public String mostrarActivosDetalle(@RequestParam("idActivo") BigInteger idActivo, Authentication auth) {
		return serviceActivo.activoDeatlle(idActivo, auth);
	}

	/**
	 * Agrega un nuevo monitoreo
	 * 
	 * @body monitoreo: representa el modelo de un monitoreo
	 * 
	 * @param auth: Representa el token para una solicitud de autenticación no se
	 *              manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@PreAuthorize("hasAnyRole('ADMIN','GERENTE')")
	@PostMapping("/monitoreo")
	public String registrarMonitoreo(@Valid @RequestBody Monitoreo monitoreo, Authentication auth) {
		return serviceMonitoreo.registrarMonitoreo(monitoreo, auth);
	}

	/**
	 * Modificar un nuevo monitoreo
	 * 
	 * @body monitoreo: representa el modelo de un monitoreo
	 * 
	 * @param auth: Representa el token para una solicitud de autenticación no se
	 *              manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@PreAuthorize("hasAnyRole('ADMIN','GERENTE')")
	@PutMapping("/monitoreo")
	public String modificarMonitoreo(@RequestBody Monitoreo monitoreo, Authentication auth) {
		return serviceMonitoreo.modificarMonitoreo(monitoreo, auth);
	}

	/**
	 * Obtiene todos los monitoreos
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@PreAuthorize("hasAnyRole('ADMIN','GERENTE')")
	@GetMapping("/monitoreos")
	public String mostrarMonitoreos(Authentication auth) {
		return serviceMonitoreo.obtenerListaMonitoreos(auth);
	}

	/**
	 * Obtiene el monitoreo de un activo
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@PreAuthorize("hasAnyRole('ADMIN','GERENTE')")
	@GetMapping("/monitoreos-activo")
	public String mostrarMonitoreoDetalleActivo(@RequestParam("idActivo") BigInteger idActivo, Authentication auth) {
		return serviceMonitoreo.obtenerMonitoreosActivo(idActivo, auth);
	}

	/**
	 * Obtiene el detalle de un monitoreo
	 * 
	 * @param idMonitoreo: representa el id de un monitoreo
	 * 
	 * @param auth         Representa el token para una solicitud de autenticación
	 *                     no se manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@PreAuthorize("hasAnyRole('ADMIN','GERENTE')")
	@GetMapping("/monitoreo-detalle")
	public String mostrarMonitoreoDetalle(@RequestParam("idMonitoreo") BigInteger idMonitoreo, Authentication auth) {
		return serviceMonitoreo.monitoreoDetalle(idMonitoreo, auth);
	}

	/**
	 * Agrega un nueva nueva renta
	 * 
	 * @body renta: representa el modelo de una renta
	 * 
	 * @param auth: Representa el token para una solicitud de autenticación no se
	 *              manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@PreAuthorize("hasAnyRole('USER','ADMIN')")
	@PostMapping("/renta")
	public String realizarRenta(@RequestBody Renta renta, Authentication auth) {
		return serviceRenta.agregarRenta(renta, auth);
	}

	/**
	 * Modificar una renta
	 * 
	 * @body renta: representa el modelo de una renta
	 * 
	 * @param auth: Representa el token para una solicitud de autenticación no se
	 *              manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@PreAuthorize("hasAnyRole('ADMIN','GERENTE')")
	@PutMapping("/renta")
	public String modificarRenta(@RequestBody Renta renta, Authentication auth) {
		return serviceRenta.modificarRenta(renta, auth);
	}

	/**
	 * Elimina una renta
	 * 
	 * @param idRenta: representa el un id de una renta
	 * 
	 * @param auth:    Representa el token para una solicitud de autenticación no se
	 *                 manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@PreAuthorize("hasAnyRole('ADMIN','GERENTE')")
	@DeleteMapping("/renta")
	public String EliminarRenta(@RequestParam("idRenta") BigInteger idRenta, Authentication auth) {
		return serviceRenta.eliminarRenta(idRenta, auth);
	}

	/**
	 * Obtiene todas las rentas de los usuarios
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/todas-rentas-usuarios")
	public String mostrarRentaUsuarios(Authentication auth) {
		return serviceRenta.buscarTodasRentasUsuarios(auth);
	}

	/**
	 * Obtiene las rentas de un usuario
	 * 
	 * @param email: representa el correo de un usuario
	 * 
	 * @param auth   Representa el token para una solicitud de autenticación no se
	 *               manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@PreAuthorize("hasAnyRole('ADMIN','USER')")
	@GetMapping("/mis-rentas")
	public String mostrarMisRentas(@RequestParam("email") String email, Authentication auth) {
		return serviceRenta.buscarRentasUsuario(email, auth);
	}

	/**
	 * Obtiene el detalle de una renta
	 * 
	 * @param idRenta: representa el id de una renta
	 * 
	 * @param auth     Representa el token para una solicitud de autenticación no se
	 *                 manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@PreAuthorize("hasAnyRole('ADMIN','USER')")
	@GetMapping("/renta-usuario-detalle")
	public String mostrarRrentaUsuarioDetalle(@RequestParam("idRenta") BigInteger idRenta, Authentication auth) {
		return serviceRenta.detalleRentaUsuario(idRenta, auth);
	}

	/**
	 * Obtiene todos los estados
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@PreAuthorize("hasAnyRole('USER','ADMIN','GERENTE')")
	@GetMapping("/estados")
	public String mostrarEstados(Authentication auth) {
		return serviceEstado.buscarTodos(auth);
	}

	/**
	 * Obtiene todos los municipios
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@PreAuthorize("hasAnyRole('USER','ADMIN','GERENTE')")
	@GetMapping("/municipios")
	public String mostrarMunicipios(Authentication auth) {
		return serviceMunicipio.buscarTodos(auth);
	}

	/**
	 * Obtiene las acciones registradas en la bitacora
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@PreAuthorize("hasAnyRole('ADMIN')")
	@GetMapping("/bitacora")
	public String mostrarBitacora(Authentication auth) {
		return serviceBitacora.buscarTodos(auth);
	}

}
