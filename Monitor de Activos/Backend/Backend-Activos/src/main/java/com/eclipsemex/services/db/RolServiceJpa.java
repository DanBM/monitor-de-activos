package com.eclipsemex.services.db;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.eclipsemex.model.RolUsuario;
import com.eclipsemex.repository.RolUsuarioRepository;
import com.eclipsemex.security.entity.Rol;
import com.eclipsemex.security.entity.UsuarioPrincipal;
import com.eclipsemex.security.enums.Errores;
import com.eclipsemex.security.enums.RolNombre;
import com.eclipsemex.security.repository.RolRepository;
import com.eclipsemex.security.service.UsuarioService;
import com.eclipsemex.services.IRolService;

@Service
@Primary
public class RolServiceJpa implements IRolService {

	@Autowired
	RolUsuarioRepository repoRol;

	private UsuarioPrincipal usuarioPrincipal;

	@Override
	public String buscarTodos(Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		List<Object[]> todosRoles = repoRol.listaRolesProcedure(emailUsuarioModificando);
		Object excepcion = (Object) todosRoles.get(0)[0];
		if (excepcion.equals("1")) {
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.BAD_REQUEST);
		} else if (excepcion.equals("2")) {
			return respuestaJson("Error", Errores.No_Hay_Resultados, HttpStatus.NO_CONTENT);
		} else {
			List<RolUsuario> roles = new LinkedList();
			for (Object[] objects : todosRoles) {
				BigInteger idRol = (BigInteger) objects[0];
				String nomRol = (String) objects[1];
				RolUsuario rol = new RolUsuario(idRol, nomRol);
				roles.add(rol);
			}
			return respuestaJson("Éxito", roles, HttpStatus.OK);
		}
	}

	public String respuestaJson(String respuesta, Object contenido, HttpStatus codigo) {
		JSONObject objson = new JSONObject();
		objson.put("Respuesta", respuesta);
		objson.put("Contenido", contenido);
		objson.put("Codigo http", codigo);
		return objson.toString();
	}
}
