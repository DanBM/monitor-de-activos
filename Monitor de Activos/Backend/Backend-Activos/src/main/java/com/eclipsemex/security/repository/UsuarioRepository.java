package com.eclipsemex.security.repository;

import com.eclipsemex.security.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Repositorio de un Usuario que extiende de JPARepository y tiene como modelo
 * Usuario
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {
	/**
	 * Obtiene un usuario por medio de su correo.
	 * 
	 * @param email email: correo del usuario.
	 * 
	 * @return Optional de un modelo de un usuario.
	 */
	@Query(value = "{call sp_obtener_usuario_email(:emailIn)}", nativeQuery = true)
	Optional<Usuario> buscarUsuarioEmailProcedure(@Param("emailIn") String email);

	/**
	 * Obtiene la contraseña de un usuario por medio de su correo.
	 * 
	 * @param email email: correo del usuario.
	 * 
	 * @return cadena con la contraseña de un usuario.
	 */
	@Query(value = "{call sp_obtener_pass_usuario(:emailIn)}", nativeQuery = true)
	String buscarPassUsuarioEmailProcedure(@Param("emailIn") String email);

	/**
	 * Busca si un usuario existe por medio de su correo
	 * 
	 * @param email email: correo del usuario.
	 * 
	 * @return booleano si se encontro o no el usuario.
	 */
	boolean existsByEmail(String email);

	/**
	 * Obtiene todos los usuarios de la base de datos.
	 * 
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo
	 * 
	 * @return lista de objetos de los usuarios encontrados.
	 */
	@Query(value = "{call sp_obtener_usuarios(:emailUsuarioModificandoIn)}", nativeQuery = true)
	List<Object[]> listaUsuariosProcedure(@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);

	/**
	 * Obtiene el detalle de un usuario por medio de su correo
	 * 
	 * @param emailidEmailUsuario     emailidEmailUsuario: correo del usuario
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo
	 * 
	 * @return lista de objetos del usuario encontrado
	 */
	@Query(value = "{call sp_obtener_detalle_usuario_email(:idEmailUsuarioIn,:correoUsuarioModificandoIn)}", nativeQuery = true)
	List<Object[]> usuarioDetalleProcedure(@Param("idEmailUsuarioIn") String emailidEmailUsuario,
			@Param("correoUsuarioModificandoIn") String correoUsuarioModificando);

	/**
	 * Da de alta un nuevo usuario
	 * 
	 * @param nombreUsuario           nombreUsuario: nombre de un usuario.
	 * @param appUsuario              appUsuario: apellido paterno de un usuario.
	 * @param apmUsuario              apmUsuario: apellido materno de un usuario.
	 * @param emailUsuario            emailUsuario: correo de un usuario.
	 * @param pass                    pass: contraseña de un usuario.
	 * @param tel                     tel: telefono de un usuario.
	 * @param operandoUsuario         operandoUsuario: estado de un usuario.
	 * @param nombreRol               nombreRol: Rol de un usuario.
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo
	 * 
	 * @return obtiene un entero para saber si se realizao la accion o no
	 */
	@Query(value = "{call sp_agregar_usuario(:nombreUsuarioIn,:appUsuarioIn,:apmUsuarioIn,:emailUsuarioIn,:passIn,:telIn,:operandoUsuarioIn,:nombreRolIn,:correoUsuarioIn)}", nativeQuery = true)
	int registrarUsuarioProcedure(@Param("nombreUsuarioIn") String nombreUsuario,
			@Param("appUsuarioIn") String appUsuario, @Param("apmUsuarioIn") String apmUsuario,
			@Param("emailUsuarioIn") String emailUsuario, @Param("passIn") String pass, @Param("telIn") String tel,
			@Param("operandoUsuarioIn") int operandoUsuario, @Param("nombreRolIn") String nombreRol,
			@Param("correoUsuarioIn") String correoUsuario);

	/**
	 * Modifica un usuario
	 * 
	 * @param nombreUsuario           nombreUsuario: nombre de un usuario.
	 * @param appUsuario              appUsuario: apellido paterno de un usuario.
	 * @param apmUsuario              apmUsuario: apellido materno de un usuario.
	 * @param emailUsuario            emailUsuario: correo de un usuario.
	 * @param tel                     tel: telefono de un usuario.
	 * @param operandoUsuario         operandoUsuario: estado de un usuario.
	 * @param nombreRol               nombreRol: Rol de un usuario.
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo
	 * 
	 * @return obtiene un entero para saber si se realizao la accion o no
	 */
	@Query(value = "{call sp_actualizar_usuario(:nombreUsuarioIn,:appUsuarioIn,:apmUsuarioIn,:emailUsuarioIn,:telIn,:operandoUsuarioIn,:nombreRolIn,:correoUsuarioIn)}", nativeQuery = true)
	int actualizarUsuarioProcedure(@Param("nombreUsuarioIn") String nombreUsuario,
			@Param("appUsuarioIn") String appUsuario, @Param("apmUsuarioIn") String apmUsuario,
			@Param("emailUsuarioIn") String emailUsuario, @Param("telIn") String tel,
			@Param("operandoUsuarioIn") int operandoUsuario, @Param("nombreRolIn") String nombreRol,
			@Param("correoUsuarioIn") String correoUsuario);

	/**
	 * Elimina un usuario
	 * 
	 * @param emailidEmailUsuario     emailidEmailUsuario: correo de un usuario.
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo
	 * 
	 * @return obtiene un entero para saber si se realizao la accion o no
	 */
	@Query(value = "{call sp_eliminar_usuario(:idEmailUsuarioIn,:correoUsuarioModificandoIn)}", nativeQuery = true)
	int usuarioEliminarProcedure(@Param("idEmailUsuarioIn") String emailidEmailUsuario,
			@Param("correoUsuarioModificandoIn") String correoUsuarioModificando);

	/**
	 * modificando la contraseña de un usuario.
	 * 
	 * @param emailidEmailUsuario     emailidEmailUsuario: correo de un usuario.
	 * @param password                password: Nueva contraseña de un usuario.
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo
	 * 
	 * @return obtiene un entero para saber si se realizao la accion o no
	 */
	@Query(value = "{call sp_actualizar_password_usuario(:idEmailUsuarioIn,:passwordIn,:correoUsuarioModificandoIn)}", nativeQuery = true)
	int cambiarPasswordUsuarioProcedure(@Param("idEmailUsuarioIn") String emailidEmailUsuario,
			@Param("passwordIn") String password, @Param("correoUsuarioModificandoIn") String correoUsuarioModificando);
}
