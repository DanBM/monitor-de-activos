package com.eclipsemex.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.eclipsemex.model.Estado;

/**
 * Repositorio de un Estado que extiende de JPARepository y tiene como modelo
 * Estado
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
public interface EstadoRepository extends JpaRepository<Estado, Integer> {
	/**
	 * Obtiene todos los estados por medio de un procedimiento almacenado
	 * 
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo
	 * 
	 * @return lista de objetos de los estados encontrados
	 */
	@Query(value = "{call sp_obtener_estados(:emailUsuarioModificandoIn)}", nativeQuery = true)
	List<Object[]> listaEstadosProcedure(@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);
}
