package com.eclipsemex.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.eclipsemex.model.Bitacora;

/**
 * Repositorio de una bitacora que extiende de JPARepository y tiene como modelo
 * Bitacora
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
public interface BitacoraRepository extends JpaRepository<Bitacora, Integer> {
	/**
	 * Obtiene todas las acciones que se han registrado en la bitacora
	 * 
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo
	 * 
	 * @return lista de objetos de las acciones registradas en la bitacora
	 */
	@Query(value = "{call sp_obtener_acciones(:emailUsuarioModificandoIn)}", nativeQuery = true)
	List<Object[]> listaAccionesBitacoraProcedure(@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);
}
