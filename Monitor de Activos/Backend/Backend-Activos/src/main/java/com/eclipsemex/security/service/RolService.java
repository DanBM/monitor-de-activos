package com.eclipsemex.security.service;

import com.eclipsemex.security.entity.Rol;
import com.eclipsemex.security.enums.RolNombre;
import com.eclipsemex.security.repository.RolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Servicio de un rol
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@Service
@Transactional
public class RolService {

	@Autowired
	RolRepository rolRepository;

	/**
	 * Obtiene un rol por nombre
	 * 
	 * @param rolNombre rolNombre: representa un objeto de la clase enumeradora
	 *                  RolNombre.
	 * 
	 * @return Objeto optional de los roles.
	 */
	public Optional<Rol> getByRolNombre(RolNombre rolNombre) {
		return rolRepository.findByRolNombre(rolNombre);
	}

	public void save(Rol rol) {
		rolRepository.save(rol);
	}
}
