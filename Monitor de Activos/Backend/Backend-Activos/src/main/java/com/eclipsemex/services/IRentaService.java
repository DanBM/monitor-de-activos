package com.eclipsemex.services;

import java.math.BigInteger;

import org.springframework.security.core.Authentication;

import com.eclipsemex.model.Renta;

/**
 * Interfaz donde se definen los metodos de una Renta
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
public interface IRentaService {
	/**
	 * Agrega un nueva nueva renta
	 * 
	 * @body renta: representa el modelo de una renta
	 * 
	 * @param auth: Representa el token para una solicitud de autenticación no se
	 *              manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	String agregarRenta(Renta renta, Authentication auth);

	/**
	 * Modificar una renta
	 * 
	 * @body renta: representa el modelo de una renta
	 * 
	 * @param auth: Representa el token para una solicitud de autenticación no se
	 *              manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	String modificarRenta(Renta renta, Authentication auth);

	/**
	 * Elimina una renta
	 * 
	 * @param idRenta: representa el un id de una renta
	 * 
	 * @param auth:    Representa el token para una solicitud de autenticación no se
	 *                 manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	String eliminarRenta(BigInteger idRenta, Authentication auth);

	/**
	 * Obtiene todas las rentas de los usuarios
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	String buscarTodasRentasUsuarios(Authentication auth);

	/**
	 * Obtiene las rentas de un usuario
	 * 
	 * @param email: representa el correo de un usuario
	 * 
	 * @param auth   Representa el token para una solicitud de autenticación no se
	 *               manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	String buscarRentasUsuario(String idUsuario, Authentication auth);

	/**
	 * Obtiene el detalle de una renta
	 * 
	 * @param idRenta: representa el id de una renta
	 * 
	 * @param auth     Representa el token para una solicitud de autenticación no se
	 *                 manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	String detalleRentaUsuario(BigInteger idRenta, Authentication auth);
}
