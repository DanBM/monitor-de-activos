package com.eclipsemex.services;

import org.springframework.security.core.Authentication;

/*
 * Interfaz donde se definen metodos que se requieren
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021  
 * */
public interface IRolService {

	String buscarTodos(Authentication auth);

}
