const form = document.getElementById('formularioActivo');
const submitButton = document.getElementById('submit-btn');
let timeout = null;
const camposformat = /^[A-Za-z0-9\s]+$/g;
document.querySelectorAll('.form-group').forEach((box) => {
    if (box.querySelector('input')) {
        const boxInput = box.querySelector('input');
        boxInput.addEventListener('keydown', (event) => {
            clearTimeout(timeout);
            timeout = setTimeout(() => {
                console.log(`Input ${boxInput.name} value: `, boxInput.value);
                validation(box, boxInput);
            }, 300);
        });
    }
});
validation = (box, boxInput) => {
    if (boxInput.name == 'txtplaca') {
        if (boxInput.value == '') {
            showError(true, box, boxInput);
        } else {
            showError(false, box, boxInput);
        }
        if (!boxInput.value.match(camposformat)) {
            showError(true, box, boxInput);
        } else {
            showError(false, box, boxInput);
        }
    }
    if (boxInput.name == 'txtmodelo') {
        if (boxInput.value == '') {
            showError(true, box, boxInput);
        } else {
            showError(false, box, boxInput);
        }
        if (!boxInput.value.match(camposformat)) {
            showError(true, box, boxInput);
        } else {
            showError(false, box, boxInput);
        }
    }
    if (boxInput.name == 'txtserie') {
        if (boxInput.value == '') {
            showError(true, box, boxInput);
        } else {
            showError(false, box, boxInput);
        }
        if (!boxInput.value.match(camposformat)) {
            showError(true, box, boxInput);
        } else {
            showError(false, box, boxInput);
        }
    }
    if (boxInput.name == 'habilitado') {
        if (boxInput.value == '') {
            showError(true, box, boxInput);
        } else {
            showError(false, box, boxInput);
        }
    }
};
showError = (check, box) => {
    if (check) {
        box.classList.remove('form-success');
        box.classList.add('form-error');
    } else {
        box.classList.remove('form-error');
        box.classList.add('form-success');
    }
};