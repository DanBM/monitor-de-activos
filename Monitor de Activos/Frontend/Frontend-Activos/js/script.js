$(function() {
    if ($.fn.DataTable) {
        $('#query_table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
            }
        });
    }

    if ($.fn.flexiblePagination) {
      var flexiblePagination = $("#products").flexiblePagination({
        itemsPerPage: 12,
        displayedPages: 5,
        itemSelector: "div.result:visible",
        searchBoxSelector: "#searchForm input",
        btnFirstText: '<i class="fal fa-angle-double-left fa-fw"></i>',
        btnLastText: '<i class="fal fa-angle-double-right fa-fw"></i>',
        btnNextText: '<i class="fal fa-angle-right fa-fw"></i>',
        btnPreviousText: '<i class="fal fa-angle-left fa-fw"></i>',
        css: {
          btnNumberingClass: "btn btn-sm btn-info",
          btnActiveClass: "btn btn-sm btn-outline-info disabled",
          btnFirstClass: "btn btn-sm btn-info",
          btnLastClass: "btn btn-sm btn-info",
          btnNextClass: "btn btn-sm btn-info",
          btnPreviousClass: "btn btn-sm btn-info",
        },
      });
      flexiblePagination.getController().onPageClick = function (pageNum, e) {};
    }

});