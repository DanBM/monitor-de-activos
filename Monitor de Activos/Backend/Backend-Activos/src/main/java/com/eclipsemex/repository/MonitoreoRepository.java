package com.eclipsemex.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.eclipsemex.model.Monitoreo;

/**
 * Repositorio de un Monitoreo que extiende de JPARepository y tiene como modelo
 * Monitoreo
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
public interface MonitoreoRepository extends JpaRepository<Monitoreo, Integer> {
	/**
	 * Agrega un monitoreo
	 * 
	 * @param comentario              comentario: comentario de un monitoreo.
	 * @param idSensor                idSensor: id de un sensor
	 * @param idActivo                idActivo: id de un sensor.
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo.
	 * 
	 * @return obtiene un entero para saber si se realizao la accion o no
	 */
	@Query(value = "{call sp_agregar_monitoreo(:comentarioIn,:idSensorIn,:idActivoIn,:emailUsuarioModificandoIn)}", nativeQuery = true)
	int registrarMonitoreoProcedure(@Param("comentarioIn") String comentario, @Param("idSensorIn") BigInteger idSensor,
			@Param("idActivoIn") BigInteger idActivo,
			@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);

	/**
	 * Modifica un monitoreo
	 * 
	 * @param idMonitoreo             idMonitoreo: id de un Monitoreo
	 * @param comentario              comentario: comentario de un monitoreo.
	 * @param idSensor                idSensor: id de un sensor.
	 * @param idActivo                idActivo: id de un sensor.
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo.
	 * 
	 * @return obtiene un entero para saber si se realizao la accion o no
	 */
	@Query(value = "{call sp_actualizar_monitoreo(:idMonitoreoIn,:comentarioIn,:idSensorIn,:idActivoIn,:emailUsuarioModificandoIn)}", nativeQuery = true)
	int modificarMonitoreoProcedure(@Param("idMonitoreoIn") BigInteger idMonitoreo,
			@Param("comentarioIn") String comentario, @Param("idSensorIn") BigInteger idSensor,
			@Param("idActivoIn") BigInteger idActivo,
			@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);

	/**
	 * Obtiene El detalle de un Monitoreo a partir de su idMonitoreo por medio de un
	 * procedimiento almacenado
	 * 
	 * @param idMonitoreo             idMonitoreo: id de un Monitoreo
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo
	 * 
	 * @return lista de objetos de los activos encontrados
	 */
	@Query(value = "{call sp_obtener_monitoreo_idMonitoreo(:idMonitoreoIn,:emailUsuarioModificandoIn)}", nativeQuery = true)
	List<Object[]> monitoreoDetalleProcedure(@Param("idMonitoreoIn") BigInteger idMonitoreo,
			@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);

	/**
	 * Obtiene todos los monitoreo por medio de un procedimiento almacenado
	 * 
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo
	 * 
	 * @return lista de objetos de los activos encontrados
	 */
	@Query(value = "{call sp_obtener_monitoreos(:emailUsuarioModificandoIn)}", nativeQuery = true)
	List<Object[]> obtenerMonitoreosProcedure(@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);

	/**
	 * Obtiene El detalle de un Monitoreo a partir de su idActivo por medio de un
	 * procedimiento almacenado
	 * 
	 * @param idActivo                idActivo: id de un Activo
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo
	 * 
	 * @return lista de objetos de los activos encontrados
	 */
	@Query(value = "{call sp_obtener_monitoreo_idActivo(:idActivoIn,:emailUsuarioModificandoIn)}", nativeQuery = true)
	List<Object[]> obtenerMonitoreosActivoProcedure(@Param("idActivoIn") BigInteger idActivo,
			@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);
}
