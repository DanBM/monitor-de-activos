package com.eclipsemex.services.db;

import java.math.BigInteger;

import java.util.LinkedList;
import java.util.List;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.eclipsemex.model.Monitoreo;
import com.eclipsemex.repository.MonitoreoRepository;
import com.eclipsemex.security.entity.UsuarioPrincipal;
import com.eclipsemex.security.enums.Errores;
import com.eclipsemex.services.IMonitoreoService;

/**
 * Clase de servicio para los Monitoreos
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@Service
@Primary
public class MonitoreoServiceJpa implements IMonitoreoService {

	@Autowired
	private MonitoreoRepository repoMonitoreo;
	private UsuarioPrincipal usuarioPrincipal;

	/**
	 * Agrega un nuevo monitoreo
	 * 
	 * @body monitoreo: representa el modelo de un monitoreo
	 * 
	 * @param auth: Representa el token para una solicitud de autenticación no se
	 *              manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@Override
	public String registrarMonitoreo(Monitoreo monitoreo, Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		if (monitoreo.getComentario().isEmpty() || monitoreo.getComentario().trim() == null
				|| monitoreo.getComentario().trim().length() <= 0 || monitoreo.getComentario().length() > 200
				|| alfanumerico(monitoreo.getComentario().trim().replace(" +", "")) == false)
			return respuestaJson("Error",
					"El campo 'comentario' debe estar lleno, tener solo letras o numeros y debe tener entre 1 y 6 caracteres.",
					HttpStatus.BAD_REQUEST);
		if (soloNumeros(String.valueOf(monitoreo.getIdSensor())) == false)
			return respuestaJson("Error", "El campo 'idActivo' debe estar lleno,y tener solo numeros.",
					HttpStatus.BAD_REQUEST);
		if (soloNumeros(String.valueOf(monitoreo.getIdActivo())) == false)
			return respuestaJson("Error", "El campo 'idActivo' debe estar lleno,y tener solo numeros.",
					HttpStatus.BAD_REQUEST);
		int status = repoMonitoreo.registrarMonitoreoProcedure(monitoreo.getComentario().trim().replace(" +", ""),
				monitoreo.getIdSensor(), monitoreo.getIdActivo(), emailUsuarioModificando);
		if (status == 1)
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.NO_CONTENT);
		if (status == 2)
			return respuestaJson("Error", Errores.Sensor_No_Encontrado, HttpStatus.NO_CONTENT);
		if (status == 3)
			return respuestaJson("Error", Errores.Activo_No_Encontrado, HttpStatus.NO_CONTENT);
		return respuestaJson("Éxito", "Activo registrado", HttpStatus.OK);
	}

	/**
	 * Modificar un nuevo monitoreo
	 * 
	 * @body monitoreo: representa el modelo de un monitoreo
	 * 
	 * @param auth: Representa el token para una solicitud de autenticación no se
	 *              manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@Override
	public String modificarMonitoreo(Monitoreo monitoreo, Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		if (soloNumeros(String.valueOf(monitoreo.getIdMonitoreo())) == false)
			return respuestaJson("Error", "El campo 'idMonitoreo' debe estar lleno,y tener solo numeros.",
					HttpStatus.BAD_REQUEST);
		if (monitoreo.getComentario().isEmpty() || monitoreo.getComentario().trim() == null
				|| monitoreo.getComentario().trim().length() <= 0 || monitoreo.getComentario().length() > 200
				|| alfanumerico(monitoreo.getComentario().trim().replace(" +", "")) == false)
			return respuestaJson("Error",
					"El campo 'comentario' debe estar lleno, tener solo letras o numeros y debe tener entre 1 y 6 caracteres.",
					HttpStatus.BAD_REQUEST);
		if (soloNumeros(String.valueOf(monitoreo.getIdSensor())) == false)
			return respuestaJson("Error", "El campo 'idActivo' debe estar lleno,y tener solo numeros.",
					HttpStatus.BAD_REQUEST);
		if (soloNumeros(String.valueOf(monitoreo.getIdActivo())) == false)
			return respuestaJson("Error", "El campo 'idActivo' debe estar lleno,y tener solo numeros.",
					HttpStatus.BAD_REQUEST);
		int status = repoMonitoreo.modificarMonitoreoProcedure(monitoreo.getIdMonitoreo(), monitoreo.getComentario(),
				monitoreo.getIdSensor(), monitoreo.getIdActivo(), emailUsuarioModificando);
		if (status == 1)
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.NO_CONTENT);
		if (status == 2)
			return respuestaJson("Error", Errores.Monitoreo_No_Encontrado, HttpStatus.NO_CONTENT);
		if (status == 3)
			return respuestaJson("Error", Errores.Sensor_No_Encontrado, HttpStatus.NO_CONTENT);
		if (status == 4)
			return respuestaJson("Error", Errores.Activo_No_Encontrado, HttpStatus.NO_CONTENT);
		return respuestaJson("Éxito", "Activo registrado", HttpStatus.OK);
	}

	/**
	 * Obtiene todos los monitoreos
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@Override
	public String obtenerListaMonitoreos(Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		List<Object[]> todosMonitoreos = repoMonitoreo.obtenerMonitoreosProcedure(emailUsuarioModificando);
		Object excepcion = (Object) todosMonitoreos.get(0)[0];
		if (excepcion.equals("1")) {
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.BAD_REQUEST);
		} else if (excepcion.equals("2")) {
			return respuestaJson("Error", Errores.No_Hay_Resultados, HttpStatus.NO_CONTENT);
		} else {
			List<Monitoreo> monitoreos = new LinkedList();
			for (Object[] objects : todosMonitoreos) {
				BigInteger idMonitoreo = (BigInteger) objects[0];
				String fechaRevision = (String) objects[1];
				String comentario = (String) objects[2];
				BigInteger idSensor = (BigInteger) objects[3];
				String nomSensor = (String) objects[4];
				BigInteger idActivo = (BigInteger) objects[5];
				String placa = (String) objects[6];
				Monitoreo activo = new Monitoreo(idMonitoreo, fechaRevision, comentario, idSensor, nomSensor, idActivo,
						placa);
				monitoreos.add(activo);
			}
			return respuestaJson("Éxito", monitoreos, HttpStatus.OK);
		}
	}

	/**
	 * Obtiene el monitoreo de un activo
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@Override
	public String obtenerMonitoreosActivo(BigInteger idActivoB, Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		List<Object[]> todosMonitoreos = repoMonitoreo.obtenerMonitoreosActivoProcedure(idActivoB,
				emailUsuarioModificando);
		Object excepcion = (Object) todosMonitoreos.get(0)[0];
		if (excepcion.equals("1")) {
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.BAD_REQUEST);
		} else if (excepcion.equals("2")) {
			return respuestaJson("Error", Errores.No_Hay_Resultados, HttpStatus.NO_CONTENT);
		} else {
			List<Monitoreo> monitoreos = new LinkedList();
			for (Object[] objects : todosMonitoreos) {
				BigInteger idMonitoreo = (BigInteger) objects[0];
				String fechaRevision = (String) objects[1];
				String comentario = (String) objects[2];
				BigInteger idSensor = (BigInteger) objects[3];
				String nomSensor = (String) objects[4];
				BigInteger idActivo = (BigInteger) objects[5];
				String placa = (String) objects[6];
				Monitoreo activo = new Monitoreo(idMonitoreo, fechaRevision, comentario, idSensor, nomSensor, idActivo,
						placa);
				monitoreos.add(activo);
			}
			return respuestaJson("Éxito", monitoreos, HttpStatus.OK);
		}
	}

	/**
	 * Obtiene el detalle de un monitoreo
	 * 
	 * @param idMonitoreo: representa el id de un monitoreo
	 * 
	 * @param auth         Representa el token para una solicitud de autenticación
	 *                     no se manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@Override
	public String monitoreoDetalle(BigInteger idMonitoreoB, Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		List<Object[]> todosMonitoreos = repoMonitoreo.monitoreoDetalleProcedure(idMonitoreoB, emailUsuarioModificando);
		Object excepcion = (Object) todosMonitoreos.get(0)[0];
		if (excepcion.equals("1")) {
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.BAD_REQUEST);
		}
		if (excepcion.equals("2")) {
			return respuestaJson("Error", Errores.No_Hay_Resultados, HttpStatus.NO_CONTENT);
		} else {
			List<Monitoreo> monitoreos = new LinkedList();
			for (Object[] objects : todosMonitoreos) {
				BigInteger idMonitoreo = (BigInteger) objects[0];
				String fechaRevision = (String) objects[1];
				String comentario = (String) objects[2];
				BigInteger idSensor = (BigInteger) objects[3];
				String nomSensor = (String) objects[4];
				BigInteger idActivo = (BigInteger) objects[5];
				String placa = (String) objects[6];
				Monitoreo activo = new Monitoreo(idMonitoreo, fechaRevision, comentario, idSensor, nomSensor, idActivo,
						placa);
				monitoreos.add(activo);
			}
			return respuestaJson("Éxito", monitoreos, HttpStatus.OK);
		}
	}

	/**
	 * Genera un JSON de respuesta en String
	 * 
	 * @param respuesta que respuesta se requiere mostrar
	 * 
	 * @param contenido objeto que puede tener cualquier estructura para mostrar
	 * 
	 * @param codigo    codigo de respuesta que se desea enviar
	 * 
	 * @return JSON con valores enviados en formato string
	 */
	private String respuestaJson(String respuesta, Object contenido, HttpStatus codigo) {
		JSONObject objson = new JSONObject();
		objson.put("Respuesta", respuesta);
		objson.put("Contenido", contenido);
		objson.put("Codigo http", codigo);
		return objson.toString();
	}

	/**
	 * Valida que una cadena tenga solo letras y numeros
	 * 
	 * @param cadena que respuesta se requiere mostrar
	 * 
	 * @return true o false si el correo es valido o incorrecto segun su caso
	 */
	private boolean alfanumerico(String cadena) {
		Pattern pat = Pattern.compile("[ A-Za-z0-9äÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓ	ÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙñÑ.-]+");
		Matcher mat = pat.matcher(cadena);
		return mat.matches();
	}

	/**
	 * Valida que una cadena tenga solo numeros
	 * 
	 * @param cadena que respuesta se requiere mostrar
	 * 
	 * @return true o false si el correo es valido o incorrecto segun su caso
	 */
	private boolean soloNumeros(String cadena) {
		if (cadena.matches("[0-9]*")) {
			return true;
		} else {
			return false;
		}
	}

}
