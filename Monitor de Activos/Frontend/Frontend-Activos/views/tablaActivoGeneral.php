<?php
/*Se verefica que el rol ingresado tenga el permiso para estar en esta pagania o pestaña.
@author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
@version 23/02/2021*/
session_start();
$views = [
    'admin'   => 'vistaAdminnistrador.html',
    'gerente' => 'vistaGerente.html',
    'index'   => '../index.php',
];
if ($_SESSION['tokenGuardado'] == '') {
    header('Location: ' . $views['index']);
} else {
    if ($_SESSION['rol'] == 'ROLE_ADMIN') {
        header('Location: ' . $views['admin']);

    }if ($_SESSION['rol'] == 'ROLE_GERENTE') {
        header('Location: ' . $views['gerente']);

    }
}
$curl = curl_init();
/* Se verifica que el token exista para poder utilizar la pagina.
@author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
@version 23/02/2021
 */

curl_setopt_array($curl, array(
    CURLOPT_URL            => 'http://localhost:8080/rest/activos',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING       => '',
    CURLOPT_MAXREDIRS      => 10,
    CURLOPT_TIMEOUT        => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST  => 'GET',
    CURLOPT_HTTPHEADER     => array(
        'Authorization: Bearer ' . $_SESSION['tokenGuardado'],
    ),
));

$response = curl_exec($curl);
curl_close($curl);
$dataToJson = json_decode($response, true);
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Tabla Activo</title>
        <meta name="description" content="A sidebar menu as seen on the Google Nexus 7 website" />
        <meta name="keywords" content="google nexus 7 menu, css transitions, sidebar, side menu, slide out menu" />
        <meta name="author" content="Codrops" />
        <link rel="shortcut icon" href="../favicon.ico">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.2/css/all.css" integrity="sha384-vSIIfh2YWi9wW0r9iZe7RJPrKwp6bG+s9QZMoITbCckVJqGCCRhc+ccxNcdpHuYu" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/validacion.css" />
        <link rel="stylesheet" type="text/css" href="css/normalize.css" />
        <link rel="stylesheet" type="text/css" href="css/demo.css" />
        <link rel="stylesheet" type="text/css" href="css/component.css" />
         <!-- Librerias para la tabla -->
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js"></script>

    <!-- Datatables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/v/bs4-4.1.1/dt-1.10.18/datatables.min.css">
    <script src="https://cdn.datatables.net/v/bs4-4.1.1/dt-1.10.18/datatables.min.js"></script>

    <!-- Buttons -->
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.53/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.53/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>




        <script src="js/modernizr.custom.js"></script>
    </head>
    <body>
        <div class="container">
            <ul id="gn-menu" class="gn-menu-main">
                <li class="gn-trigger">
                    <a class="gn-icon gn-icon-menu"><span>Menú</span></a>
                    <nav class="gn-menu-wrapper">
                        <div class="gn-scroller">
                            <ul class="gn-menu">

                                <li>
                                    <a href="salir.php" class="gn-icon gn-icon-downloa">Salir</a>
                                    <ul class="gn-submenu">
                                        <li><a href="tablaActivoGeneral.php" class="gn-icon gn-icon-photosh">Activos</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="tablaRentasAdmin.php" class="gn-icon gn-icon-archive">Rentas</a>

                                </li>
                            </ul>
                        </div><!-- /gn-scroller -->
                    </nav>
                </li>
                <li><a href="">Sistema de Monitoreo de Activos</a></li>

                <li><a class="codrops-icon codrops-icon-" href="vistaGeneral.html"><span>Home</span></a></li>
            </ul>
            <br><br><br>
        </div><!-- /container -->
<section class="contenidoTabla">
    <div class="table-responsive" id="mydatatable-container">
        <table class="records_list table table-striped table-bordered table-hover" id="mydatatable">
            <thead>
              <tr>
                <th scope="col">Placa</th>
                <th scope="col">Modelo</th>
                <th scope="col">Serie</th>
                <th scope="col">Funcionando</th>

              </tr>
            </thead>
             <tfoot>
                <tr>
                    <th>Filter..</th>
                    <th>Filter..</th>
                    <th>Filter..</th>
                    <th>Filter..</th>

                </tr>
            </tfoot>
            <tbody>
                <?php foreach ($dataToJson['Contenido'] as $activo) {?>
            <tr>

                <td><?php echo $activo['placa'] ?></td>
                <td><?php echo $activo['modelo'] ?></td>
                <td><?php echo $activo['serie'] ?></td>
                <td><?php echo ($activo['disponible'] == 1) ? 'Si' : 'No' ?></td>

              </tr>
                <?php }?>
            </tbody>
        </table>
    </div>

</section>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#mydatatable tfoot th').each(function () {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Filtrar.." />');
            });

            var table = $('#mydatatable').DataTable({
                "dom": 'B<"float-left"i><"float-right"f>t<"float-left"l><"float-right"p><"clearfix">',
                "responsive": false,
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                },
                "order": [
                    [0, "desc"]
                ],
                "initComplete": function () {
                    this.api().columns().every(function () {
                        var that = this;

                        $('input', this.footer()).on('keyup change', function () {
                            if (that.search() !== this.value) {
                                that
                                    .search(this.value)
                                    .draw();
                            }
                        });
                    })
                },
                "buttons": [ 'excel', 'pdf']
            });
        });
    </script>

        <script src="js/classie.js"></script>
        <script src="js/gnmenu.js"></script>
        <script>
            new gnMenu( document.getElementById( 'gn-menu' ) );
        </script>
<!-- /Cerrar Pagina en 15 min Script
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
-->
 <div id="warning" style="display: none;" class="modal fade">
            <p>
                Tiempo para finalizar la sesion.
                <span>
                </span>
            </p>
            <p>
                <button id="close" type="button">
                    Close
                </button>
            </p>
        </div>

<script type="text/javascript">
var timeOfLastActivity;
var timeForCloseSession;
var timeBeforeWarning = 5000;   // 5 seconds
var timeBeforeClose = 900000;    // 15 min
var timeForUpdateWarning = 500; // 0.5 seconds

function resetAll() {
    timeOfLastActivity = new Date().getTime();
    timeForCloseSession = timeOfLastActivity + timeBeforeClose;
    setTimeout(function() {
        $("#warning").show();
        showTime();
    }, timeBeforeWarning);
    $("#warning").hide();
}

function showTime() {
    var currentTime = new Date().getTime();
    if (currentTime < timeForCloseSession) {
        $("#warning span").html(timeForCloseSession - currentTime)
    } else {
        $("#close").trigger("click");
    }
    if ($("#warning").is(":visible")) {
        setTimeout(showTime, timeForUpdateWarning);
    }
}

$("#close").click(function() {
    $("#warning").hide();
    location.href ="../index.php";
    $.post("/echo/json/"); // Send AJAX close
});


resetAll();
        </script>
    </body>
</html>