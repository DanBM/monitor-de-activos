const form = document.getElementById('formularioUsu');
const submitButton = document.getElementById('submit-btn');
let timeout = null;
const mailformatRegex = /^[^@]+@\w+(\.\w+)+\w$/;
const camposformat = /^[A-Za-z\s]+$/g;
const numformat = /^([0-9])*$/;
const regexp_password = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]/;
document.querySelectorAll('.form-group').forEach((box) => {
    if (box.querySelector('input')) {
        const boxInput = box.querySelector('input');
        boxInput.addEventListener('keydown', (event) => {
            clearTimeout(timeout);
            timeout = setTimeout(() => {
                console.log(`Input ${boxInput.name} value: `, boxInput.value);
                validation(box, boxInput);
            }, 300);
        });
    }
});
validation = (box, boxInput) => {
    if (boxInput.name == 'txtnombre') {
        if (boxInput.value == '') {
            showError(true, box, boxInput);
        } else {
            showError(false, box, boxInput);
        }
        if (!boxInput.value.match(camposformat)) {
            showError(true, box, boxInput);
        } else {
            showError(false, box, boxInput);
        }
    }
    if (boxInput.name == 'txtapp') {
        if (boxInput.value == '') {
            showError(true, box, boxInput);
        } else {
            showError(false, box, boxInput);
        }
        if (!boxInput.value.match(camposformat)) {
            showError(true, box, boxInput);
        } else {
            showError(false, box, boxInput);
        }
    }
    if (boxInput.name == 'txtapm') {
        if (boxInput.value == '') {
            showError(true, box, boxInput);
        } else {
            showError(false, box, boxInput);
        }
        if (!boxInput.value.match(camposformat)) {
            showError(true, box, boxInput);
        } else {
            showError(false, box, boxInput);
        }
    }
    if (boxInput.name == 'txtemail') {
        if (boxInput.value == '') {
            showError(true, box, boxInput);
        } else {
            showError(false, box, boxInput);
        }
        if (!boxInput.value.match(mailformatRegex)) {
            showError(true, box, boxInput);
        } else {
            showError(false, box, boxInput);
        }
    }
    if (boxInput.name == 'txtpassword') {
        if (boxInput.value == '') {
            showError(true, box, boxInput);
        } else {
            showError(false, box, boxInput);
        }
        if (!boxInput.value.match(regexp_password)) {
            showError(true, box, boxInput);
        } else {
            showError(false, box, boxInput);
        }
    }
    if (boxInput.name == 'txttel') {
        if (boxInput.value == '') {
            showError(true, box, boxInput);
        } else {
            showError(false, box, boxInput);
        }
        if (!boxInput.value.match(numformat)) {
            showError(true, box, boxInput);
        } else {
            showError(false, box, boxInput);
        }
    }
};
showError = (check, box) => {
    if (check) {
        box.classList.remove('form-success');
        box.classList.add('form-error');
    } else {
        box.classList.remove('form-error');
        box.classList.add('form-success');
    }
};