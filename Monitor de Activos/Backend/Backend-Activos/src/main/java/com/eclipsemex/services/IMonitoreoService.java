package com.eclipsemex.services;

import java.math.BigInteger;

import org.springframework.security.core.Authentication;

import com.eclipsemex.model.Monitoreo;

/**
 * Interfaz donde se definen los metodos de un Activo
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
public interface IMonitoreoService {
	/**
	 * Agrega un nuevo monitoreo
	 * 
	 * @body monitoreo: representa el modelo de un monitoreo
	 * 
	 * @param auth: Representa el token para una solicitud de autenticación no se
	 *              manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	String registrarMonitoreo(Monitoreo monitoreo, Authentication auth);

	/**
	 * Modificar un nuevo monitoreo
	 * 
	 * @body monitoreo: representa el modelo de un monitoreo
	 * 
	 * @param auth: Representa el token para una solicitud de autenticación no se
	 *              manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	String modificarMonitoreo(Monitoreo monitoreo, Authentication auth);

	/**
	 * Obtiene el detalle de un monitoreo
	 * 
	 * @param idMonitoreo: representa el id de un monitoreo
	 * 
	 * @param auth         Representa el token para una solicitud de autenticación
	 *                     no se manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	String monitoreoDetalle(BigInteger idMonitoreo, Authentication auth);

	/**
	 * Obtiene todos los monitoreos
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	String obtenerListaMonitoreos(Authentication auth);

	/**
	 * Obtiene el monitoreo de un activo
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	String obtenerMonitoreosActivo(BigInteger idActivo, Authentication auth);
}
