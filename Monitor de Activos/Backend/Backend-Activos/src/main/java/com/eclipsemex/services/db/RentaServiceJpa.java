package com.eclipsemex.services.db;

import java.math.BigInteger;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.LinkedList;
import java.util.List;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.eclipsemex.model.Renta;

import com.eclipsemex.repository.RentaRepository;
import com.eclipsemex.security.entity.UsuarioPrincipal;
import com.eclipsemex.security.enums.Errores;
import com.eclipsemex.services.IRentaService;

/**
 * Clase de servicio para las Rentas
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@Service
@Primary
public class RentaServiceJpa implements IRentaService {

	@Autowired
	private RentaRepository repoRenta;
	private UsuarioPrincipal usuarioPrincipal;

	/**
	 * Agrega un nueva nueva renta
	 * 
	 * @body renta: representa el modelo de una renta
	 * 
	 * @param auth: Representa el token para una solicitud de autenticación no se
	 *              manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@Override
	public String agregarRenta(Renta renta, Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		if (validarFecha(renta.getFechaVencimiento()) == false || renta.getFechaVencimiento().isEmpty()
				|| renta.getFechaVencimiento() == null)
			return respuestaJson("Error",
					"El campo fecha entrega es incorrecto debe estar lleno y coincidir con el formato 'yyyy-MM-dd HH:mm:ss'",
					HttpStatus.NO_CONTENT);
		if (renta.getDireccion() == null || renta.getDireccion().length() <= 0 || renta.getDireccion().length() > 200
				|| renta.getDireccion().isEmpty())
			return respuestaJson("Error",
					"El campo direccion es incorrecto debe estar lleno y tener de 1 a 200 caracteres",
					HttpStatus.NO_CONTENT);
		if (soloNumeros(String.valueOf(renta.getIdEstado())) == false)
			return respuestaJson("Error", "El campo 'idEstado' debe estar lleno, contener solo numeros",
					HttpStatus.BAD_REQUEST);
		if (soloNumeros(String.valueOf(renta.getIdMunicipio())) == false)
			return respuestaJson("Error", "El campo 'idMunicipio' debe estar lleno, contener solo numeros",
					HttpStatus.BAD_REQUEST);
		if (soloNumeros(String.valueOf(renta.getIdActivo())) == false)
			return respuestaJson("Error", "El campo 'idActivo' debe estar lleno, contener solo numeros",
					HttpStatus.BAD_REQUEST);
		if (renta.getEmail().trim().isEmpty() || renta.getEmail().trim() == null
				|| renta.getEmail().trim().length() <= 0 || renta.getEmail().trim().length() >= 50
				|| correoValido(renta.getEmail().trim().replace(" +", "")) == false)
			return respuestaJson("Error", "El 'Email' debe estar lleno, formato valido y tener entre 1 y 50 caracteres",
					HttpStatus.BAD_REQUEST);
		int status = repoRenta.agregarRenta(renta.getFechaVencimiento(), renta.getDireccion(), renta.getIdEstado(),
				renta.getIdMunicipio(), renta.getIdActivo(), renta.getEmail(), emailUsuarioModificando);
		if (status == 1)
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.NO_CONTENT);
		if (status == 2)
			return respuestaJson("Error", Errores.Estado_No_Encontrado, HttpStatus.NO_CONTENT);
		if (status == 3)
			return respuestaJson("Error", Errores.Municipio_No_Encontrado, HttpStatus.NO_CONTENT);
		if (status == 4)
			return respuestaJson("Error", Errores.Activo_No_Encontrado, HttpStatus.NO_CONTENT);
		if (status == 5)
			return respuestaJson("Error", Errores.Usuario_No_Encontrado, HttpStatus.NO_CONTENT);
		return respuestaJson("Éxito", "Renta registrado", HttpStatus.OK);
	}

	/**
	 * Modificar una renta
	 * 
	 * @body renta: representa el modelo de una renta
	 * 
	 * @param auth: Representa el token para una solicitud de autenticación no se
	 *              manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@Override
	public String modificarRenta(Renta renta, Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		if (soloNumeros(String.valueOf(renta.getIdRenta())) == false)
			return respuestaJson("Error", "El campo 'idRenta' debe estar lleno, contener solo numeros",
					HttpStatus.BAD_REQUEST);
		if (validarFecha(renta.getFechaVencimiento()) == false || renta.getFechaVencimiento().isEmpty()
				|| renta.getFechaVencimiento() == null)
			return respuestaJson("Error",
					"El campo fecha entrega es incorrecto debe estar lleno y coincidir con el formato 'yyyy-MM-dd HH:mm:ss'",
					HttpStatus.NO_CONTENT);
		if (renta.getDireccion() == null || renta.getDireccion().length() <= 0 || renta.getDireccion().length() > 200
				|| renta.getDireccion().isEmpty())
			return respuestaJson("Error",
					"El campo direccion es incorrecto debe estar lleno y tener de 1 a 200 caracteres",
					HttpStatus.NO_CONTENT);
		if (soloNumeros(String.valueOf(renta.getIdEstado())) == false)
			return respuestaJson("Error", "El campo 'idEstado' debe estar lleno, contener solo numeros",
					HttpStatus.BAD_REQUEST);
		if (soloNumeros(String.valueOf(renta.getIdMunicipio())) == false)
			return respuestaJson("Error", "El campo 'idMunicipio' debe estar lleno, contener solo numeros",
					HttpStatus.BAD_REQUEST);
		if (soloNumeros(String.valueOf(renta.getIdActivo())) == false)
			return respuestaJson("Error", "El campo 'idActivo' debe estar lleno, contener solo numeros",
					HttpStatus.BAD_REQUEST);
		if (renta.getEmail().trim().isEmpty() || renta.getEmail().trim() == null
				|| renta.getEmail().trim().length() <= 0 || renta.getEmail().trim().length() >= 50
				|| correoValido(renta.getEmail().trim().replace(" +", "")) == false)
			return respuestaJson("Error", "El 'Email' debe estar lleno, formato valido y tener entre 1 y 50 caracteres",
					HttpStatus.BAD_REQUEST);
		if (renta.getOperandoRenta() < 0 || renta.getOperandoRenta() > 1
				|| soloNumeros(String.valueOf(renta.getOperandoRenta())) == false)
			return respuestaJson("Error",
					"El campo 'operandoRenta' debe estar lleno, contener solo letras y debe ser 0 o 1",
					HttpStatus.BAD_REQUEST);
		int status = repoRenta.modificarRenta(renta.getIdRenta(), renta.getFechaVencimiento(), renta.getDireccion(),
				renta.getIdEstado(), renta.getIdMunicipio(), renta.getIdActivo(), renta.getEmail(),
				renta.getOperandoRenta(), emailUsuarioModificando);
		if (status == 1)
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.NO_CONTENT);
		if (status == 2)
			return respuestaJson("Error", Errores.Renta_No_Encontrada, HttpStatus.NO_CONTENT);
		if (status == 3)
			return respuestaJson("Error", Errores.Estado_No_Encontrado, HttpStatus.NO_CONTENT);
		if (status == 4)
			return respuestaJson("Error", Errores.Municipio_No_Encontrado, HttpStatus.NO_CONTENT);
		if (status == 5)
			return respuestaJson("Error", Errores.Activo_No_Encontrado, HttpStatus.NO_CONTENT);
		if (status == 6)
			return respuestaJson("Error", Errores.Usuario_No_Encontrado, HttpStatus.NO_CONTENT);
		return respuestaJson("Éxito", "Renta Modificado", HttpStatus.OK);
	}

	/**
	 * Elimina una renta
	 * 
	 * @param idRenta: representa el un id de una renta
	 * 
	 * @param auth:    Representa el token para una solicitud de autenticación no se
	 *                 manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@Override
	public String eliminarRenta(BigInteger idRenta, Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		if (soloNumeros(String.valueOf(idRenta)) == false)
			return respuestaJson("Error", "El campo 'idEstado' debe estar lleno, contener solo numeros",
					HttpStatus.BAD_REQUEST);
		int status = repoRenta.eliminarRenta(idRenta, emailUsuarioModificando);
		if (status == 1)
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.NO_CONTENT);
		if (status == 2)
			return respuestaJson("Error", Errores.Renta_No_Encontrada, HttpStatus.NO_CONTENT);
		return respuestaJson("Éxito", "Renta Eliminado", HttpStatus.OK);
	}

	/**
	 * Obtiene todas las rentas de los usuarios
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@Override
	public String buscarTodasRentasUsuarios(Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		List<Object[]> todasRentasUsuarios = repoRenta.listaRentasUsuariosProcedure(emailUsuarioModificando);
		Object excepcion = (Object) todasRentasUsuarios.get(0)[0];
		if (excepcion.equals("1")) {
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.BAD_REQUEST);
		} else if (excepcion.equals("2")) {
			return respuestaJson("Error", Errores.No_Hay_Resultados, HttpStatus.NO_CONTENT);
		} else {
			List<Renta> rentas = new LinkedList();
			for (Object[] objects : todasRentasUsuarios) {
				BigInteger idRenta = (BigInteger) objects[0];
				String fechaInicio = (String) objects[1];
				String fechaVencimiento = (String) objects[2];
				String direccion = (String) objects[3];
				String estado = (String) objects[4];
				String municipio = (String) objects[5];
				String placActivo = (String) objects[6];
				String nombreUsuario = (String) objects[7] + " " + (String) objects[8] + " " + (String) objects[9];
				String email = (String) objects[10];
				int operandoRenta = (int) objects[11];
				Renta rent = new Renta(idRenta, fechaInicio, fechaVencimiento, direccion, estado, municipio, placActivo,
						nombreUsuario, email, operandoRenta);
				rentas.add(rent);
			}
			return respuestaJson("Éxito", rentas, HttpStatus.OK);
		}
	}

	/**
	 * Obtiene las rentas de un usuario
	 * 
	 * @param email: representa el correo de un usuario
	 * 
	 * @param auth   Representa el token para una solicitud de autenticación no se
	 *               manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@Override
	public String buscarRentasUsuario(String emailB, Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		List<Object[]> todasRentasUsuarios = repoRenta.listaRentasUsuarioProcedure(emailB, emailUsuarioModificando);
		Object excepcion = (Object) todasRentasUsuarios.get(0)[0];
		if (excepcion.equals("1")) {
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.BAD_REQUEST);
		} else if (excepcion.equals("2")) {
			return respuestaJson("Error", Errores.Usuario_No_Encontrado, HttpStatus.NO_CONTENT);
		} else if (excepcion.equals("3")) {
			return respuestaJson("Error", Errores.No_Hay_Resultados, HttpStatus.NO_CONTENT);
		} else {
			List<Renta> rentas = new LinkedList();
			for (Object[] objects : todasRentasUsuarios) {
				BigInteger idRenta = (BigInteger) objects[0];
				String fechaInicio = (String) objects[1];
				String fechaVencimiento = (String) objects[2];
				String direccion = (String) objects[3];
				String estado = (String) objects[4];
				String municipio = (String) objects[5];
				String placActivo = (String) objects[6];
				String nombreUsuario = (String) objects[7] + " " + (String) objects[8] + " " + (String) objects[9];
				String email = (String) objects[10];
				int operandoRenta = (int) objects[11];
				Renta rent = new Renta(idRenta, fechaInicio, fechaVencimiento, direccion, estado, municipio, placActivo,
						nombreUsuario, email, operandoRenta);
				rentas.add(rent);
			}
			return respuestaJson("Éxito", rentas, HttpStatus.OK);
		}
	}

	/**
	 * Obtiene el detalle de una renta
	 * 
	 * @param idRenta: representa el id de una renta
	 * 
	 * @param auth     Representa el token para una solicitud de autenticación no se
	 *                 manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@Override
	public String detalleRentaUsuario(BigInteger idRentaB, Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		List<Object[]> todasRentasUsuarios = repoRenta.rentaUsuarioDetalleProcedure(idRentaB, emailUsuarioModificando);
		Object excepcion = (Object) todasRentasUsuarios.get(0)[0];
		if (excepcion.equals("1")) {
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.BAD_REQUEST);
		} else if (excepcion.equals("2")) {
			return respuestaJson("Error", Errores.No_Hay_Resultados, HttpStatus.NO_CONTENT);
		} else {
			List<Renta> rentas = new LinkedList();
			for (Object[] objects : todasRentasUsuarios) {
				BigInteger idRenta = (BigInteger) objects[0];
				String fechaInicio = (String) objects[1];
				String fechaVencimiento = (String) objects[2];
				String direccion = (String) objects[3];
				BigInteger idEstado = (BigInteger) objects[4];
				String estado = (String) objects[5];
				BigInteger idMunicipio = (BigInteger) objects[6];
				String municipio = (String) objects[7];
				BigInteger idActivo = (BigInteger) objects[8];
				String placActivo = (String) objects[9];
				String nombreUsuario = (String) objects[10] + " " + (String) objects[11] + " " + (String) objects[12];
				String email = (String) objects[13];
				int operandoRenta = (int) objects[14];
				Renta rent = new Renta(idRenta, fechaInicio, fechaVencimiento, direccion, idEstado, estado, idMunicipio,
						municipio, idActivo, placActivo, nombreUsuario, email, operandoRenta);
				rentas.add(rent);
			}
			return respuestaJson("Éxito", rentas, HttpStatus.OK);
		}
	}

	/**
	 * Genera un JSON de respuesta en String
	 * 
	 * @param respuesta que respuesta se requiere mostrar
	 * 
	 * @param contenido objeto que puede tener cualquier estructura para mostrar
	 * 
	 * @param codigo    codigo de respuesta que se desea enviar
	 * 
	 * @return JSON con valores enviados en formato string
	 */
	public String respuestaJson(String respuesta, Object contenido, HttpStatus codigo) {
		JSONObject objson = new JSONObject();
		objson.put("Respuesta", respuesta);
		objson.put("Contenido", contenido);
		objson.put("Codigo http", codigo);
		return objson.toString();
	}

	/**
	 * Valida que una cadena tenga solo letras
	 * 
	 * @param cadena que respuesta se requiere mostrar
	 * 
	 * @return true o false si el correo es valido o incorrecto segun su caso
	 */
	public boolean soloLetras(String cadena) {
		Pattern pat = Pattern.compile("[ A-Za-zäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓ	ÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙñÑ.-]+");
		Matcher mat = pat.matcher(cadena);
		return mat.matches();
	}

	/**
	 * Valida que una cadena tenga solo numeros
	 * 
	 * @param cadena que respuesta se requiere mostrar
	 * 
	 * @return true o false si el correo es valido o incorrecto segun su caso
	 */
	private boolean soloNumeros(String cadena) {
		if (cadena.matches("[0-9]*")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Valida que un correo tenga formato valido
	 * 
	 * @param cadena que respuesta se requiere mostrar
	 * 
	 * @return true o false si el correo es valido o incorrecto segun su caso
	 */
	public boolean correoValido(String cadena) {
		Pattern pat = Pattern.compile(
				"^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");
		Matcher mat = pat.matcher(cadena);
		return mat.matches();
	}

	/**
	 * Valida que una fecha este correcta
	 * 
	 * @param cadena que respuesta se requiere mostrar
	 * 
	 * @return true o false si el correo es valido o incorrecto segun su caso
	 */
	public boolean validarFecha(String fecha) {
		try {
			SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			formatoFecha.setLenient(false);
			formatoFecha.parse(fecha);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}

}
