package com.eclipsemex.model;

import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Modelo que pertenece a la tabla de la base de datos monitoreo
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@Entity
@Table(name = "monitoreo")

public class Monitoreo {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger idMonitoreo;
	private String fechaRevision;
	private String comentario;
	private BigInteger idSensor;
	private String nomSensor;
	private BigInteger idActivo;
	private String placa;

	/**
	 * Constructor.
	 * 
	 * @param idMonitoreo   idMonitoreo: id de un monitoreo
	 * @param fechaRevision fechaRevision: fecha de revision de un monitoreo
	 * @param comentario    comentario: comentario de un monitoreo
	 * @param idSensor      idSensor: id de un sensor
	 * @param nomSensor     nomSensor: nombre de un sensor
	 * @param idActivo      idActivo: id de un activo
	 * @param placa         placa: placa de un activo
	 */
	public Monitoreo(BigInteger idMonitoreo, String fechaRevision, String comentario, BigInteger idSensor,
			String nomSensor, BigInteger idActivo, String placa) {
		super();
		this.idMonitoreo = idMonitoreo;
		this.fechaRevision = fechaRevision;
		this.comentario = comentario;
		this.idSensor = idSensor;
		this.nomSensor = nomSensor;
		this.idActivo = idActivo;
		this.placa = placa;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getNomSensor() {
		return nomSensor;
	}

	public void setNomSensor(String nomSensor) {
		this.nomSensor = nomSensor;
	}

	public BigInteger getIdMonitoreo() {
		return idMonitoreo;
	}

	public void setIdMonitoreo(BigInteger idMonitoreo) {
		this.idMonitoreo = idMonitoreo;
	}

	public BigInteger getIdSensor() {
		return idSensor;
	}

	public void setIdSensor(BigInteger idSensor) {
		this.idSensor = idSensor;
	}

	public BigInteger getIdActivo() {
		return idActivo;
	}

	public void setIdActivo(BigInteger idActivo) {
		this.idActivo = idActivo;
	}

	public String getFechaRevision() {
		return fechaRevision;
	}

	public void setFechaRevision(String fechaRevision) {
		this.fechaRevision = fechaRevision;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

}
