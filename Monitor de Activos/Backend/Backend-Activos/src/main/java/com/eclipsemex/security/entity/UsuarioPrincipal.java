package com.eclipsemex.security.entity;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.sun.istack.NotNull;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Modelo que sirve para guardar la sesion de un usuario cuando sus credenciales
 * de acceso son correctas
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
public class UsuarioPrincipal implements UserDetails {

	private String nombreUsuario;
	private String appUsuario;
	private String apmUsuario;
	private String email;
	private String password;
	private String tel;
	private int operandoUsuario;
	private Collection<? extends GrantedAuthority> authorities;

	/**
	 * Constructor.
	 * 
	 * @param nombreUsuario   nombreUsuario: nombre de un usuario.
	 * @param appUsuario      appUsuario: apellido paterno de un usuario.
	 * @param apmUsuario      apmUsuario: apellido materno de un usuario.
	 * @param email           email: correo de un usuario.
	 * @param password        password: contraseña de un usuario.
	 * @param tel             tel: telefono de un usuario.
	 * @param operandoUsuario operandoUsuario: estado de un usuario.
	 * @param authorities     authorities: Rol de un usuario.
	 */
	public UsuarioPrincipal(String nombreUsuario, String appUsuario, String apmUsuario, String email, String password,
			String tel, int operandoUsuario, Collection<? extends GrantedAuthority> authorities) {
		this.nombreUsuario = nombreUsuario;
		this.appUsuario = appUsuario;
		this.apmUsuario = apmUsuario;
		this.email = email;
		this.password = password;
		this.tel = tel;
		this.operandoUsuario = operandoUsuario;
		this.authorities = authorities;
	}

	public static UsuarioPrincipal build(Usuario usuario) {
		List<GrantedAuthority> authorities = usuario.getRoles().stream()
				.map(rol -> new SimpleGrantedAuthority(rol.getRolNombre().name())).collect(Collectors.toList());
		return new UsuarioPrincipal(usuario.getNombreUsuario(), usuario.getAppUsuario(), usuario.getApmUsuario(),
				usuario.getEmail(), usuario.getPassword(), usuario.getTel(), usuario.getOperandoUsuario(), authorities);
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return nombreUsuario;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public String getAppUsuario() {
		return appUsuario;
	}

	public void setAppUsuario(String appUsuario) {
		this.appUsuario = appUsuario;
	}

	public String getApmUsuario() {
		return apmUsuario;
	}

	public void setApmUsuario(String apmUsuario) {
		this.apmUsuario = apmUsuario;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public int getOperandoUsuario() {
		return operandoUsuario;
	}

	public void setOperandoUsuario(int operandoUsuario) {
		this.operandoUsuario = operandoUsuario;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
