package com.eclipsemex.services.db;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.eclipsemex.model.Activo;
import com.eclipsemex.repository.ActivoRepository;
import com.eclipsemex.security.entity.UsuarioPrincipal;
import com.eclipsemex.security.enums.Errores;
import com.eclipsemex.services.IActivoService;

/**
 * Clase de servicio para los activos
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@Service
@Primary
public class ActivoServiceJpa implements IActivoService {

	@Autowired
	private ActivoRepository repoActivo;
	private UsuarioPrincipal usuarioPrincipal;

	/**
	 * Obtiene todos los Activos
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@Override
	public String buscarTodos(Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		List<Object[]> todosRoles = repoActivo.listaActivosProcedure(emailUsuarioModificando);
		Object excepcion = (Object) todosRoles.get(0)[0];
		if (excepcion.equals("1")) {
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.BAD_REQUEST);
		} else if (excepcion.equals("2")) {
			return respuestaJson("Error", Errores.No_Hay_Resultados, HttpStatus.NO_CONTENT);
		} else {
			List<Activo> activos = new LinkedList();
			for (Object[] objects : todosRoles) {
				BigInteger idActivo = (BigInteger) objects[0];
				String placa = (String) objects[1];
				String modelo = (String) objects[2];
				String serie = (String) objects[3];
				int disponible = (int) objects[4];
				Activo activo = new Activo(idActivo, placa, modelo, serie, disponible);
				activos.add(activo);
			}
			return respuestaJson("Éxito", activos, HttpStatus.OK);
		}
	}

	/**
	 * Obtiene un activo en especifico
	 * 
	 * @param idActivo: representa un id de un activo
	 * 
	 * @param auth      Representa el token para una solicitud de autenticación no
	 *                  se manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@Override
	public String activoDeatlle(BigInteger idActivoB, Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		List<Object[]> todosRoles = repoActivo.activoDetalleProcedure(idActivoB, emailUsuarioModificando);
		Object excepcion = (Object) todosRoles.get(0)[0];
		if (excepcion.equals("1")) {
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.BAD_REQUEST);
		} else if (excepcion.equals("2")) {
			return respuestaJson("Error", Errores.No_Hay_Resultados, HttpStatus.NO_CONTENT);
		} else {
			List<Activo> activos = new LinkedList();
			for (Object[] objects : todosRoles) {
				BigInteger idActivo = (BigInteger) objects[0];
				String placa = (String) objects[1];
				String modelo = (String) objects[2];
				String serie = (String) objects[3];
				int disponible = (int) objects[4];
				Activo activo = new Activo(idActivo, placa, modelo, serie, disponible);
				activos.add(activo);
			}
			return respuestaJson("Éxito", activos, HttpStatus.OK);
		}
	}

	/**
	 * Agrega un nuevo Activo
	 * 
	 * @param activo: representa el modelo de un activo
	 * 
	 * @param auth:   Representa el token para una solicitud de autenticación no se
	 *                manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@Override
	public String agregarActivo(Activo activo, Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		if (activo.getPlaca().isEmpty() || activo.getPlaca().trim() == null || activo.getPlaca().trim().length() <= 0
				|| activo.getPlaca().length() > 6 || alfanumerico(activo.getPlaca().trim().replace(" +", "")) == false)
			return respuestaJson("Error",
					"El campo 'palca' debe estar lleno, tener solo letras o numeros y debe tener entre 1 y 6 caracteres.",
					HttpStatus.BAD_REQUEST);
		if (activo.getModelo().isEmpty() || activo.getModelo().trim() == null || activo.getModelo().trim().length() <= 0
				|| activo.getModelo().trim().length() > 30
				|| alfanumerico(activo.getModelo().trim().replace(" +", "")) == false)
			return respuestaJson("Error",
					"El campo 'modelo' debe estar lleno, tener solo letras o numeros y debe tener entre 1 y 30 caracteres.",
					HttpStatus.BAD_REQUEST);
		if (activo.getSerie().trim().isEmpty() || activo.getSerie().trim() == null
				|| activo.getSerie().trim().length() <= 0 || activo.getSerie().trim().length() > 30
				|| alfanumerico(activo.getSerie().trim().replace(" +", "")) == false)
			return respuestaJson("Error",
					"El 'serie' debe estar lleno, tener solo letras o numeros y debe tener entre 1 y 30 caracteres.",
					HttpStatus.BAD_REQUEST);
		int status = repoActivo.agregarActivoProcedure(activo.getPlaca().replace(" +", ""),
				activo.getModelo().replace(" +", ""), activo.getSerie().replace(" +", ""), emailUsuarioModificando);
		if (status == 1)
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.NO_CONTENT);
		return respuestaJson("Éxito", "Activo registrado", HttpStatus.OK);
	}

	/**
	 * Modificar un nuevo Activo
	 * 
	 * @param activo: representa el modelo de un activo
	 * 
	 * @param auth:   Representa el token para una solicitud de autenticación no se
	 *                manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@Override
	public String modificarActivo(Activo activo, Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		if (soloNumeros(String.valueOf(activo.getIdActivo())) == false)
			return respuestaJson("Error", "El campo 'idActivo' debe estar lleno,y tener solo numeros.",
					HttpStatus.BAD_REQUEST);
		if (activo.getPlaca().isEmpty() || activo.getPlaca().trim() == null || activo.getPlaca().trim().length() <= 0
				|| activo.getPlaca().length() > 6 || alfanumerico(activo.getPlaca().trim().replace(" +", "")) == false)
			return respuestaJson("Error",
					"El campo 'palca' debe estar lleno, tener solo letras o numeros y debe tener entre 1 y 6 caracteres.",
					HttpStatus.BAD_REQUEST);
		if (activo.getModelo().isEmpty() || activo.getModelo().trim() == null || activo.getModelo().trim().length() <= 0
				|| activo.getModelo().trim().length() > 30
				|| alfanumerico(activo.getModelo().trim().replace(" +", "")) == false)
			return respuestaJson("Error",
					"El campo 'modelo' debe estar lleno, tener solo letras o numeros y debe tener entre 1 y 30 caracteres.",
					HttpStatus.BAD_REQUEST);
		if (activo.getSerie().trim().isEmpty() || activo.getSerie().trim() == null
				|| activo.getSerie().trim().length() <= 0 || activo.getSerie().trim().length() > 30
				|| alfanumerico(activo.getSerie().trim().replace(" +", "")) == false)
			return respuestaJson("Error",
					"El 'serie' debe estar lleno, tener solo letras o numeros y debe tener entre 1 y 30 caracteres.",
					HttpStatus.BAD_REQUEST);
		if (activo.getDisponible() < 0 || activo.getDisponible() > 1
				|| soloNumeros(String.valueOf(activo.getIdActivo())) == false)
			return respuestaJson("Error",
					"El campo 'disponible' debe estar lleno, tener solo numeros y deber ser 1 o 0.",
					HttpStatus.BAD_REQUEST);
		int status = repoActivo.actualizarActivoProcedure(activo.getIdActivo(),
				activo.getPlaca().trim().replace(" +", ""), activo.getModelo().trim().replace(" +", ""),
				activo.getSerie().trim().replace(" +", ""), activo.getDisponible(), emailUsuarioModificando);
		if (status == 1)
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.NO_CONTENT);
		if (status == 2)
			return respuestaJson("Error", Errores.Activo_No_Encontrado, HttpStatus.NO_CONTENT);

		return respuestaJson("Éxito", "Activo Actualizado", HttpStatus.OK);
	}

	/**
	 * Eliminar un nuevo Activo
	 * 
	 * @param activo: representa el modelo de un activo
	 * 
	 * @param auth:   Representa el token para una solicitud de autenticación no se
	 *                manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@Override
	public String eliminarActivo(BigInteger idActivo, Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		if (soloNumeros(String.valueOf(idActivo)) == false)
			return respuestaJson("Error", "El campo 'idActivo' debe estar lleno,y tener solo numeros.",
					HttpStatus.BAD_REQUEST);
		int status = repoActivo.eliminarActivoProcedure(idActivo, emailUsuarioModificando);
		if (status == 1)
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.NO_CONTENT);
		if (status == 2)
			return respuestaJson("Error", Errores.Activo_No_Encontrado, HttpStatus.NO_CONTENT);

		return respuestaJson("Éxito", "Activo Eliminado", HttpStatus.OK);
	}

	/**
	 * Genera un JSON de respuesta en String
	 * 
	 * @param respuesta que respuesta se requiere mostrar
	 * 
	 * @param contenido objeto que puede tener cualquier estructura para mostrar
	 * 
	 * @param codigo    codigo de respuesta que se desea enviar
	 * 
	 * @return JSON con valores enviados en formato string
	 */
	private String respuestaJson(String respuesta, Object contenido, HttpStatus codigo) {
		JSONObject objson = new JSONObject();
		objson.put("Respuesta", respuesta);
		objson.put("Contenido", contenido);
		objson.put("Codigo http", codigo);
		return objson.toString();
	}

	/**
	 * Valida que una cadena tenga solo letras y numeros
	 * 
	 * @param cadena que respuesta se requiere mostrar
	 * 
	 * @return true o false si el correo es valido o incorrecto segun su caso
	 */
	private boolean alfanumerico(String cadena) {
		Pattern pat = Pattern.compile("[ A-Za-z0-9äÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓ	ÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙñÑ.-]+");
		Matcher mat = pat.matcher(cadena);
		return mat.matches();
	}

	/**
	 * Valida que una cadena tenga solo numeros
	 * 
	 * @param cadena que respuesta se requiere mostrar
	 * 
	 * @return true o false si el correo es valido o incorrecto segun su caso
	 */
	private boolean soloNumeros(String cadena) {
		if (cadena.matches("[0-9]*")) {
			return true;
		} else {
			return false;
		}
	}

}
