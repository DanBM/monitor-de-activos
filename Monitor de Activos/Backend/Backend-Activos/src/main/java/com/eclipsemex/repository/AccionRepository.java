package com.eclipsemex.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eclipsemex.model.Accion;

/**
 * Repositorio de una Accion que extiende de JPARepository y tiene como modelo
 * Accion
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
public interface AccionRepository extends JpaRepository<Accion, Integer> {

}
