package com.eclipsemex.security.service;

import com.eclipsemex.security.entity.Usuario;
import com.eclipsemex.security.entity.UsuarioPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Clase para usuario que implementa un interfaz para cargar un nuevo usuario
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	UsuarioService usuarioService;

	/**
	 * Busca un usuario por correo para obtener las credenciales
	 * 
	 * @body email email: es el correo de un usuario
	 * 
	 * @return regresa un modelo de un UserDetails
	 */
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Usuario usuario = usuarioService.getByEmail(email).get();
		return UsuarioPrincipal.build(usuario);
	}
}
