package com.eclipsemex.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Modelo que pertenece a la tabla de la base de datos cat_accion
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@Entity
@Table(name = "bitacora")
public class Bitacora {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idBitacora;
	private String fechaIngreso;
	private String nomAccion;
	private String nomUsuario;
	private String tablaModificada;

	/**
	 * Constructor.
	 * 
	 * @param fechaIngreso    fechaIngreso: fecha de ingreso de la accion.
	 * @param nomAccion       nomAccion: accion que se realizo.
	 * @param usuarioNom      usuarioNom: nombre de un usuario
	 * @param tablaModificada tablaModificada: la tabla a la que se esta modificando
	 */
	public Bitacora(String fechaIngreso, String nomAccion, String nomUsuario, String tablaModificada) {
		super();
		this.fechaIngreso = fechaIngreso;
		this.nomAccion = nomAccion;
		this.nomUsuario = nomUsuario;
		this.tablaModificada = tablaModificada;
	}

	public Integer getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(Integer idBitacora) {
		this.idBitacora = idBitacora;
	}

	public String getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(String fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getNomAccion() {
		return nomAccion;
	}

	public void setNomAccion(String nomAccion) {
		this.nomAccion = nomAccion;
	}

	public String getNomUsuario() {
		return nomUsuario;
	}

	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
	}

	public String getTablaModificada() {
		return tablaModificada;
	}

	public void setTablaModificada(String tablaModificada) {
		this.tablaModificada = tablaModificada;
	}
}