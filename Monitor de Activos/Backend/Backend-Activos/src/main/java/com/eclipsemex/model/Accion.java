package com.eclipsemex.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Modelo que pertenece a la tabla de la base de datos cat_accion
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@Entity
@Table(name = "cat_accion")
public class Accion {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idAccion;
	private String nomAccion;

	public Integer getIdAccion() {
		return idAccion;
	}

	public void setIdAccion(Integer idAccion) {
		this.idAccion = idAccion;
	}

	public String getNomAccion() {
		return nomAccion;
	}

	public void setNomAccion(String nomAccion) {
		this.nomAccion = nomAccion;
	}

}
