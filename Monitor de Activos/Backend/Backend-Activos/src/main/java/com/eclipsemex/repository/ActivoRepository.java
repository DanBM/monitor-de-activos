package com.eclipsemex.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.eclipsemex.model.Activo;

/**
 * Repositorio de una Activo que extiende de JPARepository y tiene como modelo
 * Activo
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
public interface ActivoRepository extends JpaRepository<Activo, Integer> {
	/**
	 * Obtiene todos los activos por medio de un procedimiento almacenado
	 * 
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo
	 * 
	 * @return lista de objetos de los activos encontrados
	 */
	@Query(value = "{call sp_obtener_activos(:emailUsuarioModificandoIn)}", nativeQuery = true)
	List<Object[]> listaActivosProcedure(@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);

	/**
	 * Obtiene un activo a detalle
	 * 
	 * @param idActivo                idActivo: id de un activo
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo
	 * 
	 * @return lista de objetos del activo encontrado
	 */
	@Query(value = "{call sp_obtener_activo_idActivo(:idActivoIn,:emailUsuarioModificandoIn)}", nativeQuery = true)
	List<Object[]> activoDetalleProcedure(@Param("idActivoIn") BigInteger idActivo,
			@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);

	/**
	 * Agrega un activo
	 * 
	 * @param placaActivo             placaActivo: placa de un activo.
	 * @param modeloActivo            modeloActivo: modelo de un activo.
	 * @param serieActivo             serieActivo: serie de un activo.
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo.
	 * 
	 * @return obtiene un entero para saber si se realizao la accion o no
	 */
	@Query(value = "{call sp_agregar_activo(:palacaIn,:modeloIn,:serieIn,:emailUsuarioModificandoIn)}", nativeQuery = true)
	int agregarActivoProcedure(@Param("palacaIn") String placaActivo, @Param("modeloIn") String modeloActivo,
			@Param("serieIn") String serieActivo, @Param("emailUsuarioModificandoIn") String emailUsuarioModificando);

	/**
	 * Actualiza un activo
	 * 
	 * @param idActivo                idActivo: id de un activo.
	 * @param placaActivo             placaActivo: placa de un activo.
	 * @param modeloActivo            modeloActivo: modelo de un activo.
	 * @param serieActivo             serieActivo: serie de un activo.
	 * @param disponible              disponible: disponibilidad de un activo.
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo.
	 * 
	 * @return obtiene un entero para saber si se realizao la accion o no
	 */
	@Query(value = "{call sp_actualizar_activo(:idActivoIn,:palacaIn,:modeloIn,:serieIn,:disponibleIn,:emailUsuarioModificandoIn)}", nativeQuery = true)
	int actualizarActivoProcedure(@Param("idActivoIn") BigInteger idActivo, @Param("palacaIn") String placaActivo,
			@Param("modeloIn") String modeloActivo, @Param("serieIn") String serieActivo,
			@Param("disponibleIn") int disponibleActivo,
			@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);

	/**
	 * Elimina un activo
	 * 
	 * @param idActivo                idActivo: id de un activo.
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo.
	 * 
	 * @return obtiene un entero para saber si se realizao la accion o no
	 */
	@Query(value = "{call sp_eliminar_activo(:idActivoIn,:emailUsuarioModificandoIn)}", nativeQuery = true)
	int eliminarActivoProcedure(@Param("idActivoIn") BigInteger idActivo,
			@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);
}
