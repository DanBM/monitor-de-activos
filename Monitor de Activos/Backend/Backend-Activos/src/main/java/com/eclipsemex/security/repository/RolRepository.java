package com.eclipsemex.security.repository;

import com.eclipsemex.security.entity.Rol;
import com.eclipsemex.security.enums.RolNombre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Repositorio de un rol que extiende de JPARepository y tiene como modelo rol
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@Repository
public interface RolRepository extends JpaRepository<Rol, Integer> {
	/**
	 * Obtiene el rol por nombre que sean parecidos a como se tiene en la clase
	 * enum.
	 * 
	 * @param rolNombre rolNombre: Clase enum que tiene los roles que se aceptan.
	 * 
	 * @return Obtiene un optional de los roles.
	 */
	Optional<Rol> findByRolNombre(RolNombre rolNombre);

}
