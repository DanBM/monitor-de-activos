package com.eclipsemex.model;

import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Modelo que pertenece a la tabla de la base de datos cat_activo
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@Entity
@Table(name = "activo")
public class Activo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger idActivo;
	private String placa;
	private String modelo;
	private String serie;
	private Integer disponible;

	/**
	 * Constructor.
	 * 
	 * @param idActivo   idActivo: id de un activo.
	 * @param placa      placa: placa de un activo.
	 * @param modelo     modelo: modelo de un activo.
	 * @param serie      serie: serie de un activo.
	 * @param disponible disponible: disponible de un activo.
	 */
	public Activo(BigInteger idActivo, String placa, String modelo, String serie, Integer disponible) {
		super();
		this.idActivo = idActivo;
		this.placa = placa;
		this.modelo = modelo;
		this.serie = serie;
		this.disponible = disponible;
	}

	public BigInteger getIdActivo() {
		return idActivo;
	}

	public void setIdActivo(BigInteger id_activo) {
		this.idActivo = id_activo;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public Integer getDisponible() {
		return disponible;
	}

	public void setDisponible(Integer disponible) {
		this.disponible = disponible;
	}

}
