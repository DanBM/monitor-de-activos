-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 01-03-2021 a las 16:06:13
-- Versión del servidor: 5.7.26
-- Versión de PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `monactivos`
--
DROP DATABASE IF EXISTS `monactivos`;
CREATE DATABASE IF NOT EXISTS `monactivos` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `monactivos`;

DELIMITER $$
--
-- Procedimientos
--
DROP PROCEDURE IF EXISTS `sp_actualizar_activo`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_actualizar_activo` (IN `spidActivo` BIGINT, IN `spplaca` VARCHAR(6), IN `spmodelo` VARCHAR(30), IN `spserie` VARCHAR(30), IN `spdisponible` INT, IN `correoUsuario` VARCHAR(50))  BEGIN
	DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countIdActivo int;
    DECLARE  idActivoModificando int;
    select count(idUsuario)  from usuario where email=correoUsuario into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=correoUsuario into idUsuarioModificando;
        select count(idActivo) from activo where idActivo=spidActivo into countIdActivo;
        if(countIdActivo>0) then
			update activo set placa=spplaca, modelo=spmodelo, serie=spserie,disponible=spdisponible where idActivo=spidActivo;
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),1,idUsuarioModificando,"activo");
            select 3;
        else 
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),1,idUsuarioModificando,"No se econtro Sensor para Modificar(Accion NO realizada)");
			select 2;
        end if;
    else
		 select 1;
	END IF;
END$$

DROP PROCEDURE IF EXISTS `sp_actualizar_monitoreo`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_actualizar_monitoreo` (IN `spidMonitoreo` BIGINT, IN `spcomentario` VARCHAR(200), IN `spidSensor` BIGINT, IN `spidActivo` BIGINT, IN `correoUsuario` VARCHAR(50))  BEGIN
	DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countIdMonitoreo int;
    DECLARE  countIdSensor int;
    DECLARE  countIdActivo int;
    select count(idUsuario)  from usuario where email=correoUsuario into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=correoUsuario into idUsuarioModificando;
        SELECT count(*) FROM monactivos.monitoreo where idMonitoreo=spidMonitoreo into countIdMonitoreo;
        if (countIdMonitoreo>0) then
			select count(idSensor) from cat_sensor where idSensor=spidSensor into countIdSensor;
				if (countIdSensor>0) then
					select count(idActivo) from activo where idActivo=spidActivo into countIdActivo;
						if(countIdActivo>0)then
							update monitoreo set fechaRevision=now(), comentario=spcomentario,idSensor=spidSensor,idActivo=spidActivo where idMonitoreo=spidMonitoreo;
							insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),1,idUsuarioModificando,"monitoreo");
							select 5;
						else
							insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),1,idUsuarioModificando,"No se encontró el idActivo a para actualizar monitoreo(Acción NO realizada)");
							select 4;
						end if;
				else
					insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),1,idUsuarioModificando,"No se encontró el idSensor a para actualizar monitoreo(Acción NO realizada)");
					select 3;
				end if;
		else 
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),1,idUsuarioModificando,"No se encontró el idMonitoreo a para actualizar monitoreo(Acción NO realizada)");
			select 2;
        end if;
    else
		select 1;
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_actualizar_password_usuario`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_actualizar_password_usuario` (IN `emailUsuarioIn` VARCHAR(50), IN `passIn` VARCHAR(100), IN `emailUsuarioModificandoIn` VARCHAR(50))  BEGIN
	DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countIdUsuarioPass int;
    DECLARE  idUsuarioModificandoPass int;
    select count(idUsuario)  from usuario where email=emailUsuarioModificandoIn into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=emailUsuarioModificandoIn into idUsuarioModificando;
        select count(idUsuario)  from usuario where email=emailUsuarioIn into countIdUsuarioPass;
        if(countIdUsuarioPass>0) then
			update usuario set password=passIn
			where email = emailUsuarioIn;
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),1,idUsuarioModificando,"usuario");
            select 3;
        else
			select 2;
        end if;
    else
		select 1;
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_actualizar_renta`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_actualizar_renta` (IN `spidRenta` BIGINT, IN `spfechaEntrega` VARCHAR(20), IN `spdireccion` VARCHAR(200), IN `spidEstado` BIGINT, IN `spidMunicipio` BIGINT, IN `spidActivo` BIGINT, IN `spcorreoUsuario` VARCHAR(50), IN `spoperandoRenta` INT, IN `correoUsuario` VARCHAR(50))  BEGIN
	DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countIdEstado int;
    DECLARE  countIdMunicipio int;
    DECLARE  countIdActivo int;
    DECLARE  countIdUsuarioAgregar int;
    DECLARE  idUsuarioAgregar int;
    DECLARE  countIdRentaModificando int;
    select count(idUsuario)  from usuario where email=correoUsuario into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=correoUsuario into idUsuarioModificando;
		select count(idRenta)  from renta where idRenta=spidRenta into countIdRentaModificando;
		if(countIdRentaModificando>0)then 
			select count(idEstado)  from cat_estado where idEstado=spidEstado into countIdEstado;
				if(countIdEstado>0) then
					select count(idMunicipio)  from cat_municipio where idMunicipio=spidMunicipio into countIdMunicipio;
					if (countIdMunicipio>0) then
						select count(idActivo)  from activo where idActivo=spidActivo into countIdActivo;
						if(countIdActivo>0)then
							select count(idUsuario)  from usuario where email=spcorreoUsuario into countIdUsuarioAgregar;
								if(countIdUsuarioAgregar>0)then
									select idUsuario  from usuario where email=spcorreoUsuario into idUsuarioAgregar;
									update renta set fechaInicio=now(),fechaVencimiento=spfechaEntrega,direccion=spdireccion,operandoRenta=spoperandoRenta,
									idEstado=spidEstado,idMunicipio=spidMunicipio,idActivo=spidActivo,idUsuario=idUsuarioAgregar where idRenta=spidRenta;
									insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),1,idUsuarioModificando,"renta");
									select 7;
								else
									insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),5,idUsuarioModificando,"No se encontró el Usuario a para modificar la renta(Acción NO realizada)");
									select 6;
								end if;
						else
							insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),5,idUsuarioModificando,"No se encontró el idActivo a para modificar la renta(Acción NO realizada)");
							select 5;
						end if;
					else
						insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),5,idUsuarioModificando,"No se encontró el idMunicipio a para modificar la renta (Acción NO realizada)");
						select 4;
					end if;
				else
					insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),5,idUsuarioModificando,"No se encontró el idEstado a para modificar la renta(Acción NO realizada)");
					select 3;
				end if;
		else
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),5,idUsuarioModificando,"No se encontró el idRenta a para modificar la renta(Acción NO realizada)");
			select 2;
		end if;
    else
		select 1;
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_actualizar_sensor`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_actualizar_sensor` (IN `idSensorIn` BIGINT, IN `nombreSensor` VARCHAR(50), IN `habilitadoSensor` INT, IN `emailUsuarioModificandoIn` VARCHAR(50))  BEGIN
	DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countIdSensor int;
    select count(idUsuario)  from usuario where email=emailUsuarioModificandoIn into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=emailUsuarioModificandoIn into idUsuarioModificando;
        select count(*) from cat_sensor where idSensor=idSensorIn into countIdSensor;
        if (countIdSensor>0) then
			update cat_sensor set nomSensor=nombreSensor,habilitado=habilitadoSensor where idSensor=idSensorIn;
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),5,countIdUsuario,"sensor");
			select 3;
        else
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),1,idUsuarioModificando,"No se encontró el idSensor para modificar(Acción NO realizada)");
			select 2;
        end if;
    else
		select 1;
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_actualizar_usuario`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_actualizar_usuario` (IN `spnombreUsuario` VARCHAR(50), IN `spappUsuario` VARCHAR(50), IN `spapmUsuario` VARCHAR(50), IN `spemail` VARCHAR(60), IN `sptel` VARCHAR(10), IN `spoperandoUsuario` INT, IN `spnombreRol` VARCHAR(30), IN `correoUsuario` VARCHAR(50))  begin 
		DECLARE  countIdUsuario int;
        DECLARE  idUsuarioModificando int;
        DECLARE  countIdUsuarioModificado int;
        DECLARE  idUsuarioModificado int;
        DECLARE  countidRolModificado int;
        DECLARE  idRolModificado int;
        select count(idUsuario)  from usuario where email=correoUsuario into countIdUsuario;
		if (countIdUsuario>0) then 
			select idUsuario  from usuario where email=correoUsuario into idUsuarioModificando;
			select count(idUsuario)  from usuario where email=spemail into countIdUsuarioModificado;
				if(countIdUsuarioModificado>0) then
					select idUsuario  from usuario where email=spemail into idUsuarioModificado;
                    select idRol from rol where rolNombre =spnombreRol into countidRolModificado;
						if(countidRolModificado>0) then
							select idRol from rol where rolNombre =spnombreRol into idRolModificado;
							update usuario set nombreUsuario=spnombreUsuario, appUsuario=spappUsuario, apmUsuario=spapmUsuario,
								tel=sptel,operandoUsuario=spoperandoUsuario where email=spemail;
							update usuario_rol set rol_id=idRolModificado where usuario_id=idUsuarioModificado;
							insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),1,idUsuarioModificando,"usuario");
							insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),1,idUsuarioModificando,"usuario_rol");
							select 4;
						else
							insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),1,idUsuarioModificando,"No se econtro el rol a modificar(Accion NO realizada)");
							select 3;
						end if;
				else
					insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),1,idUsuarioModificando,"No se econtro el usuario a modificar(Accion NO realizada)");
					select 2;
                end if;
		else
			select 1;
		END IF;
end$$

DROP PROCEDURE IF EXISTS `sp_agregar_activo`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_agregar_activo` (IN `spplaca` VARCHAR(6), IN `spmodelo` VARCHAR(30), IN `spserie` VARCHAR(30), IN `correoUsuario` VARCHAR(50))  BEGIN
	DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    select count(idUsuario)  from usuario where email=correoUsuario into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=correoUsuario into idUsuarioModificando;
		insert into activo(placa,modelo,serie,disponible)
				values (spplaca,spmodelo,spserie,1);
		insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),5,idUsuarioModificando,"activo");
        select 2;
    else
		select 1;
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_agregar_monitoreo`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_agregar_monitoreo` (IN `spcomentario` VARCHAR(200), IN `spidSensor` BIGINT, IN `spidActivo` BIGINT, IN `correoUsuario` VARCHAR(50))  BEGIN
	DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countIdSensor int;
    DECLARE  countIdActivo int;
    select count(idUsuario)  from usuario where email=correoUsuario into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=correoUsuario into idUsuarioModificando;
		select count(idSensor) from cat_sensor where idSensor=spidSensor into countIdSensor;
			if (countIdSensor>0) then
				select count(idActivo) from activo where idActivo=spidActivo into countIdActivo;
					if(countIdActivo>0)then
							insert into monitoreo(fechaRevision,comentario,idSensor,idActivo)
								values (now(), spcomentario, spidSensor, spidActivo);
							insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),5,idUsuarioModificando,"monitoreo");
                            select 4;
					else
						insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),1,idUsuarioModificando,"No se encontró el idActivo a para registrar monitoreo(Acción NO realizada)");
						select 3;
                    end if;
			else
				insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),1,idUsuarioModificando,"No se encontró el idSensor a para registrar monitoreo(Acción NO realizada)");
				select 2;
            end if;
    else
		select 1;
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_agregar_renta`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_agregar_renta` (IN `spfechaVencimiento` VARCHAR(20), IN `spdireccion` VARCHAR(200), IN `spidEstado` BIGINT, IN `spidMunicipio` BIGINT, IN `spidActivo` BIGINT, IN `spcorreoUsuario` VARCHAR(50), IN `correoUsuario` VARCHAR(50))  BEGIN
	DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countIdEstado int;
    DECLARE  countIdMunicipio int;
    DECLARE  countIdActivo int;
    DECLARE  countIdUsuarioAgregar int;
    DECLARE  idUsuarioAgregar int;
    select count(idUsuario)  from usuario where email=correoUsuario into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=correoUsuario into idUsuarioModificando;
        select count(idEstado)  from cat_estado where idEstado=spidEstado into countIdEstado;
			if(countIdEstado>0) then
				select count(idMunicipio)  from cat_municipio where idMunicipio=spidMunicipio into countIdMunicipio;
                if (countIdMunicipio>0) then
					select count(idActivo)  from activo where idActivo=spidActivo into countIdActivo;
                    if(countIdActivo>0)then
						select count(idUsuario)  from usuario where email=spcorreoUsuario into countIdUsuarioAgregar;
							if(countIdUsuarioAgregar>0)then
								select idUsuario  from usuario where email=spcorreoUsuario into idUsuarioAgregar;
								insert into renta(fechaInicio,fechaVencimiento,direccion,idEstado,idMunicipio,idActivo,idUsuario,operandoRenta)
										values (now(),spfechaVencimiento,spdireccion,spidEstado,spidMunicipio,spidActivo,idUsuarioAgregar,1);
								insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),5,idUsuarioModificando,"renta");
                                select 6;
							else
								insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),5,idUsuarioModificando,"No se encontró el Usuario a para agregar renta(Acción NO realizada)");
								select 5;
							end if;
                    else
						insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),5,idUsuarioModificando,"No se encontró el idActivo a para agregar renta (Acción NO realizada)");
						select 4;
                    end if;
                else
					insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),5,idUsuarioModificando,"No se encontró el idMunicipio a para agregar renta (Acción NO realizada)");
					select 3;
                end if;
			else
				insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),5,idUsuarioModificando,"No se encontró el idEstado a para agregar renta(Acción NO realizada)");
				select 2;
            end if;
    else
		select 1;
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_agregar_sensor`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_agregar_sensor` (IN `spNombreSensor` VARCHAR(50), IN `correoUsuario` VARCHAR(50))  BEGIN
	DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    select count(idUsuario)  from usuario where email=correoUsuario into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=correoUsuario into idUsuarioModificando;
		insert into cat_sensor values(null,spNombreSensor,1);
		select @id:=idUsuario  from usuario where email=correoUsuario;
		insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),5,idUsuarioModificando,"sensor");
    else
		select 1;
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_agregar_usuario`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_agregar_usuario` (IN `spnombreUsuario` VARCHAR(50), IN `spappUsuario` VARCHAR(50), IN `spapmUsuario` VARCHAR(50), IN `spemail` VARCHAR(60), IN `spPassword` VARCHAR(100), IN `sptel` VARCHAR(10), IN `spoperandoUsuario` INT, IN `spnombreRol` VARCHAR(30), IN `correoUsuario` VARCHAR(50))  BEGIN
	DECLARE  countIdUsuario int;
	DECLARE  idUsuarioModificando int;
	DECLARE  countIdUsuarioAgregado int;
	DECLARE  idUsuarioAgregado int;
	DECLARE  countidRolUsuarioAgregado int;
	DECLARE  idRolUsuarioAgregado int;
	select count(idUsuario)  from usuario where email=correoUsuario into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=correoUsuario into idUsuarioModificando;
		select idRol from rol where rolNombre =spnombreRol into countidRolUsuarioAgregado;
			if(countidRolUsuarioAgregado>0) then
				select idRol from rol where rolNombre =spnombreRol into idRolUsuarioAgregado;
				INSERT INTO usuario (nombreUsuario, appUsuario, apmUsuario, email, password, tel, operandoUsuario) 
					VALUES (spnombreUsuario, spappUsuario, spapmUsuario, spemail, spPassword, sptel, spoperandoUsuario);
				select count(idUsuario)  from usuario where email=spemail into countIdUsuarioAgregado;
					if(countIdUsuarioAgregado>0) then
						select idUsuario  from usuario where email=spemail into idUsuarioAgregado;
						INSERT INTO usuario_rol (usuario_id, rol_id) VALUES (idUsuarioAgregado, idRolUsuarioAgregado);
						insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),1,idUsuarioModificando,"usuario");
						insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),1,idUsuarioModificando,"usuario_rol");
						select 4;
                    else
						insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),1,idUsuarioModificando,"No se econtro Usuario agregado a para asignar rol(Accion NO realizada)");
						select 3;
						ROLLBACK;
                    end if;
			else
				insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),1,idUsuarioModificando,"No se econtro el rol a para agregar(Accion NO realizada)");
				select 2;
			end if;
	else
		select 1;
	END IF;
END$$

DROP PROCEDURE IF EXISTS `sp_eliminar_activo`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_eliminar_activo` (IN `spidActivo` BIGINT, IN `correoUsuario` VARCHAR(50))  BEGIN
	DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countIdActivo int;
    DECLARE  idActivoModificando int;
    select count(idUsuario)  from usuario where email=correoUsuario into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=correoUsuario into idUsuarioModificando;
        select count(idActivo) from activo where idActivo=spidActivo into countIdActivo;
        if(countIdActivo>0) then
			update activo set disponible=0 where idActivo=spidActivo;
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),2,idUsuarioModificando,"activo");
            select 3;
		else
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),2,idUsuarioModificando,"No se encontró el activo a para eliminar(Acción NO realizada)");
			select 2;
		end if;
	else
		select 1;
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_eliminar_renta`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_eliminar_renta` (IN `spidRenta` BIGINT, IN `correoUsuario` VARCHAR(50))  BEGIN
	DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countIdRentaEliminando int;
    select count(idUsuario)  from usuario where email=correoUsuario into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=correoUsuario into idUsuarioModificando;
		select count(idRenta)  from renta where idRenta=spidRenta into countIdRentaEliminando;
		if(countIdRentaEliminando>0)then 
			update renta set operandoRenta=0 where idRenta=spidRenta;
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),2,idUsuarioModificando,"renta");
            select 3;
		else
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),2,idUsuarioModificando,"No se encontró el idRenta a para eliminar(Acción NO realizada)");
			select 2;
		end if;
    else
		select 1;
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_eliminar_sensor`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_eliminar_sensor` (IN `spIdSensor` VARCHAR(50), IN `correoUsuario` VARCHAR(50))  BEGIN
    DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countIdSensor int;
    select count(idUsuario)  from usuario where email=correoUsuario into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=correoUsuario into idUsuarioModificando;
        select count(*) from cat_sensor where idSensor=spIdSensor into countIdSensor;
        if (countIdSensor>0) then
			update cat_sensor set habilitado=0 where idSensor=spIdSensor;
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),2,countIdUsuario,"sensor");
			select 3;
        else
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),2,idUsuarioModificando,"No se encontró el idSensor para eliminar(Acción NO realizada)");
			select 2;
        end if;
    else
		select 1;
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_eliminar_usuario`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_eliminar_usuario` (IN `spEmailUsuario` VARCHAR(50), IN `correoUsuarioModificando` VARCHAR(50))  begin 
	DECLARE  countIdUsuario int;
	DECLARE  idUsuarioModificando int;
    DECLARE  countIdUsuarioEliminar int;
	DECLARE  idUsuarioEliminar int;
    select count(idUsuario)  from usuario where email=correoUsuarioModificando into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=correoUsuarioModificando into idUsuarioModificando;
        select count(idUsuario)  from usuario where email=spEmailUsuario into countIdUsuarioEliminar;
        if(countIdUsuarioEliminar>0) then
			update usuario set operandoUsuario=0 where email=spEmailUsuario;
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),2,idUsuarioModificando,"usuario");
            select 3;
        else
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),1,idUsuarioModificando,"No se encontró el usuario a para eliminar(Acción NO realizada)");
			select 2;
        end if;
	else
		select 1;
    end if;
end$$

DROP PROCEDURE IF EXISTS `sp_obtener_acciones`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obtener_acciones` (IN `correoUsuario` VARCHAR(50))  BEGIN
	DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countBitacora int;
    select count(idUsuario)  from usuario where email=correoUsuario into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=correoUsuario into idUsuarioModificando;
		select count(*)  from bitacora into countBitacora;
		if(countBitacora>0)then 
			SELECT b.fechaIngreso,ca.nomAccion,us.nombreUsuario,us.appUsuario,us.apmUsuario,b.tablaModificada FROM bitacora b
			INNER JOIN cat_accion ca ON b.idAccion = ca.idAccion
			INNER JOIN usuario us ON b.idUsuario = us.idUsuario order by fechaIngreso desc;
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"bitacora");
		else
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"No se encontraron Acciones en bitacora(Acción NO realizada)");
			select "2";
		end if;
    else
		select "1";
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_obtener_activos`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obtener_activos` (IN `correoUsuario` VARCHAR(50))  BEGIN
	DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countIdActivo int;
    select count(idUsuario)  from usuario where email=correoUsuario into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=correoUsuario into idUsuarioModificando;
        select count(idActivo) from activo into countIdActivo;
        if(countIdActivo>0) then
			select * from activo order by disponible desc;
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"activos");
		else
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"No se encontró el activos a para eliminar(Acción NO realizada)");
			select "2";
		end if;
	else
		select "1";
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_obtener_activo_idActivo`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obtener_activo_idActivo` (IN `tipoActivo` BIGINT, IN `correoUsuario` VARCHAR(50))  BEGIN
    DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countIdActivo int;
    DECLARE  idActivoModificando int;
    select count(idUsuario)  from usuario where email=correoUsuario into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=correoUsuario into idUsuarioModificando;
        select count(idActivo) from activo where idActivo=tipoActivo into countIdActivo;
        if(countIdActivo>0) then
			select * from activo where idActivo = tipoActivo;
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"activo");
		else
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"No se encontró el activo para mostrar detalle(Acción NO realizada)");
			select "2";
		end if;
	else
		select "1";
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_obtener_detalle_usuario_email`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obtener_detalle_usuario_email` (IN `emailUsuarioIn` VARCHAR(50), IN `emailUsuarioModificandoIn` VARCHAR(50))  begin
    DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countIdUsuarioBuscado int;
    select count(idUsuario)  from usuario where email=emailUsuarioModificandoIn into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=emailUsuarioModificandoIn into idUsuarioModificando;
		SELECT count(*) FROM monactivos.usuario us
		INNER JOIN usuario_rol ur ON us.idUsuario = ur.usuario_id
		INNER JOIN rol r ON ur.rol_id = r.idRol
		where email = emailUsuarioIn into countIdUsuarioBuscado;
			if(countIdUsuarioBuscado>0) then
				SELECT us.nombreUsuario,us.appUsuario,us.apmUsuario,us.email,us.tel,us.operandoUsuario, r.rolNombre FROM monactivos.usuario us
				INNER JOIN usuario_rol ur ON us.idUsuario = ur.usuario_id
				INNER JOIN rol r ON ur.rol_id = r.idRol
				where email = emailUsuarioIn;			
				insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"usuario");
			else
				insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),1,idUsuarioModificando,"No se econtro Usuario para ver detalle(Accion NO realizada)");
				select "2";
            end if;
    else
		select "1";
    end if;
end$$

DROP PROCEDURE IF EXISTS `sp_obtener_estados`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obtener_estados` (IN `correoUsuario` VARCHAR(50))  BEGIN
    DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countEstados int;
    select count(idUsuario)  from usuario where email=correoUsuario into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=correoUsuario into idUsuarioModificando;
		select count(*)  from cat_estado into countEstados;
		if(countEstados>0)then 
			SELECT * FROM monactivos.cat_estado order by nomEstado;
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"estados");
		else
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"No se encontraron estados(Acción NO realizada)");
			select "2";
		end if;
    else
		select "1";
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_obtener_monitoreos`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obtener_monitoreos` (IN `emialUsuarioModificando` VARCHAR(50))  BEGIN
	DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countMonitoreos int;
    select count(idUsuario)  from usuario where email=emialUsuarioModificando into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=emialUsuarioModificando into idUsuarioModificando;
        SELECT count(*) FROM monitoreo into countMonitoreos;
        if(countMonitoreos>0) then
			SELECT idMonitoreo,fechaRevision,comentario,cs.idSensor,cs.nomSensor,ac.idActivo,ac.placa FROM monitoreo mo 
			INNER JOIN cat_sensor cs on mo.idSensor=cs.idSensor
			INNER JOIN activo ac on mo.idActivo=ac.idActivo 
            order by fechaRevision;
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"monitoreo");
		else
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"No se encontró monitoreos para mostrar(Acción NO realizada)");
			select "2";
		end if;
	else
		select "1";
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_obtener_monitoreo_idActivo`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obtener_monitoreo_idActivo` (IN `tipoActivo` BIGINT, IN `emialUsuarioModificando` VARCHAR(50))  BEGIN
	DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countMonitoreo int;
    select count(idUsuario)  from usuario where email=emialUsuarioModificando into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=emialUsuarioModificando into idUsuarioModificando;
        SELECT count(*) FROM monitoreo where idActivo=tipoActivo into countMonitoreo;
        if(countMonitoreo>0) then
			SELECT idMonitoreo,fechaRevision,comentario,cs.idSensor,cs.nomSensor,ac.idActivo,ac.placa FROM monitoreo mo 
			INNER JOIN cat_sensor cs on mo.idSensor=cs.idSensor
			INNER JOIN activo ac on mo.idActivo=ac.idActivo
			where mo.idActivo=tipoActivo
            order by fechaRevision;
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"monitoreo");
		else
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"No se encontró monitoreos para este activo para mostrar(Acción NO realizada)");
			select "2";
		end if;
	else
		select "1";
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_obtener_monitoreo_idMonitoreo`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obtener_monitoreo_idMonitoreo` (IN `tipoMonitoreo` BIGINT, IN `correoUsuario` VARCHAR(50))  BEGIN
    DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countMonitoreo int;
    select count(idUsuario)  from usuario where email=correoUsuario into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=correoUsuario into idUsuarioModificando;
        SELECT count(*) FROM monitoreo where idMonitoreo=tipoMonitoreo into countMonitoreo;
        if(countMonitoreo>0) then
			SELECT idMonitoreo,fechaRevision,comentario,cs.idSensor,cs.nomSensor,ac.idActivo,ac.placa FROM monitoreo mo 
			INNER JOIN cat_sensor cs on mo.idSensor=cs.idSensor
			INNER JOIN activo ac on mo.idActivo=ac.idActivo
			where idMonitoreo = tipoMonitoreo;
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"monitoreo");
		else
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"No se encontró el monitoreo para mostrar(Acción NO realizada)");
			select "2";
		end if;
	else
		select "1";
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_obtener_municipio`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obtener_municipio` (IN `correoUsuario` VARCHAR(50))  BEGIN
	DECLARE  id int;
    DECLARE  numMun int;
    DECLARE  municipios text;
    select idUsuario  from usuario where email=correoUsuario into id;
    if (id>0) then 
		insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,id,"municipio");
		SELECT count(*) FROM monactivos.cat_municipio order by nomMunicipio into numMun;
			if (numMun>0) then 
				SELECT * FROM monactivos.cat_municipio order by nomMunicipio;
			else 
				 insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,id,"No se encontró municipios para mostrar(Acción NO realizada)");
				 select "2";
			END IF;
	else
		 select "1";
	END IF;
END$$

DROP PROCEDURE IF EXISTS `sp_obtener_pass_usuario`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obtener_pass_usuario` (IN `emailUsuario` VARCHAR(50))  BEGIN
	DECLARE  countEmail int;
    select count(idUsuario)  from usuario where email=emailUsuario into countEmail;
	if (countEmail>0) then 	
		select password  from usuario where email=emailUsuario;
    else
		select "1";
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_obtener_rentas`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obtener_rentas` (IN `correoUsuario` VARCHAR(50))  BEGIN
	DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countRentas int;
    select count(idUsuario)  from usuario where email=correoUsuario into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=correoUsuario into idUsuarioModificando;
		select count(*)  from renta into countRentas;
		if(countRentas>0)then 
			SELECT idRenta,fechaInicio,fechaVencimiento,direccion,e.nomEstado,m.nomMunicipio,ac.placa,u.nombreUsuario,u.appUsuario,u.apmUsuario,u.email,operandoRenta
			FROM renta r
			INNER JOIN cat_estado e on r.idEstado=e.idEstado
			INNER JOIN cat_municipio m on r.idMunicipio=m.idmunicipio
			INNER JOIN activo ac on r.idActivo=ac.idActivo
			INNER JOIN usuario u on r.idUsuario=u.idUsuario
			order by operandoRenta desc;
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"renta");
		else
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"No se encontraron rentas(Acción NO realizada)");
			select "2";
		end if;
    else
		select "1";
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_obtener_rentas_email_usuario`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obtener_rentas_email_usuario` (IN `spEmail` VARCHAR(50), IN `correoUsuario` VARCHAR(50))  BEGIN
    DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countUsuario int;
    DECLARE  idUsuarioRenta int;
    DECLARE  countRentas int;
    select count(idUsuario)  from usuario where email=correoUsuario into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=correoUsuario into idUsuarioModificando;
        select count(idUsuario)  from usuario where email=spEmail into countUsuario;
			if (countUsuario>0) then
				select idUsuario  from usuario where email=spEmail into idUsuarioRenta;
				select count(idUsuario)  from renta where idUsuario=idUsuarioRenta into countRentas;
					if(countRentas>0)then 
						SELECT idRenta,fechaInicio,fechaVencimiento,direccion,e.nomEstado,m.nomMunicipio,ac.placa,u.nombreUsuario,u.appUsuario,u.apmUsuario,u.email,operandoRenta
						FROM renta r
						INNER JOIN cat_estado e on r.idEstado=e.idEstado
						INNER JOIN cat_municipio m on r.idMunicipio=m.idmunicipio
						INNER JOIN activo ac on r.idActivo=ac.idActivo
						INNER JOIN usuario u on r.idUsuario=u.idUsuario
						where u.idUsuario = idUsuarioRenta
						order by operandoRenta desc;
						insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"renta");
					else
						insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"No se encontraron rentas de este usuario(Acción NO realizada)");
						select "3";
					end if;
            else
				insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"No se encontro este usuario(Acción NO realizada)");
				select "2";
            end if;
    else
		select "1";
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_obtener_renta_id`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obtener_renta_id` (IN `tipoRenta` BIGINT, IN `correoUsuario` VARCHAR(50))  BEGIN
	DECLARE  id int;
    SELECT idRenta,fechaFin,fechaIngreso,fechaEntrega,direccion,e.idEstado,e.nomEstado,m.idMunicipio,m.nomMunicipio,ac.idActivo,ac.placa,ac.modelo,u.idUsuario,u.nombreUsuario,u.appUsuario,u.apmUsuario,u.email,operandoRenta
	FROM renta r
	INNER JOIN cat_estado e on r.idEstado=e.idEstado
	INNER JOIN cat_municipio m on r.idMunicipio=m.idmunicipio
	INNER JOIN activo ac on r.idActivo=ac.idActivo
	INNER JOIN usuario u on r.idUsuario=u.idUsuario
    where idRenta = tipoRenta;
    select @id:=idUsuario  from usuario where email=correoUsuario;
	insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,@id,"renta");
END$$

DROP PROCEDURE IF EXISTS `sp_obtener_renta_usuario_detalle`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obtener_renta_usuario_detalle` (IN `tipoRenta` BIGINT, IN `correoUsuario` VARCHAR(50))  BEGIN
    DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countRentas int;
    select count(idUsuario)  from usuario where email=correoUsuario into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=correoUsuario into idUsuarioModificando;
		select count(idRenta)  from renta where idRenta=tipoRenta into countRentas;
		if(countRentas>0)then 
			SELECT idRenta,fechaInicio,fechaVencimiento,direccion,e.idEstado,e.nomEstado,m.idMunicipio,m.nomMunicipio,ac.idActivo,ac.placa,u.nombreUsuario,u.appUsuario,u.apmUsuario,u.email,operandoRenta
			FROM renta r
			INNER JOIN cat_estado e on r.idEstado=e.idEstado
			INNER JOIN cat_municipio m on r.idMunicipio=m.idmunicipio
			INNER JOIN activo ac on r.idActivo=ac.idActivo
			INNER JOIN usuario u on r.idUsuario=u.idUsuario
			where idRenta=tipoRenta 
            order by operandoRenta desc;
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"renta");
		else
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"No se encontraro esta renta(Acción NO realizada)");
			select "2";
		end if;
    else
		select "1";
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_obtener_roles`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obtener_roles` (IN `correoUsuario` VARCHAR(50))  BEGIN
	DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countRoles int;
    select count(idUsuario)  from usuario where email=correoUsuario into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=correoUsuario into idUsuarioModificando;
        SELECT count(*) FROM monactivos.rol into countRoles;
        if (countRoles>0) then
			SELECT * FROM monactivos.rol order by rolNombre;
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"roles");
		else
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"No se econtro roles para mostrar(Accion NO realizada)");
			select "2";
        end if;
    else 
		select "1";
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_obtener_rol_idRol`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obtener_rol_idRol` (IN `tipoRol` BIGINT)  BEGIN
    select * from rol where idRol = tipoRol;
END$$

DROP PROCEDURE IF EXISTS `sp_obtener_rol_nombre`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obtener_rol_nombre` (IN `rolNombreIn` VARCHAR(255))  BEGIN
    select * from rol where rolNombre=rolNombreIn;
END$$

DROP PROCEDURE IF EXISTS `sp_obtener_sensores`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obtener_sensores` (IN `correoUsuario` VARCHAR(50))  BEGIN
	DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countSensores int;
    select count(idUsuario)  from usuario where email=correoUsuario into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=correoUsuario into idUsuarioModificando;
        select count(*) from CAT_SENSOR into countSensores;
        if (countSensores>0) then
			select * from CAT_SENSOR order by habilitado desc;
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"sensor");
        else
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"No se econtro Sensores para mostrar(Accion NO realizada)");
			select "2";
        end if;
    else 
		select "1";
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_obtener_sensor_idSensor`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obtener_sensor_idSensor` (IN `tipoSensor` BIGINT, IN `correoUsuario` VARCHAR(50))  begin
    DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    DECLARE  countSensor int;
    select count(idUsuario)  from usuario where email=correoUsuario into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=correoUsuario into idUsuarioModificando;
        select count(*) from CAT_SENSOR where idSensor=tipoSensor into countSensor;
        if (countSensor>0) then
			select * from CAT_SENSOR where idSensor = tipoSensor;
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"sensor");
		else
			insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"No se econtro Sensor para mostrar(Accion NO realizada)");
			select "2";
        end if;
    else 
		select "1";
    end if;
end$$

DROP PROCEDURE IF EXISTS `sp_obtener_usuarios`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obtener_usuarios` (IN `emialUsuarioModificando` VARCHAR(50))  BEGIN
	DECLARE  countIdUsuario int;
    DECLARE  idUsuarioModificando int;
    select count(idUsuario)  from usuario where email=emialUsuarioModificando into countIdUsuario;
	if (countIdUsuario>0) then 	
		select idUsuario  from usuario where email=emialUsuarioModificando into idUsuarioModificando;
		SELECT us.nombreUsuario,us.appUsuario,us.apmUsuario,us.email,us.tel,us.operandoUsuario, r.rolNombre FROM monactivos.usuario us
		INNER JOIN usuario_rol ur ON us.idUsuario = ur.usuario_id
		INNER JOIN rol r ON ur.rol_id = r.idRol
		order by operandoUsuario desc;
		insert into bitacora(fechaIngreso,idAccion,idUsuario,tablaModificada) values(now(),3,idUsuarioModificando,"usuario");
    else
		select "1";
    end if;
END$$

DROP PROCEDURE IF EXISTS `sp_obtener_usuario_email`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obtener_usuario_email` (IN `emailIn` VARCHAR(50))  BEGIN
	select * from usuario where email=emailIn;
END$$

DROP PROCEDURE IF EXISTS `sp_registrar_rol`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_registrar_rol` (IN `nombreRol` VARCHAR(50))  BEGIN
	insert into rol values(null, nombreRol);
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activo`
--

DROP TABLE IF EXISTS `activo`;
CREATE TABLE IF NOT EXISTS `activo` (
  `idActivo` bigint(20) NOT NULL AUTO_INCREMENT,
  `placa` varchar(6) DEFAULT NULL,
  `modelo` varchar(30) NOT NULL,
  `serie` varchar(30) NOT NULL,
  `disponible` int(11) NOT NULL,
  PRIMARY KEY (`idActivo`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `activo`
--

INSERT INTO `activo` (`idActivo`, `placa`, `modelo`, `serie`, `disponible`) VALUES
(1, '123aa', 'postamn', 'ps', 1),
(2, 'HM8M98', 'john deere maxi', ' 5090EH', 1),
(3, 'MX8MN5', 'john deere predator ', '5065E/5075E', 1),
(4, 'YO8651', 'john deere ulti', '5045D', 1),
(5, 'KL9812', 'john deere 95', ' 7230R', 1),
(6, 'bb', 'bb', 'bb', 1),
(7, 'cba21', 'md', 'aj-2', 1),
(10, '123a', 'postamn', 'ps', 1),
(11, 'postma', 'postamn', 'ps', 1),
(12, 'postma', 'postamn', 'ps', 0),
(13, 'postma', 'postamn', 'ps', 0),
(14, 'cba9', 'wv', 'vento', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacora`
--

DROP TABLE IF EXISTS `bitacora`;
CREATE TABLE IF NOT EXISTS `bitacora` (
  `idBitacora` bigint(20) NOT NULL AUTO_INCREMENT,
  `fechaIngreso` varchar(20) NOT NULL,
  `idAccion` bigint(20) NOT NULL,
  `idUsuario` bigint(20) NOT NULL,
  `tablaModificada` varchar(100) NOT NULL,
  PRIMARY KEY (`idBitacora`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `bitacora`
--

INSERT INTO `bitacora` (`idBitacora`, `fechaIngreso`, `idAccion`, `idUsuario`, `tablaModificada`) VALUES
(1, '2021-02-09 15:31:51', 3, 1, 'No se encontraron Acciones en bitacora(Acción NO realizada)'),
(2, '2021-02-09 15:31:53', 3, 1, 'bitacora'),
(3, '2021-02-09 15:31:59', 3, 1, 'bitacora'),
(4, '2021-02-09 15:32:04', 3, 14, 'bitacora'),
(5, '2021-02-12 14:15:52', 5, 14, 'sensor'),
(6, '2021-02-12 14:26:34', 1, 14, 'No se encontró el idSensor para modificar(Acción NO realizada)'),
(7, '2021-02-12 14:26:59', 5, 1, 'sensor'),
(8, '2021-02-15 14:23:35', 3, 14, 'activos'),
(9, '2021-02-15 15:01:17', 3, 14, 'roles'),
(10, '2021-02-15 15:13:22', 3, 14, 'roles'),
(11, '2021-02-15 15:13:50', 3, 14, 'activos'),
(12, '2021-02-15 15:17:58', 3, 14, 'activos'),
(13, '2021-02-15 15:18:53', 3, 14, 'usuario'),
(14, '2021-02-15 15:28:56', 1, 14, 'No se econtro el rol a para agregar(Accion NO realizada)'),
(15, '2021-02-15 15:34:45', 1, 14, 'usuario'),
(16, '2021-02-15 15:34:45', 1, 14, 'usuario_rol'),
(17, '2021-02-15 15:45:42', 1, 14, 'usuario'),
(18, '2021-02-15 15:45:42', 1, 14, 'usuario_rol'),
(19, '2021-02-15 15:49:02', 1, 14, 'No se encontró el usuario a para eliminar(Acción NO realizada)'),
(20, '2021-02-15 15:50:00', 2, 14, 'usuario'),
(21, '2021-02-15 15:53:45', 1, 14, 'usuario'),
(22, '2021-02-15 15:56:48', 1, 14, 'usuario'),
(23, '2021-02-15 15:58:28', 3, 14, 'usuario'),
(24, '2021-02-15 16:03:49', 3, 14, 'usuario'),
(25, '2021-02-15 16:04:42', 1, 14, 'No se econtro Usuario para ver detalle(Accion NO realizada)'),
(26, '2021-02-15 22:05:54', 1, 1, 'No se econtro Usuario para ver detalle(Accion NO realizada)'),
(27, '2021-02-15 16:06:31', 1, 14, 'No se econtro Usuario para ver detalle(Accion NO realizada)'),
(28, '2021-02-15 16:07:22', 3, 14, 'usuario'),
(29, '2021-02-15 17:03:14', 5, 14, 'sensor'),
(30, '2021-02-15 17:05:39', 5, 14, 'sensor'),
(31, '2021-02-15 17:07:07', 1, 14, 'No se encontró el idSensor para modificar(Acción NO realizada)'),
(32, '2021-02-15 17:11:03', 5, 1, 'sensor'),
(33, '2021-02-15 17:13:59', 2, 14, 'No se encontró el idSensor para eliminar(Acción NO realizada)'),
(34, '2021-02-15 17:15:29', 2, 1, 'sensor'),
(35, '2021-02-15 17:16:47', 3, 14, 'sensor'),
(36, '2021-02-15 17:19:00', 3, 14, 'activos'),
(37, '2021-02-15 17:24:29', 3, 14, 'sensor'),
(38, '2021-02-15 17:30:26', 5, 14, 'activo'),
(39, '2021-02-15 17:32:39', 1, 14, 'No se econtro Sensor para Modificar(Accion NO realizada)'),
(40, '2021-02-15 17:35:32', 1, 14, 'activo'),
(41, '2021-02-15 17:37:22', 2, 14, 'No se encontró el activo a para eliminar(Acción NO realizada)'),
(42, '2021-02-15 17:38:15', 2, 14, 'activo'),
(43, '2021-02-15 17:38:51', 3, 14, 'activos'),
(44, '2021-02-15 17:40:21', 2, 14, 'activo'),
(45, '2021-02-15 17:40:25', 2, 14, 'No se encontró el activo a para eliminar(Acción NO realizada)'),
(46, '2021-02-15 17:44:57', 3, 14, 'No se encontró el activo para mostrar detalle(Acción NO realizada)'),
(47, '2021-02-15 17:46:31', 3, 14, 'activo'),
(48, '2021-02-15 17:48:30', 1, 14, 'No se encontró el idSensor a para registrar monitoreo(Acción NO realizada)'),
(49, '2021-02-15 17:51:55', 1, 14, 'No se encontró el idActivo a para registrar monitoreo(Acción NO realizada)'),
(50, '2021-02-15 17:52:51', 5, 14, 'monitoreo'),
(51, '2021-02-15 17:54:50', 1, 14, 'No se encontró el idMonitoreo a para actualizar monitoreo(Acción NO realizada)'),
(52, '2021-02-15 17:57:12', 1, 14, 'monitoreo'),
(53, '2021-02-15 17:58:37', 3, 14, 'monitoreo'),
(54, '2021-02-15 18:01:24', 3, 14, 'No se encontró monitoreos para este activo para mostrar(Acción NO realizada)'),
(55, '2021-02-15 18:03:37', 3, 14, 'monitoreo'),
(56, '2021-02-15 18:05:39', 3, 14, 'No se encontró monitoreos para este activo para mostrar(Acción NO realizada)'),
(57, '2021-02-15 18:07:31', 3, 14, 'No se encontró monitoreos para este activo para mostrar(Acción NO realizada)'),
(58, '2021-02-15 18:07:40', 3, 14, 'monitoreo'),
(59, '2021-02-15 18:08:44', 3, 14, 'monitoreo'),
(60, '2021-02-15 18:08:48', 3, 14, 'No se encontró el monitoreo para mostrar(Acción NO realizada)'),
(61, '2021-02-15 18:11:37', 3, 14, 'monitoreo'),
(62, '2021-02-15 18:11:41', 3, 14, 'monitoreo'),
(63, '2021-02-15 18:13:47', 5, 14, 'No se encontró el idEstado a para agregar renta(Acción NO realizada)'),
(64, '2021-02-15 18:14:18', 5, 14, 'No se encontró el idEstado a para agregar renta(Acción NO realizada)'),
(65, '2021-02-15 18:20:03', 5, 14, 'No se encontró el idEstado a para agregar renta(Acción NO realizada)'),
(66, '2021-02-15 18:20:56', 5, 14, 'No se encontró el idMunicipio a para agregar renta (Acción NO realizada)'),
(67, '2021-02-15 18:22:06', 5, 14, 'No se encontró el idActivo a para agregar renta (Acción NO realizada)'),
(68, '2021-02-15 18:22:52', 5, 14, 'No se encontró el Usuario a para agregar renta(Acción NO realizada)'),
(69, '2021-02-15 18:23:59', 5, 14, 'renta'),
(70, '2021-02-15 18:25:22', 5, 14, 'No se encontró el idRenta a para modificar la renta(Acción NO realizada)'),
(71, '2021-02-15 18:27:58', 1, 14, 'renta'),
(72, '2021-02-15 18:30:15', 2, 14, 'No se encontró el idRenta a para eliminar(Acción NO realizada)'),
(73, '2021-02-15 18:31:23', 2, 14, 'renta'),
(74, '2021-02-15 18:34:37', 3, 14, 'renta'),
(75, '2021-02-15 18:35:23', 3, 14, 'No se encontraro esta renta(Acción NO realizada)'),
(76, '2021-02-15 18:36:33', 3, 14, 'renta'),
(77, '2021-02-15 18:37:22', 3, 14, 'renta'),
(78, '2021-02-15 18:39:22', 3, 14, 'No se encontro este usuario(Acción NO realizada)'),
(79, '2021-02-15 18:41:12', 3, 14, 'No se encontraro esta renta(Acción NO realizada)'),
(80, '2021-02-15 18:42:29', 3, 14, 'renta'),
(81, '2021-02-15 18:45:16', 3, 14, 'No se encontraro esta renta(Acción NO realizada)'),
(82, '2021-02-15 18:47:09', 3, 14, 'renta'),
(83, '2021-02-15 18:48:36', 3, 14, 'estados'),
(84, '2021-02-15 18:53:31', 3, 14, 'estados'),
(85, '2021-02-15 18:55:57', 3, 14, 'municipio'),
(86, '2021-02-15 18:58:22', 3, 14, 'bitacora'),
(87, '2021-02-16 17:39:50', 3, 14, 'roles'),
(88, '2021-02-16 17:51:02', 3, 14, 'usuario'),
(89, '2021-02-16 17:51:02', 3, 14, 'roles'),
(90, '2021-02-16 18:04:25', 3, 14, 'usuario'),
(91, '2021-02-16 18:04:25', 3, 14, 'roles'),
(92, '2021-02-16 18:05:01', 3, 14, 'usuario'),
(93, '2021-02-16 18:05:01', 3, 14, 'roles'),
(94, '2021-02-16 18:05:45', 3, 14, 'usuario'),
(95, '2021-02-16 18:05:45', 3, 14, 'roles'),
(96, '2021-02-16 18:06:26', 3, 14, 'usuario'),
(97, '2021-02-16 18:06:26', 3, 14, 'roles'),
(98, '2021-02-16 18:06:44', 3, 14, 'usuario'),
(99, '2021-02-16 18:06:44', 3, 14, 'roles'),
(100, '2021-02-16 18:07:08', 3, 14, 'usuario'),
(101, '2021-02-16 18:07:08', 3, 14, 'roles'),
(102, '2021-02-17 13:39:46', 3, 14, 'usuario'),
(103, '2021-02-17 13:39:46', 3, 14, 'roles'),
(104, '2021-02-17 13:40:14', 3, 14, 'usuario'),
(105, '2021-02-17 13:40:14', 3, 14, 'roles'),
(106, '2021-02-17 13:42:58', 3, 14, 'usuario'),
(107, '2021-02-17 13:42:58', 3, 14, 'roles'),
(108, '2021-02-17 13:43:19', 3, 14, 'usuario'),
(109, '2021-02-17 13:43:19', 3, 14, 'roles'),
(110, '2021-02-17 13:43:28', 3, 14, 'usuario'),
(111, '2021-02-17 13:43:28', 3, 14, 'roles'),
(112, '2021-02-17 14:55:24', 3, 14, 'usuario'),
(113, '2021-02-17 14:55:24', 3, 14, 'roles'),
(114, '2021-02-17 14:55:28', 3, 14, 'usuario'),
(115, '2021-02-17 14:55:28', 3, 14, 'roles'),
(116, '2021-02-17 15:01:22', 3, 14, 'usuario'),
(117, '2021-02-17 15:01:22', 3, 14, 'roles'),
(118, '2021-02-18 12:38:01', 1, 14, 'usuario'),
(119, '2021-02-18 14:03:42', 1, 1, 'No se econtro el rol a para agregar(Accion NO realizada)'),
(120, '2021-02-23 14:43:58', 3, 14, 'usuario'),
(121, '2021-02-23 14:43:58', 3, 14, 'roles'),
(122, '2021-02-23 18:21:58', 3, 14, 'usuario'),
(123, '2021-02-23 18:21:58', 3, 14, 'roles');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_accion`
--

DROP TABLE IF EXISTS `cat_accion`;
CREATE TABLE IF NOT EXISTS `cat_accion` (
  `idAccion` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomAccion` varchar(40) NOT NULL,
  PRIMARY KEY (`idAccion`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cat_accion`
--

INSERT INTO `cat_accion` (`idAccion`, `nomAccion`) VALUES
(1, 'update'),
(2, 'delete'),
(3, 'read'),
(4, 'write'),
(5, 'create');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_estado`
--

DROP TABLE IF EXISTS `cat_estado`;
CREATE TABLE IF NOT EXISTS `cat_estado` (
  `idEstado` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomEstado` varchar(50) NOT NULL,
  PRIMARY KEY (`idEstado`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cat_estado`
--

INSERT INTO `cat_estado` (`idEstado`, `nomEstado`) VALUES
(1, 'CDMX'),
(2, 'Estado de Mexico'),
(3, 'Hidalgo'),
(4, 'Sinaloa'),
(5, 'Morelos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_municipio`
--

DROP TABLE IF EXISTS `cat_municipio`;
CREATE TABLE IF NOT EXISTS `cat_municipio` (
  `idMunicipio` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomMunicipio` varchar(50) NOT NULL,
  PRIMARY KEY (`idMunicipio`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cat_municipio`
--

INSERT INTO `cat_municipio` (`idMunicipio`, `nomMunicipio`) VALUES
(6, 'Tonanitla'),
(7, 'Tecamac'),
(8, 'Zumpango'),
(9, 'Ecatepec'),
(10, 'Nextlalpan');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_sensor`
--

DROP TABLE IF EXISTS `cat_sensor`;
CREATE TABLE IF NOT EXISTS `cat_sensor` (
  `idSensor` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomSensor` varchar(40) NOT NULL,
  `habilitado` int(11) NOT NULL,
  PRIMARY KEY (`idSensor`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cat_sensor`
--

INSERT INTO `cat_sensor` (`idSensor`, `nomSensor`, `habilitado`) VALUES
(1, 'tanque de diesel ', 1),
(2, 'presion llantas', 1),
(3, 'aceite hidraulico ', 1),
(4, 'aceite motor', 1),
(5, 'nivel agua motor ', 1),
(6, 'prueba', 1),
(7, 'prueba', 1),
(23, 'sensor', 1),
(24, 'sensor de prueba', 1),
(25, 'sensor Modificado', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `monitoreo`
--

DROP TABLE IF EXISTS `monitoreo`;
CREATE TABLE IF NOT EXISTS `monitoreo` (
  `idMonitoreo` bigint(20) NOT NULL AUTO_INCREMENT,
  `fechaRevision` varchar(20) NOT NULL,
  `comentario` varchar(200) NOT NULL,
  `idSensor` bigint(20) NOT NULL,
  `idActivo` bigint(20) NOT NULL,
  PRIMARY KEY (`idMonitoreo`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `monitoreo`
--

INSERT INTO `monitoreo` (`idMonitoreo`, `fechaRevision`, `comentario`, `idSensor`, `idActivo`) VALUES
(1, '2021-01-18 00:00:00', 'El activo se entrego en tiempo y forma, solo se encuentra sucio por dentro ', 1, 1),
(2, '2021-01-18 00:00:00', 'El activo se entrego en tiempo y forma, se encuentra con rayones en la pintura.', 2, 2),
(3, '2021-01-18 00:00:00', 'El activo se entrego en tiempo y forma.', 3, 3),
(4, '2021-01-18 00:00:00', 'El activo se entrego en tiempo y forma, tiene una abolladura en el costado derecho.', 4, 4),
(5, '2021-01-18 00:00:00', 'El activo se entrego en tiempo y forma, solo se encuentra sucio por dentro.', 5, 5),
(6, '2021-01-26 00:00:00', 'coment', 1, 2),
(7, '2021-01-25 00:00:00', 'comentario MODIFICADOBB', 2, 2),
(8, '2021-02-03 14:57:12', 'prueba postman modificado', 1, 3),
(9, '2021-01-18 00:00:00', 'prueba postman', 1, 1),
(14, '2021-02-03 15:05:36', 'prueba postmanc', 2, 2),
(15, '2021-02-03 15:06:12', 'a', 2, 2),
(16, '2021-02-06 15:23:30', 'prueba postman modificado', 3, 3),
(17, '2021-02-09 13:14:59', 'prueba postman modificado', 2, 2),
(18, '2021-02-15 17:57:12', 'Falla de aceite', 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `renta`
--

DROP TABLE IF EXISTS `renta`;
CREATE TABLE IF NOT EXISTS `renta` (
  `idRenta` bigint(20) NOT NULL AUTO_INCREMENT,
  `fechaInicio` varchar(20) NOT NULL,
  `fechaVencimiento` varchar(20) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `idEstado` bigint(20) NOT NULL,
  `idMunicipio` bigint(20) NOT NULL,
  `idActivo` bigint(20) NOT NULL,
  `idUsuario` bigint(20) NOT NULL,
  `operandoRenta` int(11) NOT NULL,
  PRIMARY KEY (`idRenta`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `renta`
--

INSERT INTO `renta` (`idRenta`, `fechaInicio`, `fechaVencimiento`, `direccion`, `idEstado`, `idMunicipio`, `idActivo`, `idUsuario`, `operandoRenta`) VALUES
(1, '2021-02-02 13:37:06', '2021-01-18 00:00:00', 'calle 12 de octubre, centro, cp.55785', 2, 7, 7, 1, 1),
(2, '2021-01-22 00:00:00', '2021-01-23 00:00:00', 'calle 24 de octubre, pemex, 55789', 1, 6, 5, 9, 1),
(3, '2021-01-18 00:00:00', '2021-01-18 00:00:00', 'Av. Carretera federal, Napoles, cp.36251', 3, 8, 6, 12, 1),
(4, '2021-01-05 00:00:00', '2021-01-05 00:00:00', 'cerrada los angeles, texcal, cp.12051', 4, 9, 3, 10, 1),
(5, '2021-01-18 00:00:00', '2021-01-18 00:00:00', 'nuevo leaon, centro, cp.13254', 5, 10, 4, 11, 1),
(6, '2021-01-18 00:00:00', '2021-01-18 00:00:00', 'calle 12 de octubre, centro, cp.55785', 2, 7, 1, 1, 1),
(23, '2021-01-20 18:35:01', '2021-01-20 18:35:01', 'calle 12 de octubre, centro, cp.55785 MODIFICADO POSTMAN 2', 3, 8, 3, 1, 1),
(24, '2021-01-03 18:35:50', '2021-02-01 12:36:01', 'Agregada4', 1, 10, 7, 1, 1),
(25, '2021-01-20 18:35:01', '2021-01-20 18:35:01', 'calle 12 de octubre, centro, cp.55785 MODIFICADO POSTMAN 2', 4, 9, 4, 1, 0),
(26, '2021-02-09 14:28:51', '2021-01-20 18:35:01', 'calle 12 de octubre, centro, cp.55785 MODIFICADO POSTMAN 2', 1, 10, 1, 1, 0),
(27, '2021-02-15 18:27:58', '2021-01-20 18:35:01', 'calle 12 de octubre, centro, cp.55785 MODIFICADO POSTMAN 2', 1, 10, 1, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

DROP TABLE IF EXISTS `rol`;
CREATE TABLE IF NOT EXISTS `rol` (
  `idRol` bigint(20) NOT NULL,
  `rolNombre` varchar(255) NOT NULL,
  PRIMARY KEY (`idRol`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`idRol`, `rolNombre`) VALUES
(1, 'ROLE_ADMIN'),
(2, 'ROLE_USER'),
(3, 'ROLE_GERENTE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `idUsuario` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombreUsuario` varchar(50) NOT NULL,
  `appUsuario` varchar(50) NOT NULL,
  `apmUsuario` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `tel` varchar(10) NOT NULL,
  `operandoUsuario` int(11) NOT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `nombreUsuario`, `appUsuario`, `apmUsuario`, `email`, `password`, `tel`, `operandoUsuario`) VALUES
(1, 'admin', 'Badillo', 'Martinez', 'a@a.com', '$2a$10$7TUL58lWeBjXnCJZk7N33u68zguRh3Qv4sS8v/VM6ZGI4LzbmgAvO', '1234567890', 1),
(36, 'md', 'md', 'md', 'root3@root3.com', '$2a$10$d6uFxFMVtZqwY63BOlFpReM.vDIBtefpjcToAZgST5vcsOZR2o2RS', '1111111111', 0),
(9, 'gerenteBUENA', 'Badillo', 'Martinez', 'gb@gb.com', '$2a$10$De1./LA75b6lNq9aYC0xQuREKAnbkgIK9hgXDEy7Vn3vfRzplMkVO', '0987654321', 1),
(10, 'USUARIOBUENA', 'Badillo', 'Martinez', 'ub@ub.com', '$2a$10$oNIH5DwALgczShVVxsYFje8F8qRzjwYiLwbohbLugGiwf5O21hrw6', '0987654321', 1),
(11, 'USUARIOp', 'Badillo', 'Martinez', 'up@up.com', '$2a$10$HAnHbWNhiQnjfQ7clXEycOnN5dF2E8vVpuY0IRcRr40I38l3EQkJK', '0987654321', 0),
(12, 'd', 'b', 'm', 'up2@up2.com', '$2a$10$FPBCeGzCaXHJJV4R.ScTTO6zzee7REcKcr0a5s0JAfd9Emgy2xUb.', '000000000', 1),
(13, 'ad2@ad2@gmail.com', 'Badillo', 'Martinez', 'ad2@ad2.com', '$2a$10$thAEbHZmwDaVvfMnpLoJp.kRHOY2quvaoUvOwK2bqbpcncXNBVjtS', '0987654321', 0),
(14, 'admin3', 'Badillo', 'Martinez', 'ad3@ad3.com', '$2a$10$6XPl3RjxLKLeB//7hEEU2u0zOZe3AP9ZM2vF0Di4G0goJTUDTmaJW', '0987654321', 1),
(15, 'adm', 'postman', 'postman', 'ad4@ad4.com', '$2a$10$avOP5.CcwF/3JzKfiSiXZ.Heid2aMi5h1KXW/X2h0iWoPMH.rCUme', '0987654321', 1),
(37, 'Usuario', 'Prueba', 'Modificado', 'usuario@prueba.com', '$2a$10$aOuNUmSHua61EyhmVMy3seZ9BVU2JQ./EuApinYnKy4BowEjNLSfi', '1111111111', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_rol`
--

DROP TABLE IF EXISTS `usuario_rol`;
CREATE TABLE IF NOT EXISTS `usuario_rol` (
  `usuario_id` int(11) NOT NULL,
  `rol_id` int(11) NOT NULL,
  PRIMARY KEY (`usuario_id`,`rol_id`),
  KEY `FK610kvhkwcqk2pxeewur4l7bd1` (`rol_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario_rol`
--

INSERT INTO `usuario_rol` (`usuario_id`, `rol_id`) VALUES
(1, 1),
(8, 1),
(8, 2),
(9, 3),
(10, 2),
(11, 2),
(12, 3),
(14, 1),
(15, 3),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 3),
(37, 3);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
