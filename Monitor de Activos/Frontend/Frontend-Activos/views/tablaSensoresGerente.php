<?php
session_start();
/*Se verefica que el rol ingresado tenga el permiso para estar en esta pagania o pestaña.
@author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
@version 23/02/2021
 */
$views = [
    'admin'   => 'vistaAdminnistrador.html',
    'usuario' => 'vistaGeneral.html',
    'index'   => '../index.php',
];
if ($_SESSION['tokenGuardado'] == '') {
    header('Location: ' . $views['index']);
} else {
    if ($_SESSION['rol'] == 'ROLE_ADMIN') {
        header('Location: ' . $views['admin']);

    }if ($_SESSION['rol'] == 'ROLE_USER') {
        header('Location: ' . $views['usuario']);

    }
}
$curl = curl_init();
/* cambiar por el token que se necesite y como se haya guardado, este sempre tiene que ser dinamico
@author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
@version 23/02/2021
 */
curl_setopt_array($curl, array(
    CURLOPT_URL            => 'http://localhost:8080/rest/sensores',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING       => '',
    CURLOPT_MAXREDIRS      => 10,
    CURLOPT_TIMEOUT        => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST  => 'GET',
    CURLOPT_HTTPHEADER     => array(
        'Authorization: Bearer ' . $_SESSION['tokenGuardado'],
    ),
));

$response = curl_exec($curl);
curl_close($curl);
$dataToJson = json_decode($response, true);
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Tabla Sensores</title>
        <meta name="description" content="A sidebar menu as seen on the Google Nexus 7 website" />
        <meta name="keywords" content="google nexus 7 menu, css transitions, sidebar, side menu, slide out menu" />
        <meta name="author" content="Codrops" />
        <link rel="shortcut icon" href="../favicon.ico">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.2/css/all.css" integrity="sha384-vSIIfh2YWi9wW0r9iZe7RJPrKwp6bG+s9QZMoITbCckVJqGCCRhc+ccxNcdpHuYu" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/normalize.css" />
        <link rel="stylesheet" type="text/css" href="css/demo.css" />
        <link rel="stylesheet" type="text/css" href="css/component.css" />
         <!-- Librerias para la tabla -->
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js"></script>

    <!-- Datatables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/v/bs4-4.1.1/dt-1.10.18/datatables.min.css">
    <script src="https://cdn.datatables.net/v/bs4-4.1.1/dt-1.10.18/datatables.min.js"></script>

    <!-- Buttons -->
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.53/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.53/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>




        <script src="js/modernizr.custom.js"></script>
    </head>
    <body>
        <div class="container">
            <ul id="gn-menu" class="gn-menu-main">
                <li class="gn-trigger">
                    <a class="gn-icon gn-icon-menu"><span>Menú</span></a>
                    <nav class="gn-menu-wrapper">
                        <div class="gn-scroller">
                            <ul class="gn-menu">

                                <li>
                                    <a href="salir.php" class="gn-icon gn-icon-downloa">Salir</a>
                                    <ul class="gn-submenu">

                                        <li><a href="tablaActivoGerente.php" class="gn-icon gn-icon-photosh">Activos</a></li>
                                    </ul>
                                </li>
                                <li><a href="tablaMonitoreoGerente.php" class="gn-icon gn-icon-cog">Monitoreo</a></li>
                                <li><a href="tablaSensoresGerente.php" class="gn-icon gn-icon-help">Sensores</a></li>

                            </ul>
                        </div><!-- /gn-scroller -->
                    </nav>
                </li>
                <li><a href="">Sistema de Monitoreo de Activos</a></li>
                <li><a class="codrops-icon codrops-icon-" href="#addSensorModal" data-toggle="modal"><span>Nuevo Sensor</span></a></li>
                <li><a class="codrops-icon codrops-icon-" href="vistaGerente.html"><span>Home</span></a></li>
            </ul>

            <!-- /Scrip para los modales
            @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
            @version 23/02/2021
            -->

        <script type="text/javascript">
    $(document).ready(function(){
    // Activate tooltip
    $('[data-toggle="tooltip"]').tooltip();

    // Select/Deselect checkboxes
    var checkbox = $('table tbody input[type="checkbox"]');
    $("#selectAll").click(function(){
        if(this.checked){
            checkbox.each(function(){
                this.checked = true;
            });
        } else{
            checkbox.each(function(){
                this.checked = false;
            });
        }
    });
    checkbox.click(function(){
        if(!this.checked){
            $("#selectAll").prop("checked", false);
        }
    });
});
</script>

             <br> <br> <br>

<!-- /Contiene el mensaje de registro de un Sensor
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
 -->
<?php
if (isset($_SESSION['msg'])) {
    echo '<script type="text/javascript">
    alert("' . $_SESSION['msg'] . '");
    </script>';
}

unset($_SESSION['msg']);

?>
        </div><!-- /container -->
<section class="contenidoTabla">
    <div class="table-responsive" id="mydatatable-container">
        <table class="records_list table table-striped table-bordered table-hover" id="mydatatable">
            <thead>
              <tr>
                <th scope="col">Nombre del Sensor</th>
                <th scope="col">Activo</th>
                <th scope="col">Opcion</th>
              </tr>
            </thead>
             <tfoot>
                <tr>
                    <th>Filter..</th>
                    <th>Filter..</th>
                    <th>Filter..</th>

                </tr>
            </tfoot>
            <!-- /Se trae la informacion de los sensores de la DB.
                 @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
                 @version 23/02/2021
                -->
            <tbody>
            <?php foreach ($dataToJson['Contenido'] as $sensor) {?>
        <tr>

          <td><?php echo $sensor['nomSensor'] ?></td>
          <td><?php echo ($sensor['habilitado'] == 1) ? 'Si' : 'No' ?></td>
          <td>
          <a href="#deleteSensorModal" data-toggle="modal" onclick="copiarValor('<?php echo $sensor['idSensor']; ?>')"><i class="fas fa-trash-alt"></i></a>
          <a href="#editSensorModal" data-toggle="modal" onclick="llenarForm('<?php echo $sensor['idSensor']; ?>')"><i class="fas fa-edit"></i></a>
          </td>
    </tr>
    <?php }?>
            </tbody>
        </table>
    </div>

</section>
<!-- /Scrip para la tabla y filtros de la misma.
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
-->
    <script type="text/javascript">
        $(document).ready(function () {
            $('#mydatatable tfoot th').each(function () {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Filtrar.." />');
            });

            var table = $('#mydatatable').DataTable({
                "dom": 'B<"float-left"i><"float-right"f>t<"float-left"l><"float-right"p><"clearfix">',
                "responsive": false,
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                },
                "order": [
                    [0, "desc"]
                ],
                "initComplete": function () {
                    this.api().columns().every(function () {
                        var that = this;

                        $('input', this.footer()).on('keyup change', function () {
                            if (that.search() !== this.value) {
                                that
                                    .search(this.value)
                                    .draw();
                            }
                        });
                    })
                },
                "buttons": [ 'excel', 'pdf']
            });
        });
    </script>

        <script src="js/classie.js"></script>
        <script src="js/gnmenu.js"></script>
        <script>
            new gnMenu( document.getElementById( 'gn-menu' ) );
        </script>
          <!-- /Modal Agregar Sensor
            @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
            @version 23/02/2021
          -->
        <div id="addSensorModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="../controllerSensor/registroSensorG.php" method="POST">
                    <div class="modal-header">
                        <h4 class="modal-title">Nuevo Sensor</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" name="txtnombre" class="form-control" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
                        <input type="submit" class="btn btn-success" value="Agregar">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /Modal Editar Sensor
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
    -->
    <div id="editSensorModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>
                    <div class="modal-header">
                        <h4 class="modal-title">Editar Sensor</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="hidden" id="idSensor" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" id="nombreSensor" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Habilitado</label>
                            <input type="radio" name="habilitado" id="si" value="1"> si
                            </label>
                            <label>
                                <input type="radio" name="habilitado" id="no" value="0"> no
                            </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
                        <button class="btn btn-success" onclick="editarSensor();">Editar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /Modal Eliminar Sensor
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
     -->
    <div id="deleteSensorModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="#">
                    <div class="modal-header">
                        <h4 class="modal-title">Eliminar Sensor</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <input type="hidden" id="idSensore" name="">
                    <div class="modal-body">
                        <p>¿Está seguro de que desea eliminar este registro?</p>
                        <p class="text-warning"><small>Esta acción no se puede deshacer.</small></p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
                        <button class="btn btn-danger" onclick="eliminarSensor();">Borrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /Llenar informacion para editar Sensor Script
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
    -->
<script type="text/javascript">
     var token='<?php echo $_SESSION['tokenGuardado']; ?>';
    function llenarForm(idSensor){
        var settings = {
          "url": "http://localhost:8080/rest/sensor-detalle?idSensor=" + idSensor,
          "method": "GET",
          "dataType": 'json',
          "headers": {
            "Authorization": "Bearer " + token
          },
        };

        $.ajax(settings).done(function (response) {
          console.log(response);
          $('#idSensor').val(response['Contenido'][0]['idSensor']);
          $('#nombreSensor').val(response['Contenido'][0]['nomSensor']);
           if (response['Contenido'][0]['habilitado'] == 1) {
                    document.querySelector('#si').checked = true;
                } else {
                    document.querySelector('#no').checked = true;
                }

    });
    }
</script>
<!-- /Editar Sensor Script
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
    -->
<script type="text/javascript">
        var token='<?php echo $_SESSION['tokenGuardado']; ?>';

        function editarSensor(){
        var idSensor=$('#idSensor').val();
        var nombreSensor=$('#nombreSensor').val();
        var  select;
            if (document.querySelector('#si').checked == true) {
                  select=$('#si').val();
                }else{
                    select=$('#no').val();
                }
            var data=JSON.stringify({"idSensor":idSensor,"nomSensor":nombreSensor,"habilitado":parseInt(select)});
           $.ajax({
        url: 'http://localhost:8080/rest/sensor',
        type: 'PUT',
        data: data ,
        contentType:"application/json",
        async: false,
        headers: {
                    "Authorization": "Bearer " + token
                },
        success: function (msg) {
            var jsonData = JSON.parse(msg);
            alert(jsonData.Contenido);

        },
        error: function (error) {
            var jsonData = JSON.parse(error);
            alert(jsonData.Contenido);
        }
    });
        }
</script>

<!-- /Eliminar Sensor Script
@author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
@version 23/02/2021
-->
 <script type="text/javascript">
        var token='<?php echo $_SESSION['tokenGuardado']; ?>';
        function copiarValor(idSensor){

            $('#idSensore').val(idSensor);
        }


        function eliminarSensor(){
            var idSensor = $('#idSensore').val();
         $.ajax({
        url: "http://localhost:8080/rest/sensor?idSensor=" + idSensor,
        type: 'DELETE',
        contentType:"application/json",
        async: false,
        headers: {
                    "Authorization": "Bearer " + token
                },
        success: function (msg) {
            var jsonData = JSON.parse(msg);
            alert(jsonData.Contenido);

        },
        error: function (error) {
            var jsonData = JSON.parse(error);
            alert(jsonData.Contenido);
        }
    });
}
</script>
<!-- /Cerrar Pagina en 15 min Script
    @author Hernandez Rodriguez Edwin Ulices, Badillo Martinez Daniel
    @version 23/02/2021
-->
 <div id="warning" style="display: none;" class="modal fade">
            <p>
                Tiempo para finalizar la sesion.
                <span>
                </span>
            </p>
            <p>
                <button id="close" type="button">
                    Close
                </button>
            </p>
        </div>

<script type="text/javascript">
var timeOfLastActivity;
var timeForCloseSession;
var timeBeforeWarning = 5000;   // 5 seconds
var timeBeforeClose = 900000;    // 15 min
var timeForUpdateWarning = 500; // 0.5 seconds

function resetAll() {
    timeOfLastActivity = new Date().getTime();
    timeForCloseSession = timeOfLastActivity + timeBeforeClose;
    setTimeout(function() {
        $("#warning").show();
        showTime();
    }, timeBeforeWarning);
    $("#warning").hide();
}

function showTime() {
    var currentTime = new Date().getTime();
    if (currentTime < timeForCloseSession) {
        $("#warning span").html(timeForCloseSession - currentTime)
    } else {
        $("#close").trigger("click");
    }
    if ($("#warning").is(":visible")) {
        setTimeout(showTime, timeForUpdateWarning);
    }
}

$("#close").click(function() {
    $("#warning").hide();
    location.href ="../index.php";
    $.post("/echo/json/"); // Send AJAX close
});


resetAll();
        </script>
    </body>
</html>