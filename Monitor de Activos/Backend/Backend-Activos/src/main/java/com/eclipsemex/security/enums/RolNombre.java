package com.eclipsemex.security.enums;

/**
 * Clase de enumerador para los roles que existen en el sistema
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
public enum RolNombre {
	ROLE_ADMIN, ROLE_GERENTE, ROLE_USER
}
