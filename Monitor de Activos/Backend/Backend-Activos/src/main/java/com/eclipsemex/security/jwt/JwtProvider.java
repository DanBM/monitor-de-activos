package com.eclipsemex.security.jwt;

import com.eclipsemex.security.entity.UsuarioPrincipal;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Genera el token de un usuario
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021  
 * */
@Component
public class JwtProvider {
	private final static Logger logger = LoggerFactory.getLogger(JwtProvider.class);

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private int expiration;
    
	/**
	 * Genera el token de un usuario
	 * 
	 * @param authentication obtiene las credenciales de un usuario
	 * 
	 * @return una cadena donde se obtiene el token, bearer, email de un usuario
	 */
    public String generateToken(Authentication authentication){
        UsuarioPrincipal usuarioPrincipal = (UsuarioPrincipal) authentication.getPrincipal();
        return Jwts.builder().setSubject(usuarioPrincipal.getEmail())
                .setIssuedAt(new Date())
                .setExpiration(new Date(new Date().getTime() + expiration*25))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    /**
	 * Obtienen el email de un usuario desde un token 
	 * 
	 * @param token Es el token que tiene un usuario
	 * 
	 * @return una cadena donde se obtiene el nombre de un usuario
	 */
    public String getNombreUsuarioFromToken(String token){
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getSubject();
    }

    /**
	 * Valida si un token es correcto o hay algún error en el. 
	 * 
	 * @param token Es el token que tiene un usuario.
	 * 
	 * @return devuelve una respuesta si es válido 
	 */
    public boolean validateToken(String token){
        try {
            Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
            return true;
        }catch (MalformedJwtException e){
            logger.error("token mal formado");
        }catch (UnsupportedJwtException e){
            logger.error("token no soportado");
        }catch (ExpiredJwtException e){
            logger.error("token expirado");
        }catch (IllegalArgumentException e){
            logger.error("token vacío");
        }catch (SignatureException e){
            logger.error("fail en la firma");
        }
        return false;
    }
}
