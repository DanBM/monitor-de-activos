package com.eclipsemex.security.enums;

/**
 * Clase enumerador para errores
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
public enum Errores {
	Usuario_No_Autenticado, No_Hay_Resultados, No_Se_Encontro_EL_Rol, Usuario_Agregado_No_Se_Encontro,
	Usuario_No_Encontrado, Sensor_No_Encontrado, Activo_No_Encontrado, Monitoreo_No_Encontrado, Estado_No_Encontrado,
	Municipio_No_Encontrado, Renta_No_Encontrada, Usuario_Repetido;

}
