package com.eclipsemex.security.service;

import com.eclipsemex.security.entity.Usuario;
import com.eclipsemex.security.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * servicio donde se generan metodos para obtener información de un usuario
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@Service
@Transactional
public class UsuarioService {

	@Autowired
	UsuarioRepository usuarioRepository;

	/**
	 * Obtiene un usuario por medio de su correo
	 * 
	 * @body email email:Correo de un usuario
	 * 
	 * @return Informacion de un usuario
	 */
	public Optional<Usuario> getByEmail(String email) {
		return usuarioRepository.buscarUsuarioEmailProcedure(email);
	}

	/**
	 * Busca si un usuario existe por medio de su correo
	 * 
	 * @param email email: correo del usuario.
	 * 
	 * @return booleano si se encontro o no el usuario.
	 */
	public boolean existsByEmail(String email) {
		return usuarioRepository.existsByEmail(email);
	}

	/**
	 * Obtiene la contraseña de un usuario
	 * 
	 * @param email Correo existente de un usuario
	 * 
	 * @return la contraseña de un usuario.
	 */
	public String obtenerPass(String email) {
		return usuarioRepository.buscarPassUsuarioEmailProcedure(email);
	}

}
