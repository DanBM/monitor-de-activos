package com.eclipsemex.services;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import org.springframework.security.core.Authentication;

import com.eclipsemex.model.Activo;

/**
 * Interfaz donde se definen los metodos de un Activo
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
public interface IActivoService {
	/**
	 * Obtiene todos los Activos
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	String buscarTodos(Authentication auth);

	/**
	 * Obtiene un activo en especifico
	 * 
	 * @param idActivo: representa un id de un activo
	 * 
	 * @param auth      Representa el token para una solicitud de autenticación no
	 *                  se manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	String activoDeatlle(BigInteger idActivo, Authentication auth);

	/**
	 * Agrega un nuevo Activo
	 * 
	 * @param activo: representa el modelo de un activo
	 * 
	 * @param auth:   Representa el token para una solicitud de autenticación no se
	 *                manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	String agregarActivo(Activo activo, Authentication auth);

	/**
	 * Modificar un nuevo Activo
	 * 
	 * @param activo: representa el modelo de un activo
	 * 
	 * @param auth:   Representa el token para una solicitud de autenticación no se
	 *                manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	String modificarActivo(Activo activo, Authentication auth);

	/**
	 * Eliminar un nuevo Activo
	 * 
	 * @param activo: representa el modelo de un activo
	 * 
	 * @param auth:   Representa el token para una solicitud de autenticación no se
	 *                manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	String eliminarActivo(BigInteger idActivo, Authentication auth);

}
