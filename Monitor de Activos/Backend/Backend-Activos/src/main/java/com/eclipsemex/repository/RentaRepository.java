package com.eclipsemex.repository;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.eclipsemex.model.Renta;

/**
 * Repositorio de una Renta que extiende de JPARepository y tiene como modelo
 * Renta
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
public interface RentaRepository extends JpaRepository<Renta, Integer> {
	/**
	 * Agrega una renta
	 * 
	 * @param fechaEntrega            fechaEntrega: Fecha de entrega de la renta
	 * @param direccion               direccion: Direccion de una renta
	 * @param estado                  estado: id de un Estado.
	 * @param municipio               municipio: id de un Municipio.
	 * @param idActivo                idActivo: id de un Activo.
	 * @param emailUsuario            emailUsuario: Correo de un usuario .
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo.
	 * 
	 * @return obtiene un entero para saber si se realizao la accion o no
	 */
	@Query(value = "{call sp_agregar_renta(:fechaEntregaIn,:direccionIn,:estadoIn,:municipioIn,:idActivoIn,:emailUsuarioIn,:emailUsuarioModificandoIn)}", nativeQuery = true)
	int agregarRenta(@Param("fechaEntregaIn") String fechaEntrega, @Param("direccionIn") String direccion,
			@Param("estadoIn") BigInteger estado, @Param("municipioIn") BigInteger municipio,
			@Param("idActivoIn") BigInteger idActivo, @Param("emailUsuarioIn") String emailUsuario,
			@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);

	/**
	 * Modificar una renta
	 * 
	 * @param idRenta                 idRenta: id de una renta.
	 * @param fechaEntrega            fechaEntrega: Fecha de entrega de la renta.
	 * @param direccion               direccion: Direccion de una renta.
	 * @param estado                  estado: id de un Estado.
	 * @param municipio               municipio: id de un Municipio.
	 * @param idActivo                idActivo: id de un Activo.
	 * @param emailUsuario            emailUsuario: Correo de un usuario .
	 * @param operandoRenta           operandoRenta: estado de una renta.
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo.
	 * 
	 * @return obtiene un entero para saber si se realizao la accion o no
	 */
	@Query(value = "{call sp_actualizar_renta(:idRentaIn,:fechaEntregaIn,:direccionIn,:estadoIn,:municipioIn,:idActivoIn,:emailUsuarioIn,:operandoRentaIn,:emailUsuarioModificandoIn)}", nativeQuery = true)
	int modificarRenta(@Param("idRentaIn") BigInteger idRenta, @Param("fechaEntregaIn") String fechaEntrega,
			@Param("direccionIn") String direccion, @Param("estadoIn") BigInteger estado,
			@Param("municipioIn") BigInteger municipio, @Param("idActivoIn") BigInteger idActivo,
			@Param("emailUsuarioIn") String emailUsuario, @Param("operandoRentaIn") int operandoRenta,
			@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);

	/**
	 * Eliminar una renta
	 * 
	 * @param idRenta                 idRenta: id de una renta.
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo.
	 * 
	 * @return obtiene un entero para saber si se realizao la accion o no
	 */
	@Query(value = "{call sp_eliminar_renta(:idRentaIn,:emailUsuarioModificandoIn)}", nativeQuery = true)
	int eliminarRenta(@Param("idRentaIn") BigInteger idRenta,
			@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);

	/**
	 * Obtiene todas las rentaspor medio de un procedimiento almacenado
	 * 
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo
	 * 
	 * @return lista de objetos de las rentas encontradas
	 */
	@Query(value = "{call sp_obtener_rentas(:emailUsuarioModificandoIn)}", nativeQuery = true)
	List<Object[]> listaRentasUsuariosProcedure(@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);

	/**
	 * Obtiene las renras de un usuario a partir de un correo de un usuario por
	 * medio de un procedimiento almacenado
	 * 
	 * @param email                   email: Correo de un usuario
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo
	 * 
	 * @return lista de objetos de las rentas de un usuario
	 */
	@Query(value = "{call sp_obtener_rentas_email_usuario(:emailIn,:emailUsuarioModificandoIn)}", nativeQuery = true)
	List<Object[]> listaRentasUsuarioProcedure(@Param("emailIn") String email,
			@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);

	/**
	 * Obtiene el detalle de una renta a partir de un id de una renta por medio de
	 * un procedimiento almacenado
	 * 
	 * @param idRenta                 idRenta: id de una renta.
	 * @param emailUsuarioModificando emailUsuarioModificando: correo del usuario
	 *                                que esta en la sesion llamando a este metodo.
	 * 
	 * @return lista de objetos de una renta.
	 */
	@Query(value = "{call sp_obtener_renta_usuario_detalle(:idRentaIn,:emailUsuarioModificandoIn)}", nativeQuery = true)
	List<Object[]> rentaUsuarioDetalleProcedure(@Param("idRentaIn") BigInteger idRenta,
			@Param("emailUsuarioModificandoIn") String emailUsuarioModificando);

}
