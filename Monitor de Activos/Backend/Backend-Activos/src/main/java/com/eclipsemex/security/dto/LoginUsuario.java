package com.eclipsemex.security.dto;

/*
 * Modelo donde se define parametros de un usuario que quiere obtener sus credenciales 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021  
 * */
public class LoginUsuario {
	private String email;
	private String password;

	public String getEmail() {
		return email;
	}

	public void setEmail(String nombreUsuario) {
		this.email = nombreUsuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
