package com.eclipsemex.model;

import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;

/**
 * Modelo que pertenece a la tabla de la base de datos renta
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@Entity
@Table(name = "renta")
public class Renta {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger idRenta;
	private String fechaInicio;
	private String fechaVencimiento;
	private String direccion;
	private BigInteger idEstado;
	private String estado;
	private BigInteger idMunicipio;
	private String municipio;
	private BigInteger idActivo;
	private String placActivo;
	private String nombreUsuario;
	private String email;
	private int operandoRenta;

	/**
	 * Constructor.
	 */
	public Renta() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param idRenta          idRenta: id de una renta.
	 * @param fechaInicio      fechaInicio: fechaInicio de una renta.
	 * @param fechaVencimiento fechaVencimiento: fechaVencimiento de una renta.
	 * @param direccion        direccion: direccion de una renta.
	 * @param idEstado         idEstado: idEstado de un estado.
	 * @param estado           estado: nombre de un estado.
	 * @param idMunicipio      idMunicipio: id de un municipio.
	 * @param municipio        municipio: nombre de un municipio.
	 * @param idActivo         idActivo: id de un activo.
	 * @param placaActivo      placaActivo: placa de un activo.
	 * @param nombreUsuario    nombreUsuario: nombre de un usuario.
	 * @param email            email: correo de un usuario
	 * @param operandoRenta    operandoRenta: estado de un activo.
	 */
	public Renta(BigInteger idRenta, String fechaInicio, String fechaVencimiento, String direccion, BigInteger idEstado,
			String estado, BigInteger idMunicipio, String municipio, BigInteger idActivo, String placActivo,
			String nombreUsuario, String email, int operandoRenta) {
		super();
		this.idRenta = idRenta;
		this.fechaInicio = fechaInicio;
		this.fechaVencimiento = fechaVencimiento;
		this.direccion = direccion;
		this.idEstado = idEstado;
		this.estado = estado;
		this.idMunicipio = idMunicipio;
		this.municipio = municipio;
		this.idActivo = idActivo;
		this.placActivo = placActivo;
		this.nombreUsuario = nombreUsuario;
		this.email = email;
		this.operandoRenta = operandoRenta;
	}

	/**
	 * Constructor.
	 * 
	 * @param idRenta          idRenta: id de una renta.
	 * @param fechaInicio      fechaInicio: fechaInicio de una renta.
	 * @param fechaVencimiento fechaVencimiento: fechaVencimiento de una renta.
	 * @param direccion        direccion: direccion de una renta.
	 * @param estado           estado: nombre de un estado.
	 * @param municipio        municipio: nombre de un municipio.
	 * @param placaActivo      placaActivo: placa de un activo.
	 * @param nombreUsuario    nombreUsuario: nombre de un usuario.
	 * @param email            email: correo de un usuario
	 * @param operandoRenta    operandoRenta: estado de un activo.
	 */
	public Renta(BigInteger idRenta, String fechaInicio, String fechaVencimiento, String direccion, String estado,
			String municipio, String placActivo, String nombreUsuario, String email, int operandoRenta) {
		super();
		this.idRenta = idRenta;
		this.fechaInicio = fechaInicio;
		this.fechaVencimiento = fechaVencimiento;
		this.direccion = direccion;
		this.estado = estado;
		this.municipio = municipio;
		this.placActivo = placActivo;
		this.nombreUsuario = nombreUsuario;
		this.email = email;
		this.operandoRenta = operandoRenta;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getPlacActivo() {
		return placActivo;
	}

	public void setPlacActivo(String placActivo) {
		this.placActivo = placActivo;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public void setOperandoRenta(int operandoRenta) {
		this.operandoRenta = operandoRenta;
	}

	public BigInteger getIdRenta() {
		return idRenta;
	}

	public void setIdRenta(BigInteger idRenta) {
		this.idRenta = idRenta;
	}

	public BigInteger getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(BigInteger idEstado) {
		this.idEstado = idEstado;
	}

	public BigInteger getIdMunicipio() {
		return idMunicipio;
	}

	public void setIdMunicipio(BigInteger idMunicipio) {
		this.idMunicipio = idMunicipio;
	}

	public BigInteger getIdActivo() {
		return idActivo;
	}

	public void setIdActivo(BigInteger idActivo) {
		this.idActivo = idActivo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Integer getOperandoRenta() {
		return operandoRenta;
	}

	public void setOperandoRenta(Integer operandoRenta) {
		this.operandoRenta = operandoRenta;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

}
