package com.eclipsemex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendActivosApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendActivosApplication.class, args);
	}

}
