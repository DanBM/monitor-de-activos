package com.eclipsemex.services.db;

import java.math.BigInteger;

import java.util.LinkedList;
import java.util.List;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.eclipsemex.model.Sensor;
import com.eclipsemex.repository.SensorRepository;

import com.eclipsemex.security.entity.UsuarioPrincipal;
import com.eclipsemex.security.enums.Errores;
import com.eclipsemex.services.ISensorService;
/**
 * Clase de servicio para los Sensores
 * 
 * @author Badillo Martínez Daniel, Hernández Rodríguez Edwin Ulices
 * @version 12.02.2021
 */
@Service
@Primary
public class SensorServiceJpa implements ISensorService {

	@Autowired
	private SensorRepository repoSensor;
	private UsuarioPrincipal usuarioPrincipal;

	/**
	 * Obtiene todos los sensores
	 * 
	 * @param auth Representa el token para una solicitud de autenticación no se
	 *             manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@Override
	public String buscarTodos(Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		List<Object[]> todosSensores = repoSensor.listaSensoresProcedure(emailUsuarioModificando);
		Object excepcion = (Object) todosSensores.get(0)[0];
		if (excepcion.equals("1")) {
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.BAD_REQUEST);
		} else if (excepcion.equals("2")) {
			return respuestaJson("Error", Errores.No_Hay_Resultados, HttpStatus.NO_CONTENT);
		} else {
			List<Sensor> sensores = new LinkedList();
			for (Object[] objects : todosSensores) {
				BigInteger idSensor = (BigInteger) objects[0];
				String nomSensor = (String) objects[1];
				int habilitado = (int) objects[2];
				Sensor sensor = new Sensor(idSensor, nomSensor, habilitado);
				sensores.add(sensor);
			}
			return respuestaJson("Éxito", sensores, HttpStatus.OK);
		}
	}

	/**
	 * Obtiene el detalle de un sensor
	 * 
	 * @param idSensor: representa un id de un sensor
	 * 
	 * @param auth      Representa el token para una solicitud de autenticación no
	 *                  se manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o el contenido que se solicito
	 */
	@Override
	public String sensorDetalle(BigInteger idSensorB, Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		if (soloNumeros(String.valueOf(idSensorB)) == false)
			return respuestaJson("Error", "El campo 'idSensor' debe estar lleno, contener solo numeros",
					HttpStatus.BAD_REQUEST);
		List<Object[]> todosSensores = repoSensor.sensorDetalleProcedure(idSensorB, emailUsuarioModificando);
		Object excepcion = (Object) todosSensores.get(0)[0];
		if (excepcion.equals("1")) {
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.BAD_REQUEST);
		} else if (excepcion.equals("2")) {
			return respuestaJson("Error", Errores.No_Hay_Resultados, HttpStatus.NO_CONTENT);
		} else {
			List<Sensor> sensores = new LinkedList();
			for (Object[] objects : todosSensores) {
				BigInteger idSensor = (BigInteger) objects[0];
				String nomSensor = (String) objects[1];
				int habilitado = (int) objects[2];
				Sensor sensor = new Sensor(idSensor, nomSensor, habilitado);
				sensores.add(sensor);
			}
			return respuestaJson("Éxito", sensores, HttpStatus.OK);
		}
	}

	/**
	 * Agrega un nuevo sensor
	 * 
	 * @param nombreSensor: representa el nombre de un sensor
	 * 
	 * @param auth:         Representa el token para una solicitud de autenticación
	 *                      no se manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@Override
	public String agregarSensor(String nombreSensor, Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		if (nombreSensor.trim().isEmpty() || nombreSensor.trim() == null || nombreSensor.trim().length() <= 0
				|| nombreSensor.trim().length() > 40 || soloLetras(nombreSensor.trim().replace(" +", "")) == false)
			return respuestaJson("Error",
					"El 'nombreSensor' debe estar lleno, tener solo letras y debe tener entre 1 y 40 caracteres.",
					HttpStatus.BAD_REQUEST);
		int status = repoSensor.agregarSensorProcedure(nombreSensor, emailUsuarioModificando);
		if (status == 1)
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.NO_CONTENT);
		return respuestaJson("Éxito", "Sensor registrado", HttpStatus.OK);

	}

	/**
	 * Modifica un sensor
	 * 
	 * @body sensor: representa un modelo de sensor
	 * 
	 * @param auth: Representa el token para una solicitud de autenticación no se
	 *              manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@Override
	public String modificarSensor(Sensor sensor, Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		if (sensor.getNomSensor().trim().isEmpty() || sensor.getNomSensor().trim() == null
				|| sensor.getNomSensor().trim().length() <= 0 || sensor.getNomSensor().trim().length() > 40
				|| soloLetras(sensor.getNomSensor().trim().replace(" +", "")) == false)
			return respuestaJson("Error",
					"El 'nombreSensor' debe estar lleno, tener solo letras y debe tener entre 1 y 40 caracteres.",
					HttpStatus.BAD_REQUEST);
		if (sensor.getHabilitado() < 0 || sensor.getHabilitado() > 1
				|| soloNumeros(String.valueOf(sensor.getHabilitado())) == false)
			return respuestaJson("Error",
					"El campo 'habilitado' debe estar lleno, contener solo numeros y debe ser 0 o 1",
					HttpStatus.BAD_REQUEST);
		int status = repoSensor.modificarSensorProcedure(sensor.getIdSensor(), sensor.getNomSensor(),
				sensor.getHabilitado(), emailUsuarioModificando);
		if (status == 1)
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.NO_CONTENT);
		if (status == 2)
			return respuestaJson("Error", Errores.Sensor_No_Encontrado, HttpStatus.NO_CONTENT);
		return respuestaJson("Éxito", "Sensor Actualizado", HttpStatus.OK);
	}

	/**
	 * Elimina un sensor
	 * 
	 * @param idSensor: representa un id de un sensor
	 * 
	 * @param auth:     Representa el token para una solicitud de autenticación no
	 *                  se manda como @body o @param por que es el token.
	 * 
	 * @return mensajes de error o mensaje satisfactorio
	 */
	@Override
	public String eliminarSensor(BigInteger idSensor, Authentication auth) {
		usuarioPrincipal = (UsuarioPrincipal) auth.getPrincipal();
		String emailUsuarioModificando = usuarioPrincipal.getEmail();
		if (soloNumeros(String.valueOf(idSensor)) == false)
			return respuestaJson("Error", "El campo 'idSensor' debe estar lleno, contener solo numeros",
					HttpStatus.BAD_REQUEST);
		int status = repoSensor.eliminarSensorProcedure(idSensor, emailUsuarioModificando);
		if (status == 1)
			return respuestaJson("Error", Errores.Usuario_No_Autenticado, HttpStatus.NO_CONTENT);
		if (status == 2)
			return respuestaJson("Error", Errores.Sensor_No_Encontrado, HttpStatus.NO_CONTENT);
		return respuestaJson("Éxito", "Sensor Eliminado", HttpStatus.OK);
	}

	/**
	 * Genera un JSON de respuesta en String
	 * 
	 * @param respuesta que respuesta se requiere mostrar
	 * 
	 * @param contenido objeto que puede tener cualquier estructura para mostrar
	 * 
	 * @param codigo    codigo de respuesta que se desea enviar
	 * 
	 * @return JSON con valores enviados en formato string
	 */
	private String respuestaJson(String respuesta, Object contenido, HttpStatus codigo) {
		JSONObject objson = new JSONObject();
		objson.put("Respuesta", respuesta);
		objson.put("Contenido", contenido);
		objson.put("Codigo http", codigo);
		return objson.toString();
	}

	/**
	 * Valida que una cadena tenga solo letras
	 * 
	 * @param cadena que respuesta se requiere mostrar
	 * 
	 * @return true o false si el correo es valido o incorrecto segun su caso
	 */
	private boolean soloLetras(String cadena) {
		Pattern pat = Pattern.compile("[ A-Za-zäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓ	ÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙñÑ.-]+");
		Matcher mat = pat.matcher(cadena);
		return mat.matches();
	}

	/**
	 * Valida que una cadena tenga solo numeros
	 * 
	 * @param cadena que respuesta se requiere mostrar
	 * 
	 * @return true o false si el correo es valido o incorrecto segun su caso
	 */
	private boolean soloNumeros(String cadena) {
		if (cadena.matches("[0-9]*")) {
			return true;
		} else {
			return false;
		}
	}

}
